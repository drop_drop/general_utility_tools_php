<?php
/*
 * description：
 * author：wh
 * email：
 * createTime：{2024/3/17} {23:15} 
 */

namespace wanghua\general_utility_tools_php\twitter;


use wanghua\general_utility_tools_php\html\Htmlcontent;
use wanghua\general_utility_tools_php\http\Curl;
use wanghua\general_utility_tools_php\tool\Tools;

class Twitter
{

    public $url = '';
    public $bearer_token = '';
    /**
     * desc：
     *
     * $obj = new Twitter();
     * $obj->search($params);
     *
     * 请求方式：接受请求参数时需post提交
     *
     * 参数：
     * params 参数
     * headers 请求头
     *
    //params参数说明：
    //query：必填，查询匹配Tweets。最多512个字符。使用 OR 连接关键词表明返回包含这些关键词中任意一个的推文（OR必须大写）
    //start_time：选填，开始的时间(最早可查看7天前)。YYYY-MM-DDTHH:mm:ssZ ('2024-01-01T00:00:00Z')。
    //end_time:选填，结束时间。YYYY-MM-DDTHH:mm:ssZ ('2024-01-01T00:00:00Z')。
    //since_id:选填，返回推文 ID 大于（即更新于）指定 ID 的结果。指定的 ID 是独占的，响应将不包括它
    //until_id:选填，返回推文 ID 小于（即早于）指定 ID 的结果。指定的 ID 是独占的，响应将不包括它。
    //max_results:选填，请求要返回的最大搜索结果数。必须介于 10 和 100 之间的数字，默认为10，
    //next_token:选填，此参数用于获取结果的下一“页”。与参数一起使用的值直接从 API 提供的响应中提取，不应修改
    //tweet.fields:选填，这领域参数使您能够选择特定的推文字段将在每个返回的 Tweet 对象中传递。
    //expansions:选填，使你能够请求与最初返回的推文相关的其他数据对象
    //media.fields:选填，能够选择特定的媒体字段将在每条返回的推文中传递
    //place.fields:选填，使您能够选择特定的放置字段将在每条返回的推文中传递
    //poll.fields:选填，能够选择特定的轮询字段将在每条返回的推文中传递
    //user.fields:选填，能够选择特定的推文字段将在每个返回的 Tweet 对象中传递
    //DOC: https://api.twitter.com/2/problems/invalid-request
     * author：wh
     */
    function search($params){
        //got params must post
        $res = $this->request($params);
        if(empty($res['data'])){
            return json_encode(['data'=>$res,JSON_UNESCAPED_UNICODE]);
        }

        return json_encode($res,JSON_UNESCAPED_UNICODE);
    }

    /**
     * desc：
     * author：wh
     * got params must post
     */
    function request($params){
        $bearer_token = $this->bearer_token;

        $url = $this->url;
        $headers = [
            "Authorization: Bearer $bearer_token",
        ];
        $query_str = http_build_query($params);

        return $this->curl_request($url.'?'.$query_str,'GET',$params,$headers);
    }


    private function curl_request($url, $method = 'GET',$data=null,$header=array(),$call_back=null)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        if($header){
            curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
        }
        if($method == 'POST'){
            if($data) curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        }
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $method);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        if($call_back){
            curl_setopt($ch, CURLOPT_WRITEFUNCTION, $call_back);
        }
        $result = curl_exec($ch);
        if (curl_errno($ch)) {
            return [
                'status' => 'error',
                'message' => 'curl 错误信息: ' . curl_error($ch)
            ];
        }
        curl_close($ch);
        return $result;
    }
}