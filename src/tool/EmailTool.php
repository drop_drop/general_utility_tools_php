<?php
/*
 * description：
 * author：wh
 * email：
 * createTime：{2022/6/15} {16:16} 
 */

namespace wanghua\general_utility_tools_php\tool;


use wanghua\general_utility_tools_php\phpmailer\AliYunEmail;
use wanghua\general_utility_tools_php\log\driver\Email;
use wanghua\general_utility_tools_php\SundryConfig;
use wanghua\general_utility_tools_php\tool\Tools;

class EmailTool
{


    /**
     * @deprecated 不推荐
     *
     * desc：基础邮件
     *
     * 格式自由组合
     *
     * author：wh
     * @param string $title
     * @param string $body
     * @return array
     */
    static function log_email(string $title,string $body){
        $log_file = '';
        try {
            //服务器配置 start
            $send_server_nickname = SundryConfig::val('send_server_nickname');
            $send_server_username = SundryConfig::val('send_server_username');
            $send_server_pwd = SundryConfig::val('send_server_pwd');
            $send_server_host = SundryConfig::val('send_server_host');
            //服务器配置 end
            if(empty($send_server_nickname)){
                Tools::log_to_write_txt(['服务器配置错误.发件服务器昵称为空.'],$log_file);
                return Tools::set_res(500,'服务器配置错误.发件服务器昵称为空');
            }
            if(empty($send_server_username)){
                Tools::log_to_write_txt(['服务器配置错误.发件服务器邮箱为空.'],$log_file);
                return Tools::set_res(500,'服务器配置错误.发件服务器邮箱为空');
            }
            if(empty($send_server_pwd)){
                Tools::log_to_write_txt(['服务器配置错误.发件服务器密码为空.'],$log_file);
                return Tools::set_res(500,'服务器配置错误.发件服务器密码为空.');
            }
            if(empty($send_server_host)){
                Tools::log_to_write_txt(['服务器配置错误.发件服务器HOST为空.'],$log_file);
                return Tools::set_res(500,'服务器配置错误.发件服务器HOST为空..');
            }

            //收件人信息 start
            $receiver = explode(',',SundryConfig::val('admin_error_log_email'));//逗号隔开一次发送多个人
            $receiver_nickname = '日志管理员';//默认收件人昵称
            //收件人信息 end
            if(empty($send_server_host)){
                Tools::log_to_write_txt(['服务器配置错误.发件服务器HOST为空.'],$log_file);
                return Tools::set_res(500,'服务器配置错误.发件服务器HOST为空..');
            }

            $email_obj = new Email();
            $email_obj->set_email_config($send_server_nickname,$send_server_username,$send_server_pwd,$send_server_host);
            $email_obj->set_receiver_config($receiver,$receiver_nickname);

            //写入日志
            return $email_obj->write_text($title,$body);
        }catch (\Exception $e){
            Tools::log_to_write_txt([
                'error'=>'系统未来通用统一日志记录，出错.'.$e->getMessage(),
                'title'=>$title,
                'body'=>$body,
                'error_info'=>$e->getTraceAsString()],$log_file);
            return ['code'=>500,'msg'=>'日志写入异常.'.$e->getMessage()];
        }
    }

    /**
     * @deprecated 不推荐
     *
     * desc：向收件人发邮件
     *
     * author：wh
     * @param string $title 标题
     * @param string $body 内容
     * @param $receiver 多个收件人用逗号隔开
     * @param string $receiver_nickname 收件人昵称
     * @return array
     */
    static function log_email_to_person(string $title,string $body,$receiver,$receiver_nickname='默认'){
        $log_file = '';
        try {
            //服务器配置 start
            $send_server_nickname = SundryConfig::val('send_server_nickname');
            $send_server_username = SundryConfig::val('send_server_username');
            $send_server_pwd = SundryConfig::val('send_server_pwd');
            $send_server_host = SundryConfig::val('send_server_host');
            //服务器配置 end
            if(empty($send_server_nickname)){
                Tools::log_to_write_txt(['服务器配置错误.发件服务器昵称为空.'],$log_file);
                return Tools::set_res(500,'服务器配置错误.发件服务器昵称为空');
            }
            if(empty($send_server_username)){
                Tools::log_to_write_txt(['服务器配置错误.发件服务器邮箱为空.'],$log_file);
                return Tools::set_res(500,'服务器配置错误.发件服务器邮箱为空');
            }
            if(empty($send_server_pwd)){
                Tools::log_to_write_txt(['服务器配置错误.发件服务器密码为空.'],$log_file);
                return Tools::set_res(500,'服务器配置错误.发件服务器密码为空.');
            }
            if(empty($send_server_host)){
                Tools::log_to_write_txt(['服务器配置错误.发件服务器HOST为空.'],$log_file);
                return Tools::set_res(500,'服务器配置错误.发件服务器HOST为空..');
            }

            //收件人信息 start
            $receiver = explode(',',SundryConfig::val('admin_error_log_email'));//逗号隔开一次发送多个人
            $receiver_nickname = '日志管理员';//默认收件人昵称
            //收件人信息 end
            if(empty($send_server_host)){
                Tools::log_to_write_txt(['服务器配置错误.发件服务器HOST为空.'],$log_file);
                return Tools::set_res(500,'服务器配置错误.发件服务器HOST为空..');
            }

            $email_obj = new Email();
            $email_obj->set_email_config($send_server_nickname,$send_server_username,$send_server_pwd,$send_server_host);
            $email_obj->set_receiver_config($receiver,$receiver_nickname);

            //写入日志
            return $email_obj->write_text($title,$body);
        }catch (\Exception $e){
            Tools::log_to_write_txt([
                'error'=>'系统未来通用统一日志记录，出错.'.$e->getMessage(),
                'title'=>$title,
                'body'=>$body,
                'error_info'=>$e->getTraceAsString()],$log_file);
            return ['code'=>500,'msg'=>'日志写入异常.'.$e->getMessage()];
        }
    }

    /**
     * desc：【推荐】发送国内邮件，支持批量发送
     *
     * author：wh
     * @param string $title 标题
     * @param string $body 内容
     * @param $receiver 多个收件人用逗号隔开
     * @param string $receiver_nickname 收件人昵称
     * @return array
     */
    static function email_to_person(string $title,string $body,$receiver,$receiver_nickname='默认'){
        $log_file = '';
        try {
            //服务器配置 start
            $send_server_nickname = SundryConfig::val('send_server_nickname');
            $send_server_username = SundryConfig::val('send_server_username');
            $send_server_pwd = SundryConfig::val('send_server_pwd');
            $send_server_host = SundryConfig::val('send_server_host');
            //服务器配置 end
            if(empty($send_server_nickname)){
                Tools::log_to_write_txt(['服务器配置错误.发件服务器昵称为空.'],$log_file);
                return Tools::set_res(500,'服务器配置错误.发件服务器昵称为空');
            }
            if(empty($send_server_username)){
                Tools::log_to_write_txt(['服务器配置错误.发件服务器邮箱为空.'],$log_file);
                return Tools::set_res(500,'服务器配置错误.发件服务器邮箱为空');
            }
            if(empty($send_server_pwd)){
                Tools::log_to_write_txt(['服务器配置错误.发件服务器密码为空.'],$log_file);
                return Tools::set_res(500,'服务器配置错误.发件服务器密码为空.');
            }
            if(empty($send_server_host)){
                Tools::log_to_write_txt(['服务器配置错误.发件服务器HOST为空.'],$log_file);
                return Tools::set_res(500,'服务器配置错误.发件服务器HOST为空..');
            }

            //收件人信息 start
            $receiver = explode(',',$receiver);//逗号隔开一次发送多个人
            //$receiver_nickname = '日志管理员';//默认收件人昵称
            //收件人信息 end
            if(empty($send_server_host)){
                Tools::log_to_write_txt(['服务器配置错误.发件服务器HOST为空.'],$log_file);
                return Tools::set_res(500,'服务器配置错误.发件服务器HOST为空..');
            }

            $email_obj = new Email();
            $email_obj->set_email_config($send_server_nickname,$send_server_username,$send_server_pwd,$send_server_host);
            $email_obj->set_receiver_config($receiver,$receiver_nickname);

            //写入日志
            return $email_obj->write_text($title,$body);
        }catch (\Exception $e){
            Tools::log_to_write_txt([
                'error'=>'系统未来通用统一日志记录，出错.'.$e->getMessage(),
                'title'=>$title,
                'body'=>$body,
                'error_info'=>$e->getTraceAsString()],$log_file);
            return ['code'=>500,'msg'=>'日志写入异常.'.$e->getMessage()];
        }
    }

    /**
     * desc：发送亚马逊邮件（如果一个项目有多个邮件发送服务器请使用Email类发送,不使用本封装工具）
     * author：wh
     * @param string $title
     * @param string $body
     * @param $sender_email 发件人邮箱
     * @param $receiver 收件人邮箱
     * @param string $receiver_nickname 收件人昵称
     * @return array
     */
    static function aws_to_person(string $title,string $body,$receiver,$receiver_nickname='默认'){
        $log_file = '';
        try {
            //服务器配置 start
            $send_server_nickname = SundryConfig::val('send_server_nickname');
            $send_server_username = SundryConfig::val('send_server_username');
            $send_server_pwd = SundryConfig::val('send_server_pwd');
            $send_server_host = SundryConfig::val('send_server_host');
            $sender_email = SundryConfig::val('sender_email');
            //服务器配置 end
            if(empty($send_server_nickname)){
                Tools::log_to_write_txt(['服务器配置错误.发件服务器昵称为空.'],$log_file);
                return Tools::set_res(500,'服务器配置错误.发件服务器昵称为空');
            }
            if(empty($send_server_username)){
                Tools::log_to_write_txt(['服务器配置错误.发件服务器邮箱为空.'],$log_file);
                return Tools::set_res(500,'服务器配置错误.发件服务器邮箱为空');
            }
            if(empty($send_server_pwd)){
                Tools::log_to_write_txt(['服务器配置错误.发件服务器密码为空.'],$log_file);
                return Tools::set_res(500,'服务器配置错误.发件服务器密码为空.');
            }
            if(empty($send_server_host)){
                Tools::log_to_write_txt(['服务器配置错误.发件服务器HOST为空.'],$log_file);
                return Tools::set_res(500,'服务器配置错误.发件服务器HOST为空..');
            }
            if(empty($sender_email)){
                Tools::log_to_write_txt(['服务器配置错误.发件方邮箱为空.'],$log_file);
                return Tools::set_res(500,'服务器配置错误.发件方邮箱为空..');
            }

            //收件人信息 start
            $receiver = explode(',',$receiver);//逗号隔开一次发送多个人
            //$receiver_nickname = '日志管理员';//默认收件人昵称
            //收件人信息 end
            if(empty($send_server_host)){
                Tools::log_to_write_txt(['服务器配置错误.发件服务器HOST为空.'],$log_file);
                return Tools::set_res(500,'服务器配置错误.发件服务器HOST为空..');
            }

            $email_obj = new Email();
            $email_obj->port = 587;//STARTTLS 端口25、587 或 2587；自定义 SSL 客户端支持：TLS 包装器端口465 或 2465
            $email_obj->protocol = 'TLS';//固定
            $email_obj->sender_email = $sender_email;
            $email_obj->set_email_config($send_server_nickname,$send_server_username,$send_server_pwd,$send_server_host);
            $email_obj->set_receiver_config($receiver,$receiver_nickname);

            //写入日志
            return $email_obj->write_text_aws($title,$body);
        }catch (\Exception $e){
            Tools::log_to_write_txt([
                'error'=>'系统未来通用统一日志记录，出错.'.$e->getMessage(),
                'title'=>$title,
                'body'=>$body,
                'error_info'=>$e->getTraceAsString()],$log_file);
            return ['code'=>500,'msg'=>'日志写入异常.'.$e->getMessage()];
        }
    }

    /**
     * desc：【推荐】错误、异常邮件，固定模板
     *
     * author：wh
     * @param string $title
     * @param string $error_title
     * @param string $error_info
     * @return array
     */
    static function log_error_email(string $title, string $error_title,string $error_info){
        $log_file = '';
        try {
            //服务器配置 start
            $send_server_nickname = SundryConfig::val('send_server_nickname');
            $send_server_username = SundryConfig::val('send_server_username');
            $send_server_pwd = SundryConfig::val('send_server_pwd');
            $send_server_host = SundryConfig::val('send_server_host');
            //服务器配置 end
            if(empty($send_server_nickname)){
                Tools::log_to_write_txt(['服务器配置错误.发件服务器昵称为空.'],$log_file);
                return Tools::set_res(500,'服务器配置错误.发件服务器昵称为空');
            }
            if(empty($send_server_username)){
                Tools::log_to_write_txt(['服务器配置错误.发件服务器邮箱为空.'],$log_file);
                return Tools::set_res(500,'服务器配置错误.发件服务器邮箱为空');
            }
            if(empty($send_server_pwd)){
                Tools::log_to_write_txt(['服务器配置错误.发件服务器密码为空.'],$log_file);
                return Tools::set_res(500,'服务器配置错误.发件服务器密码为空.');
            }
            if(empty($send_server_host)){
                Tools::log_to_write_txt(['服务器配置错误.发件服务器HOST为空.'],$log_file);
                return Tools::set_res(500,'服务器配置错误.发件服务器HOST为空..');
            }

            //收件人信息 start
            $receiver = SundryConfig::val('admin_error_log_email');
            $receiver_nickname = '日志管理员';//默认收件人昵称
            //收件人信息 end
            if(empty($send_server_host)){
                Tools::log_to_write_txt(['服务器配置错误.发件服务器HOST为空.'],$log_file);
                return Tools::set_res(500,'服务器配置错误.发件服务器HOST为空..');
            }

            $email_obj = new Email();
            $email_obj->set_email_config($send_server_nickname,$send_server_username,$send_server_pwd,$send_server_host);
            $email_obj->set_receiver_config(explode(',',$receiver),$receiver_nickname);

            $err_json_input = json_encode(input(),JSON_UNESCAPED_UNICODE);
            $url = request()->url(true);
            $body = <<<EOF
<p>错误标题：$error_title</p>
<p>输入参数(json后)：</p>
<p>$err_json_input</p>
<p>触发url地址：</p>
<p>$url</p>
<p>详细错误信息：</p>
<p>$error_info</p>
EOF;

            //写入日志
            return $email_obj->write_text($title,$body);
        }catch (\Exception $e){
            Tools::log_to_write_txt([
                'error'=>'系统未来通用统一日志记录，出错.'.$e->getMessage(),
                'title'=>$title,
                'body'=>$error_info,
                'error_info'=>$e->getTraceAsString()],$log_file);
            return ['code'=>500,'msg'=>'日志写入异常.'.$e->getMessage()];
        }
    }

    /**
     * desc：通过阿里云邮件服务器发送邮件
     * author：wh
     */
    static function sendAliEmail($toAddress,$subject,$content){
        $config = config('aliyun_oss_config');
        return (new AliYunEmail($config))->sendMail($toAddress,$subject,$content);
    }
}