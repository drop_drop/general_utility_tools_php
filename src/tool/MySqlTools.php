<?php
/*
 * description：
 * author：wh
 * email：
 * createTime：{2021/09/15} {11:34} 
 */

namespace wanghua\general_utility_tools_php\tool;

use think\Db;

/**
 * MySQL工具箱
 *
 * 适用环境：thinkPHP5.0+,php7.1+
 *
 * 不依赖第三方类库
 *
 */
class MySqlTools
{

    public function __construct()
    {

    }

    /**
     *
     * desc：构建统一的表名配置
     *
     * author：wh
     *
     * 使用
     * eg：
     *
     */
    function buildTablesConf(string $file_path = ''){
        $sql = "show tables;";
        $tables = Db::query($sql);

        $field = 'Tables_in_'.config('database.database');

        $tables_build_file_path = $file_path?$file_path:Tools::get_root_path().'application/common/model/TabConf.php';


        $tab_conf_code_str = <<<EOF
<?php

namespace app\common\model;

class TabConf
{

EOF;


        foreach ($tables as $table){
            $tmp_table = $table[$field];
            $comments = Tools::get_table_comments($tmp_table);
            $tab_conf_code_str .= <<<EOF

    /** 
    * $comments  
    */
    static $$tmp_table = '$tmp_table';


EOF;
        }

        $tab_conf_code_str.='}';

        file_put_contents($tables_build_file_path,$tab_conf_code_str);

    }
}