<?php
/*
 * description：
 * author：wh
 * email：
 * createTime：{2024/2/19} {11:33} 
 */

namespace wanghua\general_utility_tools_php\tool;


/**
 * 依赖库：doing/phpqrcode
 *
 * composer require doing/phpqrcode dev-master
 *
 * Class Qrcode
 * @package wanghua\general_utility_tools_php\tool
 */
class Qrcode
{
    public $size = 2;
    /**
     * 生成带logo的二维码
     * author：wh
     * doc: https://packagist.org/packages/doing/phpqrcode
     * @param string $str 二维码的内容
     * @param string $qr_file_name 用户username作为图片名称
     * @param string $logo 真实物理路径 必须
     * @throws \Exception
     */
    function createQrcode(string $str,string $qr_file_name, string $logo=''){

        $out_path = 'uploads/qr_images/';
        if(!file_exists($out_path)){
            mkdir($out_path,0777,true);
        }
        $qrname = $qr_file_name.'.png';
        //二维码导出的储存地址
        $outfile = $out_path.$qrname;
        //二维码的大小
        $size = $this->size;

        //调用方法成功后,会在相应文件夹下生成二维码文件
        \PHPQRCode\QRcode::png($str, $outfile, $size);

        //$QR调用png生成的二维码全路径
        $QR = Tools::get_root_path().'public/uploads/qr_images/'.$qrname;
        //$header头像全路径:头像一定是正方形
        $header = $logo;
        $QR = \PHPQRCode\QRcode::addHeader($header, $QR);

        return $outfile;
    }

    /**
     * desc：生成不带logo的二维码
     *
     * author：wh
     * @param string $content 二维码的内容
     * @param string $qr_file_name 二维码图片文件名称(一般为用户唯一id)
     * @return string
     */
    function generateQrcode($content,$qr_file_name='')
    {
        if(empty($qr_file_name)){
            $qr_file_name = 'qrcode_'.time().rand(1000,9999);
        }
        $out_path = 'uploads/qr_images/';
        if(!file_exists($out_path)){
            mkdir($out_path,0777,true);
        }
        $qrname = $qr_file_name.'.png';
        //二维码导出的储存地址
        $outfile = $out_path.$qrname;
        //二维码的大小
        $size = $this->size;

        //调用方法成功后,会在相应文件夹下生成二维码文件
        \PHPQRCode\QRcode::png($content, $outfile, $size);
        return $outfile;
    }
}