<?php
/*
 * description：
 * author：wh
 * email：
 * createTime：{2024/6/24} {23:01} 
 */

namespace wanghua\general_utility_tools_php\tool;


use wanghua\general_utility_tools_php\http\Curl;
use wanghua\general_utility_tools_php\Mmodel;

class KuaidiTools
{
    /**
     * desc：查询物流平台
     * author：wh
     * @param string $num 物流单号
     * @return array
     */
    function getPlatform($num){
        return Mmodel::catch(function (){
            $url = "https://alayn.baidu.com/express/appdetail/get_com?num=315002419145908";
            $res = Curl::curl_get($url);
            if($res['code'] != 200){
                return Tools::set_res(500,'查询失败.'.$res['msg']);
            }
            $res_data = json_decode($res['data'],true);
            if($res_data['code']){
                return Tools::set_res(500,'查询失败.'.$res_data['message']);
            }
            return Tools::set_ok('ok',$res_data['data']);
        });
    }

    /**
     * desc：查询物流
     * {
    "msg": "",
    "status": "0",
    "error_code": "0",
    "data": {
    "context": [{
    "time": "1719059497",
    "desc": "【代收点】您的快件已签收，签收人在 兴科五路81妈妈驿站(重庆市市辖区渝北区兴科五路81号)领取，投诉电话：13274912700"
    }, {
    "time": "1718864720",
    "desc": "【代收点】您的快件已暂存至 兴科五路81妈妈驿站，地址：重庆市市辖区渝北区兴科五路81号，请及时领取签收，如有疑问请电联快递员：高邦明(13224040149) ，投诉电话:13274912700"
    }, {
    "time": "1718861834",
    "desc": "【重庆市】重庆渝北区西政公司[023-88972584] 快递员 高邦明(13224040149) 正在为您派送【95121为韵达快递员外呼专属号码，请放心接听】"
    }, {
    "time": "1718861796",
    "desc": "【重庆市】已到达 重庆渝北区西政公司[023-88972584]"
    }, {
    "time": "1718861793",
    "desc": "【重庆市】重庆渝北区西政公司[023-88972584] 快递员 高邦明(13224040149) 正在为您派送【95121为韵达快递员外呼专属号码，请放心接听】"
    }, {
    "time": "1718861755",
    "desc": "【重庆市】已到达 重庆渝北区西政公司[023-88972584]"
    }, {
    "time": "1718861748",
    "desc": "【重庆市】已离开 重庆市渝北区西政网格仓；发往 重庆渝北区西政公司"
    }, {
    "time": "1718841311",
    "desc": "【重庆市】已离开 重庆分拨交付中心；发往 重庆渝北区西政公司"
    }, {
    "time": "1718840136",
    "desc": "【重庆市】已到达 重庆分拨交付中心"
    }, {
    "time": "1718714128",
    "desc": "【潍坊市】已离开 山东潍坊分拨交付中心；发往 重庆分拨交付中心"
    }, {
    "time": "1718713803",
    "desc": "【潍坊市】已到达 山东潍坊分拨交付中心"
    }, {
    "time": "1718707534",
    "desc": "【潍坊市】山东潍坊奎文保税区公司-马维华（16602197439） 已揽收"
    }],
    "status": "1",
    "state": "3",
    "officalService": {
    "com": "韵达",
    "comName": "韵达速递",
    "urlDesc": "官网地址：",
    "url": "http://www.yundaex.com",
    "logo": "https://express-server.bj.bcebos.com/express-brand-card/head/rIcon/yunda.png",
    "servicePhoneDesc": "官方电话：",
    "servicePhone": "95546",
    "service": [{
    "url": "http://www.yundaex.com/cn/fuwuwangdian.php",
    "name": "网点查询"
    }, {
    "url": "http://www.yundaex.com/cn/product_yunfei.php",
    "name": "运价查询"
    }, {
    "url": "http://membernew.yundasys.com:15116/member.website/hywz/view/shipping.html",
    "name": "我要寄件"
    }, {
    "url": "http://www.yundaex.com/cn/jinji.php",
    "name": "禁限寄"
    }]
    }
    }
    }
     * author：wh
     * @return array 返回data下面context字段二维数组记录
     */
    function query($num){
        return Mmodel::catch(function () use($num){
            $plt = $this->getPlatform($num);
            if($plt['code'] != 200){
                return Tools::set_res(500,'查询失败.'.$plt['msg']);
            }
            $company = $plt['data']['company'];
            $url = "https://alayn.baidu.com/express/appdetail/get_detail?query_from_srcid=51151&tokenV2=OLRFkDonXGP%2FQTmCKNu41zXJayuyUkfll0i7Tw5IWBFAsbTSXMycQooMPzAMO1Rc&appid=4001&nu={$num}&com={$company}&qid=4879176651996235000";
            $res = Curl::curl_get($url);
            if($res['code'] != 200){
                return Tools::set_res($res['code'],'请求失败.'.$res['msg']);
            }
            $res_data = json_decode($res['data'],true);
            if($res_data['error_code']){
                return Tools::set_res($res_data['error_code'],'查询失败.'.$res_data['msg']);
            }
            return Tools::set_ok('ok',$res_data['data']['context']);
        });
    }


    /**
     * desc：查询爱查快递接口
     * {
    "mailNo": "315002419145908:6033",
    "status": 3,
    "errCode": 0,
    "message": "",
    "expSpellName": "yunda",
    "expTextName": "韵达快递",
    "tel": "95546",
    "data": [
    {
    "time": "2024-06-18 18:45",
    "context": "【潍坊市】山东潍坊奎文保税区公司-马维华（16602197439） 已揽收"
    },
    {
    "time": "2024-06-18 20:30",
    "context": "【潍坊市】已到达 山东潍坊分拨交付中心"
    },
    {
    "time": "2024-06-18 20:35",
    "context": "【潍坊市】已离开 山东潍坊分拨交付中心；发往 重庆分拨交付中心"
    },
    {
    "time": "2024-06-20 07:35",
    "context": "【重庆市】已到达 重庆分拨交付中心"
    },
    {
    "time": "2024-06-20 07:55",
    "context": "【重庆市】已离开 重庆分拨交付中心；发往 重庆渝北区西政公司"
    },
    {
    "time": "2024-06-20 13:35",
    "context": "【重庆市】已离开 重庆市渝北区西政网格仓；发往 重庆渝北区西政公司"
    },
    {
    "time": "2024-06-20 13:35",
    "context": "【重庆市】已到达 重庆渝北区西政公司[023-88972584]"
    },
    {
    "time": "2024-06-20 13:36",
    "context": "【重庆市】重庆渝北区西政公司[023-88972584] 快递员 高邦明(13224040149) 正在为您派送【95121为韵达快递员外呼专属号码，请放心接听】"
    },
    {
    "time": "2024-06-20 13:36",
    "context": "【重庆市】已到达 重庆渝北区西政公司[023-88972584]"
    },
    {
    "time": "2024-06-20 13:37",
    "context": "【重庆市】重庆渝北区西政公司[023-88972584] 快递员 高邦明(13224040149) 正在为您派送【95121为韵达快递员外呼专属号码，请放心接听】"
    },
    {
    "time": "2024-06-20 14:25",
    "context": "【代收点】您的快件已暂存至 兴科五路81妈妈驿站，地址：重庆市市辖区渝北区兴科五路81号，请及时领取签收，如有疑问请电联快递员：高邦明(13224040149) ，投诉电话:13274912700"
    },
    {
    "time": "2024-06-22 20:31",
    "context": "【代收点】您的快件已签收，签收人在 兴科五路81妈妈驿站(重庆市市辖区渝北区兴科五路81号)领取，投诉电话：13274912700"
    }
    ],
    "update": 1719293097,
    "cache": 10576,
    "lang": "zh",
    "ord": "ASC"
    }
     * author：wh
     * @param $num 快递单号
     * @param $phone_end4 手机后四位
     * @return array 返回data字段二维数组记录
     */
    function queryackd($num,$phone_end4){
        return Mmodel::catch(function () use($num,$phone_end4){
            $ran_time = mt_rand(1000000,2000000);
            $tm = Tools::getMillisecond();
            $_ = $tm+$ran_time;
            usleep($ran_time);//随机暂停1-2秒（伪装）
            $tk = Tools::rand_str();

            //快递平台
            $plt = $this->getPlatform($num);
            if($plt['code'] != 200){
                return Tools::set_res(500,'查询失败.'.$plt['msg']);
            }
            $company = $plt['data']['company'];

            $url = "https://trace.fkdex.com/{$company}/{$num}:{$phone_end4}?mailNo=315002419145908%3A6033&spellName=&exp-textName=&tk={$tk}&tm={$tm}&_={$_}";
            $res = Curl::curl_get($url);
            if($res['code'] != 200){
                return Tools::set_res($res['code'],'查询失败.'.$res['msg']);
            }
            if(empty($res['data'])){
                return Tools::set_fail('未查询到数据');
            }
            $res_data = json_decode($res['data'],true);
            if($res_data['errCode']){
                return Tools::set_res($res_data['errCode'],'查询失败.'.$res_data['message']);
            }
            return Tools::set_ok('ok',$res_data['data']);
        });
    }
}