<?php
/*
 * description：
 * author：wh
 * email：
 * createTime：{2024/7/9} {0:43} 
 */

namespace wanghua\general_utility_tools_php\alibaba\distributes\strict;

use wanghua\general_utility_tools_php\alibaba\distributes\BaseStrict;

class CustomerService extends BaseStrict
{

    /**
     * desc：获取唤起旺旺聊天的链接
     * author：wh
     * @param $toOpenUid
     * @return mixed
     */
    function getCustomerServiceLink($toOpenUid){
        $urlPath = "param2/1/com.alibaba.account/account.wangwangUrl.get/".$this->authObj->config['appkey'];

        return $this->authObj->request($urlPath, ['toOpenUid'=>$toOpenUid]);
    }
}