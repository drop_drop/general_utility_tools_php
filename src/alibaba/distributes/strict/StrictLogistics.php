<?php
/*
 * description：
 * author：wh
 * email：
 * createTime：{2024/6/30} {1:54} 
 */

namespace wanghua\general_utility_tools_php\alibaba\distributes\strict;

use wanghua\general_utility_tools_php\alibaba\distributes\BaseStrict;

/**
 * 物流
 * Class StrictLogistics
 * @package app\index\logic\alibaba\distributes\strict
 */
class StrictLogistics extends BaseStrict
{

    /**
     * 获取物流模板详情
     *
     * 根据物流模版ID获取卖家的物流模板。运费模板ID为0表示运费说明，为1表示卖家承担运费
     *
     * doc:https://open.1688.com/api/apidocdetail.htm?id=com.alibaba.logistics:alibaba.logistics.myFreightTemplate.list.get-1&aopApiCategory=Logistics_NEW
     */
    function getLogisticsTemplateDetail($template_id){
        $urlPath = "param2/1/com.alibaba.logistics/alibaba.logistics.myFreightTemplate.list.get/".$this->authObj->config['appkey'];

        return $this->authObj->request($urlPath, ['templateId'=>$template_id]);
    }
    function getLogisticsTemplate(){
        $urlPath = "param2/1/com.alibaba.logistics/alibaba.logistics.myFreightTemplate.list.get/".$this->authObj->config['appkey'];

        return $this->authObj->request($urlPath, []);
    }

    /**
     * desc： 获取物流公司列表
     *
     * 物流公司列表-自联物流
     *
     * doc:https://open.1688.com/api/apidocdetail.htm?id=com.alibaba.logistics:alibaba.logistics.OpQueryLogisticCompanyList.offline-1&aopApiCategory=Logistics_NEW
     * author：wh
     */
    function getLogisticsCompanyList(){
        $urlPath = "param2/1/com.alibaba.logistics/alibaba.logistics.OpQueryLogisticCompanyList.offline/".$this->authObj->config['appkey'];

        return $this->authObj->request($urlPath, []);
    }
}