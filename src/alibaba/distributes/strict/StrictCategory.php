<?php
/*
 * description：
 * author：wh
 * email：
 * createTime：{2024/6/29} {12:12} 
 */

namespace wanghua\general_utility_tools_php\alibaba\distributes\strict;


use wanghua\general_utility_tools_php\alibaba\distributes\BaseStrict;

/**
 * 根据类目Id查询类目
 *
 * 传0获取所有一级类目，循环调用可以获取类目树
 *
 * 注意：类目id,必须大于等于0， 如果为0，则查询所有一级类目
 *
 * 类目查询。如果需要获取所有1688类目信息，需要从根类目开始遍历获取整个类目树。
 * 即：先传0获取所有一级类目ID，然后在通过获取到的一级类目ID遍历获取所二级类目，最后通过遍历二级类目ID获取三级类目。
 * 注意：1688类目仅三级，三级类目即发布商品所需的叶子类目。
 *
 * doc: https://open.1688.com/api/apidocdetail.htm?id=com.alibaba.product:alibaba.category.get-1&aopApiCategory=category_new
 * Class StrictCategory
 * @package app\index\logic\alibaba\distributes\strict
 */
class StrictCategory extends BaseStrict
{

    public function __construct($authObj)
    {
        parent::__construct($authObj);
    }

    /**
     * desc：获取严选一级分类（顶级分类）
     * author：wh
     * @return mixed
     */
    function getRootCategory(){
        //$base_url = "https://gw.open.1688.com/openapi/";
        $urlPath = "param2/1/com.alibaba.product/alibaba.category.get/".$this->authObj->config['appkey'];

        $request_data = [];
        $request_data['categoryID'] = '0';
        return $this->authObj->request($urlPath,$request_data);
    }

    /**
     * desc：根据类目id获取类目信息
     * author：wh
     * @param $category_id
     * @return mixed
     */
    function getCategoryByCID($category_id){
        $urlPath = "param2/1/com.alibaba.product/alibaba.category.get/".$this->authObj->config['appkey'];
        $request_data = [];
        $request_data['categoryID'] = $category_id;
        return $this->authObj->request($urlPath,$request_data);
    }
}