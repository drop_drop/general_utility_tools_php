<?php
/*
 * description：
 * author：wh
 * email：
 * createTime：{2024/6/30} {12:14} 
 */

namespace wanghua\general_utility_tools_php\alibaba\distributes\strict;


use wanghua\general_utility_tools_php\alibaba\distributes\BaseStrict;
/**
 * 支付相关
 *
 * Class StrictPay
 * @package app\index\logic\alibaba\distributes\strict
 */
class StrictPay extends BaseStrict
{

    /**
     * desc： 查询订单是否开通免密支付
     * author：wh
     */
    function queryOrderIsNoPassPay(){
        $urlPath = "param2/1/com.alibaba.trade/alibaba.trade.pay.protocolPay.isopen/".$this->authObj->config['appkey'];
        $request_data = [];

        $res = $this->authObj->request($urlPath, $request_data);
        return $res;
    }


    /**
     * desc：查询订单可以支持的支付渠道
     * author：wh
     * @param string $order_id
     */
    function queryOrderPayChannel(string $order_id){
        $urlPath = "param2/1/com.alibaba.trade/alibaba.trade.payWay.query/".$this->authObj->config['appkey'];
        $request_data = [];
        $request_data['orderId'] = $order_id;
        $res = $this->authObj->request($urlPath, $request_data);
        return $res;
    }

    /**
     * desc：组合收银台url获取
     * author：wh
     * @param string $orderIds 订单列表 格式参考 字符串格式的数组：["123456789","123456789"]
     * @param string $payPlatformType PC或WIRELESS
     * @return mixed
     */
    function getPayUrl(string $orderIds,string $payPlatformType){
        $urlPath = "param2/1/com.alibaba.trade/alibaba.trade.grouppay.url.get/".$this->authObj->config['appkey'];
        $request_data = [];
        $request_data['orderIds'] = $orderIds;//json_encode($orderIds);
        $request_data['payPlatformType'] = $payPlatformType;//PC或WIRELESS
        $res = $this->authObj->request($urlPath, $request_data);
        return $res;
    }
}