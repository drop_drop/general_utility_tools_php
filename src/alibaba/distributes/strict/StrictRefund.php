<?php
/*
 * description：
 * author：wh
 * email：
 * createTime：{2024/7/23} {22:57} 
 */

namespace wanghua\general_utility_tools_php\alibaba\distributes\strict;


use wanghua\general_utility_tools_php\alibaba\distributes\BaseStrict;
use wanghua\general_utility_tools_php\tool\Tools;

class StrictRefund extends BaseStrict
{

    /**
     * 创建退款退货申请
     * https://open.1688.com/api/apidocdetail.htm?id=com.alibaba.trade:alibaba.trade.createRefund-1
     */
    function createRefund($params){
        $urlPath = "param2/1/com.alibaba.trade/alibaba.trade.createRefund/".$this->authObj->config['appkey'];
        $request_data = [];

        //主订单
        if(empty($params['orderId'])){
            return Tools::set_fail('orderId不能为空');
        }
        $request_data['orderId'] = $params['orderId'];
        //子订单 订单接口 productItems- subItemID 就是子订单ID，
        // 卖家拒绝退款后无法重新申请，需要到1688后台退款单页面 修改退款协议
        if(empty($params['orderEntryIds'])){
            return Tools::set_fail('orderEntryIds不能为空');
        }
        $request_data['orderEntryIds'] = $params['orderEntryIds'];
        //退款/退款退货。只有已收到货，才可以选择退款退货 退款:"refund"; 退款退货:"returnRefund"
        if(empty($params['disputeRequest'])){
            return Tools::set_fail('disputeRequest不能为空');
        }
        $request_data['disputeRequest'] = $params['disputeRequest'];
        //退款金额（单位：分）。不大于实际付款金额；等待卖家发货时，必须为商品的实际付款金额。
        if(empty($params['applyPayment'])){
            return Tools::set_fail('applyPayment不能为空');
        }
        $request_data['applyPayment'] = $params['applyPayment'];
        //退运费金额（单位：分）。
        if(!isset($params['applyCarriage'])){
            return Tools::set_fail('applyCarriage不能为空');
        }
        $request_data['applyCarriage'] = $params['applyCarriage'];
        //退款原因id（从API getRefundReasonList获取）
        if(empty($params['applyReasonId'])){
            return Tools::set_fail('applyReasonId不能为空');
        }
        $request_data['applyReasonId'] = $params['applyReasonId'];
        //退款申请理由，2-150字
        if(empty($params['description'])){
            return Tools::set_fail('description不能为空');
        }
        $request_data['description'] = $params['description'];
        //货物状态 售中等待卖家发货:"refundWaitSellerSend"; 售中等待买家收货:"refundWaitBuyerReceive";
        // 售中已收货（未确认完成交易）:"refundBuyerReceived" 售后未收货:"aftersaleBuyerNotReceived";
        // 售后已收到货:"aftersaleBuyerReceived"
        if(empty($params['goodsStatus'])){
            return Tools::set_fail('goodsStatus不能为空');
        }
        $request_data['goodsStatus'] = $params['goodsStatus'];
        //凭证图片URLs。1-5张，必须使用API uploadRefundVoucher返回的“图片域名/相对路径”
        if(isset($params['vouchers'])){
            $request_data['vouchers'] = $params['vouchers'];
        }
        //子订单退款数量。仅在售中买家已收货（退款退货）时，可指定退货数量；默认，全部退货。[{"id":586683458996743215,"count":1}]
        if(isset($params['orderEntryCountList'])){
            $request_data['orderEntryCountList'] = $params['orderEntryCountList'];
        }

        $res = $this->authObj->request($urlPath, $request_data);
        return $res;
    }

    /**
     * 查询退款退货原因（用于创建退款退货）
     * https://open.1688.com/api/apidocdetail.htm?id=com.alibaba.trade:alibaba.trade.getRefundReasonList-1
     */
    function getRefundReasonList(int $orderId,array $orderEntryIds,$goodsStatus){
        $urlPath = "param2/1/com.alibaba.trade/alibaba.trade.getRefundReasonList/".$this->authObj->config['appkey'];

        $request_data = [];
        $request_data['orderId'] = $orderId;//主订单id 1234345
        $request_data['orderEntryIds'] = json_encode($orderEntryIds);//子订单id  Long[]
        //售中等待买家发货:”refundWaitSellerSend"; 售中等待买家收货:"refundWaitBuyerReceive";
        // 售中已收货（未确认完成交易）:"refundBuyerReceived" 售后未收货:"aftersaleBuyerNotReceived";
        // 售后已收到货:"aftersaleBuyerReceived"
        $request_data['goodsStatus'] = $goodsStatus;//货物状态
        $res = $this->authObj->request($urlPath, $request_data);
        return $res;
    }

    /**
     * @deprecated 【可在前端通过表单提交】
     *
     * 上传退款退货凭证
     *
     * https://open.1688.com/api/apidocdetail.htm?id=com.alibaba.trade:alibaba.trade.uploadRefundVoucher-1
     */
    function uploadRefundVoucher($imageData){
        //$urlPath = "param2/1/com.alibaba.trade/alibaba.trade.uploadRefundVoucher/".$this->authObj->config['appkey'];
        //$request_data = [];
        //$request_data['imageData'] = $imageData;//凭证图片数据。小于1M，jpg格式。
        //$res = $this->authObj->request($urlPath, $request_data);
        //return $res;
    }

    /**
     * 查询退款单列表(买家视角)
     * https://open.1688.com/api/apidocdetail.htm?id=com.alibaba.trade:alibaba.trade.refund.buyer.queryOrderRefundList-1&aopApiCategory=trade_new
     * 买家查看退款单列表，该接口不支持子账号查询，请使用主账号授权后查询
     */
    function queryOrderRefundList($params){
        $urlPath = "param2/1/com.alibaba.trade/alibaba.trade.refund.buyer.queryOrderRefundList/".$this->authObj->config['appkey'];
        $request_data = [];
        //订单Id
        if(isset($params['orderId'])){
            $request_data['orderId'] = $params['orderId'];
        }
        //退款申请时间（起始） 20220926114526000+0800
        if(isset($params['applyStartTime'])){
            $request_data['applyStartTime'] = $params['applyStartTime'];
        }
        //退款申请时间（截止） 20220926114526000+0800
        if(isset($params['applyEndTime'])){
            $request_data['applyEndTime'] = $params['applyEndTime'];
        }
        //退款状态列表 等待卖家同意 waitselleragree;退款成功 refundsuccess;退款关闭 refundclose;
        //待买家修改 waitbuyermodify;等待买家退货 waitbuyersend;等待卖家确认收货 waitsellerreceive
        if(isset($params['refundStatusSet'])){
            $request_data['refundStatusSet'] = $params['refundStatusSet'];
        }
        //卖家memberId
        if(isset($params['sellerMemberId'])){
            $request_data['sellerMemberId'] = $params['sellerMemberId'];
        }
        //当前页码
        if(isset($params['currentPageNum'])){
            $request_data['currentPageNum'] = $params['currentPageNum'];
        }
        //每页条数
        if(isset($params['pageSize'])){
            $request_data['pageSize'] = $params['pageSize'];
        }
        //退货物流单号（传此字段查询时，需同时传入sellerMemberId）
        if(isset($params['logisticsNo'])){
            $request_data['logisticsNo'] = $params['logisticsNo'];
            if(empty($params['sellerMemberId'])){
                return Tools::set_fail('logisticsNo必须与sellerMemberId同时传入');
            }
        }
        //退款修改时间(起始) 20220926114526000+0800
        if(isset($params['modifyStartTime'])){
            $request_data['modifyStartTime'] = $params['modifyStartTime'];
        }
        //退款修改时间(截止) 20220926114526000+0800
        if(isset($params['modifyEndTime'])){
            $request_data['modifyEndTime'] = $params['modifyEndTime'];
        }
        //1:售中退款，2:售后退款；0:所有退款单
        if(isset($params['dipsuteType'])){
            $request_data['dipsuteType'] = $params['dipsuteType'];
        }


        $res = $this->authObj->request($urlPath, $request_data);
        return $res;
    }

    /**
     * 查询退款单详情-根据订单ID（买家视角）
     * https://open.1688.com/api/apidocdetail.htm?id=com.alibaba.trade:alibaba.trade.refund.OpQueryBatchRefundByOrderIdAndStatus-1&aopApiCategory=trade_new
     */
    function queryBatchRefundByOrderIdAndStatus(int $orderId,string $queryType){
        $urlPath = "param2/1/com.alibaba.trade/alibaba.trade.refund.OpQueryBatchRefundByOrderIdAndStatus/".$this->authObj->config['appkey'];
        $request_data = [];
        //订单Id
        $request_data['orderId'] = $orderId;
        //查询类型 1：活动；3:退款成功（只支持退款中和退款成功）
        $request_data['queryType'] = $queryType;
        $res = $this->authObj->request($urlPath, $request_data);
        return $res;
    }

    /**
     * 退款单操作记录列表（买家视角）
     * https://open.1688.com/api/apidocdetail.htm?id=com.alibaba.trade:alibaba.trade.refund.OpQueryOrderRefundOperationList-1
     */
    function queryOrderRefundOperationList(string $refundId,$pageNo=1,$pageSize=100){
        $urlPath = "param2/1/com.alibaba.trade/alibaba.trade.refund.OpQueryOrderRefundOperationList/".$this->authObj->config['appkey'];
        $request_data = [];
        $request_data['refundId'] = $refundId;
        $request_data['pageNo'] = $pageNo;
        $request_data['pageSize'] = $pageSize;
        $res = $this->authObj->request($urlPath, $request_data);
        return $res;
    }
    /**
     * 查询退款单详情-根据退款单ID（买家视角）
     * https://open.1688.com/api/apidocdetail.htm?id=com.alibaba.trade:alibaba.trade.refund.OpQueryOrderRefund-1&aopApiCategory=trade_new
     */
    function opQueryOrderRefund(string $refundId,$needTimeOutInfo=false,$needOrderRefundOperation=false){
        $urlPath = "param2/1/com.alibaba.trade/alibaba.trade.refund.OpQueryOrderRefund/".$this->authObj->config['appkey'];
        $request_data = [];
        $request_data['refundId'] = $refundId;
        if(isset($needTimeOutInfo)){
            $request_data['needTimeOutInfo'] = $needTimeOutInfo;
        }
        if(isset($needOrderRefundOperation)){
            $request_data['needOrderRefundOperation'] = $needOrderRefundOperation;
        }
        $res = $this->authObj->request($urlPath, $request_data);
        return $res;
    }
    /**
     * 买家提交退款货信息
     * https://open.1688.com/api/apidocdetail.htm?id=com.alibaba.trade:alibaba.trade.refund.returnGoods-1
     */
    function buyerSubmitRefundGoodsInfo(string $refundId,string $logisticsCompanyNo,string $freightBill,string $description='',string $vouchers=''){
        $urlPath = "param2/1/com.alibaba.trade/alibaba.trade.refund.returnGoods/".$this->authObj->config['appkey'];
        $request_data = [];
        $request_data['refundId'] = $refundId;
        $request_data['logisticsCompanyNo'] = $logisticsCompanyNo;
        $request_data['freightBill'] = $freightBill;
        if($description){
            $request_data['description'] = $description;//发货说明，内容在2-200个字之间
        }
        //凭证图片URLs，必须使用API alibaba.trade.uploadRefundVoucher返回的“图片域名/相对路径”，
        //最多可上传 10 张图片 ；单张大小不超过1M；支持jpg、gif、jpeg、png、和bmp格式。
        // 请上传凭证，以便以后续赔所需（不上传将无法理赔）
        if($vouchers){
            $request_data['vouchers'] = $vouchers;//[https://cbu01.alicdn.com/img/ibank/2019/901/930/11848039109.jpg]
        }
        $res = $this->authObj->request($urlPath, $request_data);
        return $res;
    }


    /**
     * 取消退款退货申请
     * https://open.1688.com/api/apidocdetail.htm?id=com.alibaba.trade:alibaba.trade.cancelRefund-1
     */
    function cancelRefund(string $refundId){
        $urlPath = "param2/1/com.alibaba.trade/alibaba.trade.cancelRefund/".$this->authObj->config['appkey'];
        $request_data = [];
        $request_data['refundId'] = $refundId;
        $res = $this->authObj->request($urlPath, $request_data);
        return $res;
    }
}