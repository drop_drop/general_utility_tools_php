<?php
/*
 * description：
 * author：wh
 * email：
 * createTime：{2024/6/29} {10:49} 
 */

namespace wanghua\general_utility_tools_php\alibaba\distributes\strict;


use wanghua\general_utility_tools_php\alibaba\distributes\BaseStrict;
use wanghua\general_utility_tools_php\http\Curl;

/**
 * 分销严选商品
 * Class DistributeGoods
 * @package app\index\logic\alibaba\distributes
 */
class StrictGoods extends BaseStrict
{

    /**
     * desc：分销严选商品列表查询（itemId就是offerId）
     *
     * 精选货源商品列表查询（使用主搜引擎查询，和图搜、词搜能力保持一致）
     *
     * doc: https://open.1688.com/api/apidocdetail.htm?id=com.alibaba.fenxiao:jxhy.product.getPageList-1
     *
     * {"result":{"success":true,"result":{"pageIndex":1,"totalRecords":10000,"sizePerPage":20,"resultList":[{"itemId":661302333254,"imgUrl":"https://cbu01.alicdn.com/img/ibank/O1CN014hpUeJ1p572zEpP3D_!!3980345308-0-cib.jpg","title":"单据印刷进货单报销货出入库销售清单票三联据送货单二联收据批发","salesCnt90d":23305702,"maxPrice":1400,"minPrice":1400,"serviceList":[{"code":"ssbxsfh","name":"48小时发货"},{"code":"qtbh","name":"7天包换"},{"code":"shbp","name":"少货必赔"},{"code":"psbj","name":"破损包赔"}]},{"itemId":752202804373,"imgUrl":"https://cbu01.alicdn.com/img/ibank/O1CN01aB4Nfw1ZwDHm3XgM2_!!2215387823258-0-cib.jpg","title":"镭射咕卡贴纸DIY高颜值女孩手账粘贴咕咔儿童手帐镭射贴画随心配","salesCnt90d":21126765,"maxPrice":254,"minPrice":254,"serviceList":[{"code":"ssbxsfh","name":"48小时发货"},{"code":"qtbh","name":"7天包换"},{"code":"qtwlybt","name":"7天无理由包退"},{"code":"shbp","name":"少货必赔"},{"code":"gfzs","name":"官方直送"}]},{"itemId":748408396554,"imgUrl":"https://cbu01.alicdn.com/img/ibank/O1CN01Errsm42992vah85j2_!!991158024-0-cib.jpg","title":"亚马逊厂家批发儿童图钉按钉胶水胶软木板钉透明平头圆钉量大从优","salesCnt90d":17682951,"maxPrice":410,"minPrice":410,"serviceList":[{"code":"ssbxsfh","name":"48小时发货"},{"code":"qtbh","name":"7天包换"},{"code":"qtwlybt","name":"7天无理由包退"},{"code":"shbp","name":"少货必赔"}]},{"itemId":676794779579,"imgUrl":"https://cbu01.alicdn.com/img/ibank/O1CN010jwRRT2MOhTGiNMIE_!!2213903619818-0-cib.jpg","title":"广告气球定制logo刻印字定做图案批印刷二维码开业汽球订做装饰发","salesCnt90d":13601779,"maxPrice":422,"minPrice":402},{"itemId":695212560141,"imgUrl":"https://cbu01.alicdn.com/img/ibank/O1CN01wK2Tm92HtJw7wiQtS_!!2215126919208-0-cib.jpg","title":"72色丙烯马克笔 可叠色不透纸马克笔 可水洗速干笔绘水性马克笔","salesCnt90d":13566097,"maxPrice":3625,"minPrice":615,"serviceList":[{"code":"ssbxsfh","name":"48小时发货"},{"code":"psbj","name":"破损包赔"},{"code":"shbp","name":"少货必赔"},{"code":"gfzs","name":"官方直送"}]},{"itemId":702449252612,"imgUrl":"https://cbu01.alicdn.com/img/ibank/10219861228_1663286486.jpg","title":"厂家批发幼儿园图钉按钉胶水胶软木板钉绘画图钉镀镍仿金制散装定","salesCnt90d":11881846,"maxPrice":403,"minPrice":403,"serviceList":[{"code":"ssbxsfh","name":"48小时发货"},{"code":"qtbh","name":"7天包换"},{"code":"shbp","name":"少货必赔"}]},{"itemId":717671064436,"imgUrl":"https://cbu01.alicdn.com/img/ibank/O1CN01OTjb0r2992rvBM8NY_!!991158024-0-cib.jpg","title":"武义蓝天厂家批发儿童图钉按钉胶水胶软木板钉透明工字钉量大从优","salesCnt90d":8903567,"maxPrice":403,"minPrice":403,"serviceList":[{"code":"qtwlybt","name":"7天无理由包退"},{"code":"shbp","name":"少货必赔"},{"code":"ssbxsfh","name":"48小时发货"},{"code":"qtbh","name":"7天包换"}]},{"itemId":607562011725,"imgUrl":"https://cbu01.alicdn.com/img/ibank/14923817115_1544575910.jpg","title":"厂家批发金属扁平钥匙圈双层光面钥匙圈环不锈钢汽车钥匙扣配件","salesCnt90d":7203828,"maxPrice":420,"minPrice":401,"serviceList":[{"code":"ssbxsfh","name":"48小时发货"},{"code":"qtbh","name":"7天包换"},{"code":"qtwlybt","name":"7天无理由包退"},{"code":"czbz","name":"材质保障（限地区）"},{"code":"psbj","name":"破损包赔"},{"code":"shbp","name":"少货必赔"},{"code":"hdbp","name":"坏单包赔"}]},{"itemId":681891967568,"imgUrl":"https://cbu01.alicdn.com/img/ibank/O1CN01ydAVuK1iXmcNzhiUA_!!2214576174423-0-cib.jpg","title":"小白笔中性笔签字笔透明磨砂学生简约文具用品创意套装塑料批发笔","salesCnt90d":7050362,"maxPrice":410,"minPrice":410,"serviceList":[{"code":"ssbxsfh","name":"48小时发货"},{"code":"qtbh","name":"7天包换"},{"code":"qtwlybt","name":"7天无理由包退"},{"code":"czbz","name":"材质保障（限地区）"},{"code":"psbj","name":"破损包赔"},{"code":"shbp","name":"少货必赔"},{"code":"hdbp","name":"坏单包赔"}]},{"itemId":614054324569,"imgUrl":"https://cbu01.alicdn.com/img/ibank/22548917464_1721656335.jpg","title":"批发台湾金箔纸佛像金箔纸滴胶彩箔家具灯饰工艺品100张独立包装","salesCnt90d":6666701,"maxPrice":2300,"minPrice":407,"serviceList":[{"code":"essxsfh","name":"24小时发货"},{"code":"qtbh","name":"7天包换"},{"code":"qtwlybt","name":"7天无理由包退"},{"code":"shbp","name":"少货必赔"}]},{"itemId":712587086710,"imgUrl":"https://cbu01.alicdn.com/img/ibank/O1CN01XDGhWw2BCN4HXApBr_!!2214252738302-0-cib.jpg","title":"批发朱胆扣304不锈钢朱胆扣金属铁皮猪胆扣线扣钥匙扣旋转快挂扣","salesCnt90d":6196600,"maxPrice":418,"minPrice":401,"serviceList":[{"code":"ssbxsfh","name":"48小时发货"},{"code":"qtbh","name":"7天包换"},{"code":"czbz","name":"材质保障（限地区）"},{"code":"psbj","name":"破损包赔"},{"code":"shbp","name":"少货必赔"},{"code":"hdbp","name":"坏单包赔"}]},{"itemId":710328508892,"imgUrl":"https://cbu01.alicdn.com/img/ibank/O1CN01MhuR0Z1KKuisKG0hJ_!!2214050501146-0-cib.jpg","title":"原创可爱镭射咕卡贴纸DIY明星贴纸文具贴纸装饰画手账贴纸批发","salesCnt90d":6137027,"maxPrice":303,"minPrice":303,"serviceList":[{"code":"ssbxsfh","name":"48小时发货"},{"code":"qtbh","name":"7天包换"},{"code":"shbp","name":"少货必赔"}]},{"itemId":776802916017,"imgUrl":"https://cbu01.alicdn.com/img/ibank/O1CN01yiwJGy1ggMvlOiYwO_!!2213978374171-0-cib.jpg","title":"文件袋A4收纳袋档案袋白色加厚纽扣袋试卷袋透明空白袋 容量资料","salesCnt90d":6026575,"maxPrice":450,"minPrice":425,"serviceList":[{"code":"ssbxsfh","name":"48小时发货"},{"code":"qtbh","name":"7天包换"},{"code":"qtwlybt","name":"7天无理由包退"},{"code":"czbz","name":"材质保障（限地区）"},{"code":"psbj","name":"破损包赔"},{"code":"shbp","name":"少货必赔"},{"code":"hdbp","name":"坏单包赔"}]},{"itemId":721535311955,"imgUrl":"https://cbu01.alicdn.com/img/ibank/O1CN01NNTWa01NyFbBKk6ii_!!2720421638-0-cib.jpg","title":"2B擦字洁净橡皮学生考试素描绘画办公pvc少屑橡皮擦文具套装定制","salesCnt90d":6034222,"maxPrice":1380,"minPrice":1080,"serviceList":[{"code":"ssbxsfh","name":"48小时发货"},{"code":"qtbh","name":"7天包换"},{"code":"qtwlybt","name":"7天无理由包退"},{"code":"shbp","name":"少货必赔"}]},{"itemId":664623974023,"imgUrl":"https://cbu01.alicdn.com/img/ibank/O1CN01B3HJi31NyFUZdxmqy_!!2720421638-0-cib.jpg","title":"2B白色柔软橡皮学生考试素描专用pvc橡皮擦易擦少屑文具套装定制","salesCnt90d":6001314,"maxPrice":1090,"minPrice":760,"serviceList":[{"code":"ssbxsfh","name":"48小时发货"},{"code":"qtbh","name":"7天包换"},{"code":"shbp","name":"少货必赔"},{"code":"qtwlybt","name":"7天无理由包退"}]},{"itemId":778417564390,"imgUrl":"https://cbu01.alicdn.com/img/ibank/O1CN01433iRW1c6PN7cEjQF_!!932523551-0-cib.jpg","title":"批发0.8厚亚克力双面胶盲盒手办DIY防水强力圆点胶无痕透明点点胶","salesCnt90d":6375855,"maxPrice":408,"minPrice":401,"serviceList":[{"code":"qtbh","name":"7天包换"},{"code":"czbz","name":"材质保障（限地区）"},{"code":"psbj","name":"破损包赔"},{"code":"shbp","name":"少货必赔"},{"code":"hdbp","name":"坏单包赔"},{"code":"qsexsfh","name":"72小时发货"}]},{"itemId":711858905512,"imgUrl":"https://cbu01.alicdn.com/img/ibank/O1CN01f7RwEF1ojaWwZlCIY_!!2214978725261-0-cib.jpg","title":"厂家批发2.3厘米金属五帝钱 六帝钱 十帝钱 多样仿古黄铜钱","salesCnt90d":5989761,"maxPrice":403,"minPrice":402,"serviceList":[{"code":"ssbxsfh","name":"48小时发货"}]},{"itemId":606192241609,"imgUrl":"https://cbu01.alicdn.com/img/ibank/12423871516_841476354.jpg","title":"工厂批发金属钥匙圈 304不锈钢钥匙环汽车钥匙扣配件不锈钢锁匙圈","salesCnt90d":5565128,"maxPrice":422,"minPrice":401,"serviceList":[{"code":"ssbxsfh","name":"48小时发货"},{"code":"qtbh","name":"7天包换"},{"code":"qtwlybt","name":"7天无理由包退"},{"code":"czbz","name":"材质保障（限地区）"},{"code":"psbj","name":"破损包赔"}]},{"itemId":627132115288,"imgUrl":"https://cbu01.alicdn.com/img/ibank/20779845039_1139958859.jpg","title":"橡皮擦套装批发韩版4b橡皮擦小学生专用橡皮檫儿童橡皮干净不留痕","salesCnt90d":5290809,"maxPrice":790,"minPrice":400,"serviceList":[{"code":"ssbxsfh","name":"48小时发货"},{"code":"qtbh","name":"7天包换"},{"code":"qtwlybt","name":"7天无理由包退"},{"code":"shbp","name":"少货必赔"}]},{"itemId":684477707260,"imgUrl":"https://cbu01.alicdn.com/img/ibank/O1CN01x6Mo2d2BCMurnz1yq_!!2214252738302-0-cib.jpg","title":"厂家直销平圈扁圈金属汽车锁匙圈钥匙环黄铜光圈304不锈钢钥匙扣","salesCnt90d":4569207,"maxPrice":495,"minPrice":401,"serviceList":[{"code":"ssbxsfh","name":"48小时发货"},{"code":"qtbh","name":"7天包换"},{"code":"czbz","name":"材质保障（限地区）"},{"code":"psbj","name":"破损包赔"},{"code":"shbp","name":"少货必赔"},{"code":"hdbp","name":"坏单包赔"}]}]}}}
     * author：wh
     * @param array $request_data 查询条件，根据接口文档传值
     */
    function getGoodsList($request_data = [])
    {
        set_time_limit(0);
        //$base_url = "https://gw.open.1688.com/openapi/";
        $urlPath = "param2/1/com.alibaba.fenxiao/jxhy.product.getPageList/".$this->authObj->config['appkey'];

        //$request_data = [];
        //foreach ($where as $k=>$v){
        //    $request_data[$k] = $v;
        //}

        return $this->authObj->request($urlPath, $request_data);
    }


    /**
     *@deprecated  【官方说是老接口，推荐上面的新接口】
     * desc：[批发团]分销严选商品列表查询（itemId就是offerId）
     *
     * 精选货源商品列表查询，查询到的商品铺货至下游产生订单后，请采用ttpft的交易flow下单，精选货源商品下单目前仅支持单商品下单。
     *
     * https://open.1688.com/api/apidocdetail.htm?id=com.alibaba.fenxiao:alibaba.pifatuan.product.list-1
     *
     * author：wh
     * @param array $request_data 查询条件，根据接口文档传值
     * @return mixed
     */
    function getGoodsListsPft($request_data = []){
        //$base_url = "https://gw.open.1688.com/openapi/";
        $urlPath = "param2/1/com.alibaba.fenxiao/alibaba.pifatuan.product.list/".$this->authObj->config['appkey'];

        //$request_data = [];
        //foreach ($where as $k=>$v){
        //    $request_data[$k] = $v;
        //}

        return $this->authObj->request($urlPath, $request_data);
    }

    /**
     * 商品sku信息查询（itemId就是offerId）
     *
     * sku信息查询，用于页面选品后，查询商品的sku，采用接口下单
     * doc: https://open.1688.com/api/apidocdetail.htm?id=com.alibaba.product:product.skuinfo.get-1
     */
    function getGoodsSkuInfo($offerId){
        $urlPath = "param2/1/com.alibaba.product/product.skuinfo.get/".$this->authObj->config['appkey'];

        $request_data = ['offerId'=>$offerId];

        return $this->authObj->request($urlPath, $request_data);
    }

    /**
     * desc：[推荐]分销严选商品详情批量查询
     *
     * @param string $itemId 商品列表返回的itemId就是offerId
     *
     * doc:https://open.1688.com/api/apidocdetail.htm?id=com.alibaba.fenxiao:alibaba.pifatuan.product.detail.list-2
     *
     */
    function getGoodsDetail(array $itemIds){

        $urlPath = "param2/2/com.alibaba.fenxiao/alibaba.pifatuan.product.detail.list/".$this->authObj->config['appkey'];

        //精选货源offerIds 示例值[682162000966]
        $request_data = ['offerIds'=>json_encode($itemIds)];

        return $this->authObj->request($urlPath, $request_data);
    }

    /**
     * desc：[不推荐]分销严选商品详情批量查询
     *
     * @param string $itemId 商品列表返回的itemId就是offerId
     *
     * doc:https://open.1688.com/api/apidocdetail.htm?id=com.alibaba.fenxiao:alibaba.pifatuan.product.detail.list-1
     *
     */
    function getGoodsDetailV1(array $itemIds){

        $urlPath = "param2/1/com.alibaba.fenxiao/alibaba.pifatuan.product.detail.list/".$this->authObj->config['appkey'];

        //精选货源offerIds 示例值[682162000966]
        $request_data = ['offerIds'=>json_encode($itemIds)];

        return $this->authObj->request($urlPath, $request_data);
    }

    /**
     * desc：关注商品
     * author：wh
     * @param string $productId 商品id 3412111233445
     *
     * {
        "code":  0,
        "message":  "success"
        }
     * doc: https://open.1688.com/api/apidocdetail.htm?id=com.alibaba.product:alibaba.product.follow-1
     */
    function followGoods($productId){
        $urlPath = "param2/1/com.alibaba.fenxiao/alibaba.product.follow/".$this->authObj->config['appkey'];

        return $this->authObj->request($urlPath, ['productId'=>$productId]);
    }

    /**
     * 精选货源商品列表筛选条件枚举值查询
     *
     * doc: https://open.1688.com/api/apidocdetail.htm?id=com.alibaba.fenxiao:jxhy.productFilter.get-1
     */
    function getGoodsFilter(){
        $urlPath = "param2/1/com.alibaba.fenxiao/jxhy.productFilter.get/".$this->authObj->config['appkey'];

        return $this->authObj->request($urlPath, []);
    }

    /**
     * 解除关注
     *
     * {
        "code":  0,
        "message":  "success"
        }
     * doc: https://open.1688.com/api/apidocdetail.htm?id=com.alibaba.product:alibaba.product.unfollow.crossborder-1
     */
    function unfollowGoods($productId){
        $urlPath = "param2/1/com.alibaba.product/alibaba.product.unfollow.crossborder/".$this->authObj->config['appkey'];
        return $this->authObj->request($urlPath, ['productId'=>$productId]);
    }
}