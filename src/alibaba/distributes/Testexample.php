<?php
/*
 * description：
 * author：wh
 * email：
 * createTime：{2024/6/29} {12:57} 
 */

namespace wanghua\general_utility_tools_php\alibaba\distributes;


use app\common\model\TabConf;
use wanghua\general_utility_tools_php\alibaba\AlibabaAuth;
use wanghua\general_utility_tools_php\alibaba\distributes\strict\CustomerService;
use wanghua\general_utility_tools_php\alibaba\distributes\strict\StrictCategory;
use wanghua\general_utility_tools_php\alibaba\distributes\strict\StrictGoods;
use wanghua\general_utility_tools_php\alibaba\distributes\strict\StrictLogistics;
use wanghua\general_utility_tools_php\alibaba\distributes\strict\StrictOrder;
use think\Db;
use wanghua\general_utility_tools_php\alibaba\distributes\strict\StrictPay;


/**
 * 测试案例
 *
 * Class Testexample
 * @package wanghua\general_utility_tools_php\alibaba\distributes
 */
class Testexample
{
    /**
     * desc：查询订单是否开通免密支付
     */
    function queryOrderIsNoPassPay(){
        $config = config('meebo_supply_config');
        $authObj = new AlibabaAuth($config);
        $obj = new StrictPay($authObj);
        return $obj->queryOrderIsNoPassPay();
    }
    function getLogisticsTemplate(){
        $config = config('meebo_supply_config');
        $authObj = new AlibabaAuth($config);
        $obj = new StrictLogistics($authObj);
        return $obj->getLogisticsTemplate();
    }
    //物流模板详情
    function getLogisticsTemplateDetail($template_id){
        $config = config('meebo_supply_config');
        $authObj = new AlibabaAuth($config);
        $obj = new StrictLogistics($authObj);
        return $obj->getLogisticsTemplateDetail($template_id);
    }
    function getGoodsList($where){
        set_time_limit(0);
        $config = config('meebo_supply_config');
        $authObj = new AlibabaAuth($config);
        $obj = new StrictGoods($authObj);
        return $obj->getGoodsList($where);
    }
    function getGoodsListsPft($where){
        $config = config('meebo_supply_config');
        $authObj = new AlibabaAuth($config);
        $obj = new StrictGoods($authObj);
        return $obj->getGoodsListsPft($where);
    }
    function getGoodsDetail(array $itemIds){
        $config = config('meebo_supply_config');
        $authObj = new AlibabaAuth($config);
        $obj = new StrictGoods($authObj);
        return $obj->getGoodsDetail($itemIds);
    }
    function getCategoryByCID($category_id){

        $config = config('meebo_supply_config');
        $authObj = new AlibabaAuth($config);
        $obj = new StrictCategory($authObj);
        return $obj->getCategoryByCID($category_id);
    }
    function testGetRootCategory(){
        $config = config('meebo_supply_config');
        $authObj = new AlibabaAuth($config);
        $obj = new StrictCategory($authObj);
        return $obj->getRootCategory();
    }

    function testWebAuth($redirect_uri){
        $config = config('meebo_supply_config');
        $res = (new AlibabaAuth($config))->webAppAuth($redirect_uri);
        return $res;
    }
    function getTokenByCode($code,$redirect_uri){
        $config = config('meebo_supply_config');
        $res = (new AlibabaAuth($config))->getTokenByCode($code,$redirect_uri);
        return $res;
    }
    //测试创建订单
    function createOrder(){
        $order_info = Db::table(TabConf::$fa_order)
            ->where('orderid','mm70wgex1719586377768')
            ->find();
        $address = Db::table(TabConf::$fa_useraddress)
            ->where('id',$order_info['useraddress_id'])
            ->where('users_id',$order_info['users_id'])
            ->find();
        $goods = Db::table(TabConf::$fa_goods)
            ->where('id',$order_info['goods_id'])
            ->find();

        $config = config('meebo_supply_config');
        $authObj = new AlibabaAuth($config);
        $order = new StrictOrder($authObj);
        $order->setAddress($address);
        $order->setGoods($goods,$order_info['buy_num']);
        return $order->createOrder($order_info);
    }

    function previewOrder(){
        $order_info = Db::table(TabConf::$fa_order)
            ->where('orderid','mm70wgex1719586377768')
            ->find();
        $address = Db::table(TabConf::$fa_useraddress)
            ->where('id',$order_info['useraddress_id'])
            ->where('users_id',$order_info['users_id'])
            ->find();
        $goods = Db::table(TabConf::$fa_goods)
            ->where('id',$order_info['goods_id'])
            ->find();

        $config = config('meebo_supply_config');
        $authObj = new AlibabaAuth($config);
        $order = new StrictOrder($authObj);
        $order->setAddress($address);
        $order->setGoods($goods,$order_info['buy_num']);
        return $order->previewOrder();
    }
    //查询订单可以支持的支付渠道
    function queryOrderPayChannel($order_id){
        $config = config('meebo_supply_config');
        $authObj = new AlibabaAuth($config);
        $order = new StrictPay($authObj);
        return $order->queryOrderPayChannel($order_id);
    }
    //组合收银台url获取
    function getPayUrl(array $orderIds){
        $config = config('meebo_supply_config');
        $authObj = new AlibabaAuth($config);
        $order = new StrictPay($authObj);
        return $order->getPayUrl($orderIds,'PC');
    }

    //获取唤起旺旺聊天的链接
    function getCustomerServiceLink($toOpenUid){
        $config = config('meebo_supply_config');
        $authObj = new AlibabaAuth($config);
        $order = new CustomerService($authObj);
        return $order->getCustomerServiceLink($toOpenUid);
    }
}