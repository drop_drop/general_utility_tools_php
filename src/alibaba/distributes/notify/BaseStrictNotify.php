<?php
/*
 * description：
 * author：wh
 * email：
 * createTime：{2024/7/9} {13:42} 
 */

namespace wanghua\general_utility_tools_php\alibaba\distributes\notify;

use wanghua\general_utility_tools_php\Mmodel;
use wanghua\general_utility_tools_php\tool\Tools;

/**
 * @deprecated 弃用，通知属于业务模块，直接在业务控制器中处理，这里仅作参考
 * 分销严选消息通知基类
 *
 * 初始化该基类，按需调用通知处理类
 *
 * Class BaseStrictNotify
 * @package app\index\logic\alibaba\distributes\notify
 */
class BaseStrictNotify
{

    private $object = null;

    /**
     * desc：执行消息处理
     * author：wh
     * @param $callback
     * @return array
     */
    function doMessage($callback=null){
        return Mmodel::catch(function () use ($callback){
            Tools::log_to_write_txt(['执行消息处理']);
            //根据消息类型，选择对应的处理类
            $message_data = input('message');
            if(empty($message_data)){
                return Tools::set_fail('message为空');
            }
            $message_data = json_decode($message_data,true);
            Tools::log_to_write_txt(['消息解码'=>$message_data]);
            if(empty($message_data['type'])){
                return Tools::set_fail('type为空');
            }
            //获取消息类型
            $fn = strtolower($message_data['type']);//类型即为方法名
            //消息类型前缀即为类名
            $class = '\\app\\index\\logic\\alibaba\\distributes\\notify\\'.ucfirst(explode('_',$fn)[0]).'Notify';
            $this->object = (new $class());//实例化类
            return $this->object->{$fn}($callback);//调用方法
        });
    }
}