<?php
/*
 * description：
 * author：wh
 * email：
 * createTime：{2024/7/9} {13:25} 
 */
namespace wanghua\general_utility_tools_php\alibaba\distributes\notify;

use app\index\logic\alibaba\distributes\Testexample;
use wanghua\general_utility_tools_php\Mmodel;
use wanghua\general_utility_tools_php\tool\Tools;


/**
 * @deprecated 弃用，通知属于业务模块，直接在业务控制器中处理，这里仅作参考
 * 产品 消息通知
 *
 * Class ProductNotify
 * @package app\index\logic\alibaba\distributes\notify
 */
class ProductNotify extends BaseStrictNotify
{

    /**
     * @deprecated 弃用，通知属于业务模块，直接在业务控制器中处理，这里仅作参考
     * desc：商品上架通知
     * author：wh
     *
     * 输出message
    {"message":"{\"data\":{\"productIds\":\"107680826\",\"msgSendTime\":\"2018-05-30 20:29:37\",\"memberId\":\"shyxsscl\",\"status\":\"RELATION_VIEW_PRODUCT_REPOST\"},\"gmtBorn\":1720514151217,\"msgId\":88922343751,\"type\":\"PRODUCT_RELATION_VIEW_PRODUCT_REPOST\",\"userInfo\":\"b2b-1881150273\"}","_aop_signature":"03EB6538BB8864BC141ADB5A4923005BB0204F85"}
     */
    function product_relation_view_product_repost($callback=null)
    {
        return Mmodel::catch(function () use ($callback){
            $message_data = input('message');
            if(empty($message_data)){
                return Tools::set_fail('message不能为空');
            }
            $message_data = json_decode($message_data,true);
            if(empty($message_data['data'])){
                return Tools::set_fail('data不能为空');
            }
            $productIds = $data['productIds'];//商品ID集合，至少有一个，用逗号分割
            $productIdsArr = explode(',',$productIds);
        });
    }
}