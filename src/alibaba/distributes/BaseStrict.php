<?php
/*
 * description：
 * author：wh
 * email：
 * createTime：{2024/6/29} {12:23} 
 */

namespace wanghua\general_utility_tools_php\alibaba\distributes;


class BaseStrict
{
    public $base_url = 'https://gw.open.1688.com/openapi/';
    //授权对象
    protected $authObj = null;
    public function __construct($authObj)
    {
        $this->authObj = $authObj;
        //初始化并设置当前模块的基础url
        $this->authObj->base_url = $this->base_url;
    }
}