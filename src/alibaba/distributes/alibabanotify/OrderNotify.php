<?php
/*
 * description：
 * author：wh
 * email：
 * createTime：{2024/7/25} {0:37} 
 */
namespace app\index\logic\alibabanotify;

use app\index\logic\BaseLogic;
use wanghua\general_utility_tools_php\Mmodel;
use wanghua\general_utility_tools_php\tool\Tools;

class OrderNotify extends BaseAlibabaLogic
{
    /**
     * desc：1688创建订单（买家视角）/order created (buyer view)
     * author：wh
     * @return array
     */
    function order_buyer_view_buyer_make($message_data){
        return Mmodel::catch(function ()use($message_data){
            Tools::log_to_write_txt([__FUNCTION__.'消息入参：'=>$message_data]);

        });
    }

    /**
     * desc：1688交易成功（卖家视角）
     * author：wh
     */
    function order_buyer_view_order_success($message_data){
        return Mmodel::catch(function ()use($message_data){
            Tools::log_to_write_txt([__FUNCTION__.'消息入参：'=>$message_data]);

        });
    }
    /**
     * 1688订单批量支付状态同步消息
     */
    function order_batch_pay($message_data){
        return Mmodel::catch(function ()use($message_data){
            Tools::log_to_write_txt([__FUNCTION__.'消息入参：'=>$message_data]);

        });
    }
    /**
     * 1688交易付款（买家视角）/1688 transaction payment (buyer view)
     */
    function order_buyer_view_order_pay($message_data){
        return Mmodel::catch(function ()use($message_data){
            Tools::log_to_write_txt([__FUNCTION__.'消息入参：'=>$message_data]);

        });
    }
    /**
     * 1688卖家关闭订单（买家视角）/seller closing order (buyer view)
     */
    function order_buyer_view_order_seller_close($message_data){
        return Mmodel::catch(function ()use($message_data){
            Tools::log_to_write_txt([__FUNCTION__.'消息入参：'=>$message_data]);

        });
    }
    /**
     * 1688修改订单价格（买家视角）/order price modification (buyer view)
     */
    function order_buyer_view_order_price_modify($message_data){
        return Mmodel::catch(function ()use($message_data){
            Tools::log_to_write_txt([__FUNCTION__.'消息入参：'=>$message_data]);

        });
    }
    /**
     * 1688订单发货（买家视角）/1688 order delivery (buyer view)
     */
    function order_buyer_view_announce_sendgoods($message_data){
        return Mmodel::catch(function ()use($message_data){
            Tools::log_to_write_txt([__FUNCTION__.'消息入参：'=>$message_data]);

        });
    }
    /**
     * 1688订单部分发货（买家视角）/Partial delivery of 1688 order (buyer view)
     */
    function order_buyer_view_part_part_sendgoods($message_data){
        return Mmodel::catch(function ()use($message_data){
            Tools::log_to_write_txt([__FUNCTION__.'消息入参：'=>$message_data]);

        });
    }
    /**
     * 商家修改订单地址(买家视角)
     */
    function order_buyer_view_order_seller_modify_adress($message_data){
        return Mmodel::catch(function ()use($message_data){
            Tools::log_to_write_txt([__FUNCTION__.'消息入参：'=>$message_data]);

        });
    }
    /**
     * 1688订单售中退款（买家视角）
     */
    function order_buyer_view_order_buyer_refund_in_sales($message_data){
        return Mmodel::catch(function ()use($message_data){
            Tools::log_to_write_txt([__FUNCTION__.'消息入参：'=>$message_data]);

        });
    }
    /**
     * 1688订单售后退款（买家视角）
     */
    function order_buyer_view_order_refund_after_sales($message_data){
        return Mmodel::catch(function ()use($message_data){
            Tools::log_to_write_txt([__FUNCTION__.'消息入参：'=>$message_data]);

        });
    }
    /**
     * 1688买家关闭订单（买家视角）/buyer closing order (buyer view)
     */
    function order_buyer_view_order_buyer_close($message_data){
        return Mmodel::catch(function ()use($message_data){
            Tools::log_to_write_txt([__FUNCTION__.'消息入参：'=>$message_data]);

        });
    }
    /**
     * 1688订单确认收货（买家视角）/order receipt confirmation (buyer view)
     */
    function order_buyer_view_order_comfirm_receivegoods($message_data){
        return Mmodel::catch(function ()use($message_data){
            Tools::log_to_write_txt([__FUNCTION__.'消息入参：'=>$message_data]);

        });
    }
}