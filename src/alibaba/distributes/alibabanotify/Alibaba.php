<?php
/*
 * description：
 * author：wh
 * email：
 * createTime：{2024/6/29} {17:31} 
 */

namespace app\index\controller;


use app\common\model\TabConf;
use app\index\logic\alibaba\distributes\notify\BaseStrictNotify;
use wanghua\general_utility_tools_php\alibaba\distributes\Testexample;
use app\index\logic\alibabanotify\ProductNotify;
use wanghua\general_utility_tools_php\Mmodel;
use wanghua\general_utility_tools_php\tool\Tools;

/**
 * 阿里巴巴异步通知控制器类，是消息处理入口类
 * Class Alibabanotify
 * @package app\index\controller
 */
class Alibaba extends BasePublicController
{

    /**
     * desc：通知
     * index/Alibaba/notify
     *
     * 输出message
    {
        "data":{
            "productIds":"107680826",
            "msgSendTime":"2018-05-30 20:29:37",
            "memberId":"shyxsscl",
            "status":"RELATION_VIEW_PRODUCT_REPOST"
        },
        "gmtBorn":1720459935606,
        "msgId":88837491757,
        "type":"PRODUCT_RELATION_VIEW_PRODUCT_REPOST",
        "userInfo":"b2b-1881150273"
    }
     * author：wh
     * @return \think\response\Json
     */
    function notify(){
        return Mmodel::catchJson(function (){
            Tools::log_to_write_txt(['执行消息处理']);
            //根据消息类型，选择对应的处理类
            $message_data = input('message');
            if(empty($message_data)){
                return Tools::set_fail('message为空');
            }
            $message_data = json_decode($message_data,true);
            Tools::log_to_write_txt(['消息解码'=>$message_data]);
            if(empty($message_data['type'])){
                return Tools::set_fail('type为空');
            }
            //$key = strtolower(explode('_',$message_data['type'])[0]);
            //获取消息类型
            $fn = strtolower($message_data['type']);//类型即为方法名
            //消息类型前缀即为类名
            $class = '\\app\\index\\logic\\alibabanotify\\'.ucfirst(explode('_',$fn)[0]).'Notify';
            //实例化类
            return (new $class())->{$fn}($message_data);//调用方法
        });
    }

    /**
     * desc：ali采购获取临时授权code
     *
     * index/alibaba/webAuthCode
     *
     * author：wh
     * @return string
     */
    function webAuthCode(){

        $Testexample = new Testexample();

        $res = $Testexample->testWebAuth(request()->domain().'/index/alibaba/webUserAuth');
        return $res;
    }

    /**
     * desc：网页授权,code换取token
     *
     * index/alibaba/webUserAuth
     *
     * author：wh
     */
    function webUserAuth(){
        $code = input('code');
        if(empty($code)){
            return $this->error('授权失败');
        }

        $redirect_uri = url('test/test2');
        $Testexample = new Testexample();
        $res = $Testexample->getTokenByCode($code,$redirect_uri);
        if($res['code'] != 200){
            return $this->error('授权失败.'.$res['msg']);
        }
        $data = json_decode($res['data']);
        $auth_data = [
            'access_token'=>$data['access_token'],
            'aliid'=>$data['aliId'],
            'expires_in'=>$data['expires_in'],
            'memberid'=>$data['memberId'],
            'refresh_token'=>$data['refresh_token'],
            'refresh_token_timeout'=>$data['refresh_token_timeout'],
            'resource_owner'=>$data['resource_owner'],
        ];
        //写入数据库
        Mmodel::existsUpdateInsert(TabConf::$fa_alibaba_user_auth,['access_token'=>$data['access_token']],$auth_data);
        return '授权成功';
    }

    //function getCode(){
    //    return Mmodel::catchJson(function (){
    //        //保存code
    //        $code = input('code');
    //
    //    });
    //}
}