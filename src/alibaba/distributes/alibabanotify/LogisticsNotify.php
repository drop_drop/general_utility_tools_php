<?php
/*
 * description：
 * author：wh
 * email：
 * createTime：{2024/7/25} {1:04} 
 */

namespace app\index\logic\alibabanotify;


use wanghua\general_utility_tools_php\Mmodel;
use wanghua\general_utility_tools_php\tool\Tools;

class LogisticsNotify extends BaseAlibabaLogic
{
    /**
     * 物流单状态变更（买家视角）
     */
    function logistics_buyer_view_trace($message_data){
        return Mmodel::catch(function ()use($message_data){
            Tools::log_to_write_txt([__FUNCTION__.'消息入参：'=>$message_data]);

        });
    }
    /**
     * 物流单号修改消息
     */
    function logistics_mail_no_change($message_data){
        return Mmodel::catch(function ()use($message_data){
            Tools::log_to_write_txt([__FUNCTION__.'消息入参：'=>$message_data]);

        });
    }
}