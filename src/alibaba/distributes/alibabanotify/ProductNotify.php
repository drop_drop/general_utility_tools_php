<?php
/*
 * description：
 * author：wh
 * email：
 * createTime：{2024/7/25} {1:06} 
 */

namespace app\index\logic\alibabanotify;


use wanghua\general_utility_tools_php\Mmodel;
use wanghua\general_utility_tools_php\tool\Tools;

class ProductNotify extends BaseAlibabaLogic
{
    /**
     * 1688产品审核下架（关系用户视角）
     */
    function product_relation_view_product_audit($message_data){
         return Mmodel::catch(function ()use($message_data){
            Tools::log_to_write_txt([__FUNCTION__.'消息入参：'=>$message_data]);

        });
    }
    /**
     * 1688产品删除（关系用户视角）
     */
    function product_relation_view_product_delete($message_data){
         return Mmodel::catch(function ()use($message_data){
            Tools::log_to_write_txt([__FUNCTION__.'消息入参：'=>$message_data]);

        });
    }
    /**
     * 1688产品新增或修改（关系用户视角）
     */
    function product_relation_view_product_new_or_modify($message_data){
         return Mmodel::catch(function ()use($message_data){
            Tools::log_to_write_txt([__FUNCTION__.'消息入参：'=>$message_data]);

        });
    }
    /**
     * 1688产品上架（关系用户视角）
     */
    function product_relation_view_product_repost($message_data){
         return Mmodel::catch(function ()use($message_data){
            Tools::log_to_write_txt([__FUNCTION__.'消息入参：'=>$message_data]);

        });
    }
    /**
     * 1688商品库存变更消息（关系用户视角）
     */
    function product_product_inventory_change($message_data){
         return Mmodel::catch(function ()use($message_data){
            Tools::log_to_write_txt([__FUNCTION__.'消息入参：'=>$message_data]);

        });
    }
    /**
     * 精选货源商品下架消息
     */
    function product_pft_offer_quit($message_data){
         return Mmodel::catch(function ()use($message_data){
            Tools::log_to_write_txt([__FUNCTION__.'消息入参：'=>$message_data]);

        });
    }
    /**
     * 精选货源商品价格变动消息
     */
    function product_pft_offer_price_modify($message_data){
         return Mmodel::catch(function ()use($message_data){
            Tools::log_to_write_txt([__FUNCTION__.'消息入参：'=>$message_data]);

        });
    }
    /**
     * 1688产品下架（关系用户视角）
     */
    function product_relation_view_product_expire($message_data){
         return Mmodel::catch(function ()use($message_data){
            Tools::log_to_write_txt([__FUNCTION__.'消息入参：'=>$message_data]);

        });
    }
}