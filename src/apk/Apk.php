<?php
/*
 * description： apk应用安装包信息读取
 * author：wh
 * email：
 * createTime：{2020/7/1} {17:16} 
 */

namespace wanghua\general_utility_tools_php\apk;


class Apk
{

    /**
     * 前提：安装PHP ZIP扩展
     * 扩展下载：https://windows.php.net/downloads/pecl/releases/zip/
     * desc：读取APK
     * author：wh
     * @param string $path 文件地址
     * @param bool $isRemote 是否远程文件
     * @param string $proxy 代理
     * @return array
     */
    function read(string $path, $filename, bool $isRemote=false, $proxy=''){
        /*解析安卓apk包中的压缩XML文件，还原和读取XML内容

        依赖功能：需要PHP的ZIP包函数支持。*/

        $appObj = new ApkParser();

        $targetFile = $path;//a.apk; apk所在的路径地址
        // 如果是远程文件，先下载到本地
        if ($isRemote) {
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $path);
            if ($proxy != '') {
                curl_setopt($ch, CURLOPT_PROXY, $proxy);
            }
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 50);
            $fileContent = curl_exec($ch);
            curl_close($ch);

            // 写入临时文件
            $sys_tmp_path = tempnam(sys_get_temp_dir(), 'DL');
            $fp = @fopen($sys_tmp_path, 'w+');
            fwrite($fp, $fileContent);

            $tmp_path = config('app.ROOT_PATH_PRO').'runtime/temp_file';
            if(!file_exists($tmp_path)){
                mkdir($tmp_path, 0777, true);
            }
            $targetFile = $tmp_path. '/' .time().$filename;
            $res = move_uploaded_file($sys_tmp_path, $targetFile);
            dump($res);die;
        }
        $result = [
            'app_name'=>'',// 应用名称
            'package'=>'',// 应用包名
            'version_name'=>'',// 版本名称
            'version_code'=>'',// 版本代码
        ];

        dump($targetFile);die;
        if(!is_file($targetFile)){
            return $result;
        }
        $appObj->open($targetFile);//$res

        $result = [
            'app_name'=>$appObj->getAppName(),// 应用名称
            'package'=>$appObj->getPackage(),// 应用包名
            'version_name'=>$appObj->getVersionName(),// 版本名称
            'version_code'=>$appObj->getVersionCode(),// 版本代码
        ];
        return $result;
    }
}