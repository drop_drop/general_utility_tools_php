<?php
/*
 * description：
 * author：wh
 * email：
 * createTime：{2021/08/24} {18:09} 
 */

namespace wanghua\general_utility_tools_php\wechat;


use think\Exception;
use wanghua\general_utility_tools_php\framework\base\BasePublicController;
use wanghua\general_utility_tools_php\tool\Tools;


/**
 * 【注意】：此类是控制器类，用来请求微信用户信息（微信授权等操作），可以直接复制到你的控制器中改名使用
 *
 * 微信授权，授权之后重定向到其它页面
 *
 * Class Wexinauth
 */
class TmpWexinauth
{


    //请求此页面进行授权，微信将数据发送到重定向接口，接口做下一步业务
    //========================================= [3步获取用户信息 start] =========================================
    /**
     * description：（1）用户同意授权，获取code （2）第二步在Wexinauth.php ->authReturnUrl方法
     * author：wanghua
     */
    function usrAuth(){
        $wxconfig = config('pay_config.wechat');

        (new UserAuth($wxconfig,$wxconfig['access_token_path']))->usrAuth(url('authReturnUrl','',false,true));
    }

    /**
     * description：用户授权回调url （以下完整步骤）
     * array(10) {
    ["openid"]=>
    string(28) "o5_r60xfL0speOoDVcAcrfX24wTw"
    ["nickname"]=>
    string(6) "展望"
    ["sex"]=>
    int(1)
    ["language"]=>
    string(5) "zh_CN"
    ["city"]=>
    string(6) "渝中"
    ["province"]=>
    string(6) "重庆"
    ["country"]=>
    string(6) "中国"
    ["headimgurl"]=>
    string(134) "http://thirdwx.qlogo.cn/mmopen/vi_32/99az237H9C2cahYGicowt3gwGd4wGxgnf4ia4T7ZCFicml7u6EvibAFAZ8ibuB6erwtpe5gUI24VlLuiaaP5ic666HdIQ/132"
    ["privilege"]=>
    array(0) {
    }
    ["unionid"]=>
    string(28) "o20U21BMNw_3i-jpGjudYxfud6uE"
    }
     * author：wanghua
     */
    function authReturnUrl(){
        $code = input('code');
        $weixin_logic = new WeixinLogic();

        //1 获取用户授权code
        $data = $weixin_logic->usrAccessToken($code);
        if(empty($data['openid'])){
            throw new Exception('获取用户授权code出错.'.json_encode($data,JSON_UNESCAPED_UNICODE));
        }
        //2 得到 access_token
        $access_token = empty($data['access_token'])?$weixin_logic->getAccessToken():$data['access_token'];
        //得到 access_token 之后全局保存 - 等之后扩展
        //if($access_token){
        //由于access_token拥有较短的有效期，当access_token超时后，可以使用refresh_token进行刷新，
        //refresh_token有效期为30天，当refresh_token失效之后，需要用户重新授权。
        //  cookie('usr_auth_access_token', $access_token);//网页授权 access_token 全局存储
        //}
        //3 获取openid 并保存全局使用


        $openid = $data['openid'];

        //4 通过access_token 拉取用户信息
        $url = "https://api.weixin.qq.com/sns/userinfo?access_token={$access_token}&openid={$openid}&lang=zh_CN";
        $result = Tools::curl_get($url);
        //5 存储，以便后续业务使用
        session('wx_user_info', json_decode($result['data'], true));
        //6 授权完成跳转至首页进行下一步业务
        //检查授权之前的url是否存在

        $this->redirect(url('Storepage/index'));
    }
    //========================================= [3步获取用户信息 end] =========================================

}