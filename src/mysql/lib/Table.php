<?php
/*
 * description：
 * author：wh
 * email：
 * createTime：{2022/02/24} {10:48} 
 */

namespace wanghua\general_utility_tools_php\mysql\lib;
use think\Db;
use wanghua\general_utility_tools_php\tool\Tools;

/**
 * @deprecated 废弃，未来将会删除
 *
 * desc：
 *
 * author：wh
 * Class Table
 * @package wanghua\general_utility_tools_php\mysql\lib
 */
class Table
{
    /**
     * desc：创建表，初始化默认字段
     * author：wh
     * @param $data
     * @return bool
     */
    function createTable($data){
        $tablename = $data['tablename'];
        $title = $data['title'];
        //创建表
        $sql = "CREATE TABLE IF NOT EXISTS {$tablename}(
		id int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT 'ID',
		create_time timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
		update_time timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
		PRIMARY key(id)) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='{$title}' ;";
        Db::execute($sql);
    }


    /**
     * desc：改注释
     * author：wh
     * @param $tablename
     * @param $comment
     * @return bool
     */
    function updateComment($tablename, $comment){
        $sql = "ALTER TABLE {$tablename} COMMENT '{$comment}';";
        Db::execute($sql);
    }

    /**
     * desc：改表名
     * author：wh
     * @param $tablename
     * @param $new_table_name
     * @return bool
     */
    function updateTableName($tablename, $new_table_name){
        $sql = "ALTER TABLE {$tablename} RENAME TO {$new_table_name}";
        Db::execute($sql);
    }

    /**
     * desc：查询当前数据库所有的表名
     * author：wh
     * @return mixed
     */
    function getTables(){
        $dbname = $this->getNowDbName();
        return array_column(Db::query('SHOW TABLES;'), 'Tables_in_'.$dbname);
    }

    /**
     * desc：获取数据表字所有段名
     * author：wh
     * @param $tablename
     * @return array
     */
    function getTableFields($tablename){
        $dbname = $this->getNowDbName();
        $sql = "SELECT COLUMN_NAME column_name,COLUMN_COMMENT column_comment,DATA_TYPE data_type
FROM information_schema.columns WHERE TABLE_NAME='{$tablename}' AND table_schema='{$dbname}'";
        return array_column(Db::query($sql), 'column_name');
    }

    /**
     * desc：查询当前数据库名
     * author：wh
     * @return mixed
     */
    function getNowDbName(){
        $sql = "SELECT DATABASE() AS dbname;";
        return Db::query($sql)['dbname'];
    }
    /**
     * desc：获取表的属性
     * Name:
    表名称
    Engine:
    表的存储引擎
    Version:
    版本
    Row_format:
    行格式。对于MyISAM引擎，这可能是Dynamic，Fixed或Compressed。动态行的行长度可变，例如Varchar或Blob类型字段。固定行是指行长度不变，例如Char和Integer类型字段
    Rows:
    表中的行数。对于MyISAM和其他存储引擎，这个值是精确的，对于innoDB存储引擎，这个值通常是估算的
    Avg_row_length:
    平均每行包括的字节数
    Data_length:
    整个表的数据量(以字节为单位)
    Max_data_length:
    表可以容纳的最大数据量，该值和存储引擎相关
    Index_length:
    索引占用磁盘的空间大小(以字节为单位)
    Data_free:
    对于MyISAM引擎，表示已经分配，但目前没有使用的空间。这部分空间包含之前被删除的行，以及后续可以被insert利用到的空间
    Auto_increment:
    下一个Auto_increment的值
    Create_time:
    表的创建时间
    Update_time:
    表的最近更新时间
    Check_time:
    使用 check table 或myisamchk工具最后一次检查表的时间
    Collation:
    表的默认字符集和字符排序规则
    Checksum:
    如果启用，保存的是整个表的实时校验和
    Create_options:
    创建表时指定的其他选项
    Comment:
    包含了其他额外信息，对于MyISAM引擎，保存的是表在创建时带的注释。如果表使用的是innodb引擎 ，保存的是InnoDB表空间的剩余空间。如果是一个视图，注释里面包含了VIEW字样。
     *
     * @param $tablename 表名（无表名则查询所有表）
     *
     * author：wh
     * @return mixed
     */
    function getTableInfo(string $tablename=''){
        if($tablename){
            $sql = "SHOW TABLE STATUS WHERE Name = 'fa_agent';";
            return Db::query($sql);
        }
        $sql = 'show table status;';
        return Db::query($sql);
    }

    /**
     * desc：检查某个表是否存在
     *
     * author：wh
     * @param string $dbname
     * @param string $tablename
     * @return bool
     */
    function isExistTable(string $dbname,string $tablename){
        $sql = "SELECT TABLE_SCHEMA,TABLE_NAME 
FROM information_schema.TABLES 
WHERE TABLE_SCHEMA ='$dbname' 
  AND TABLE_NAME = '$tablename';";
        return empty(Db::query($sql));
    }

    /**
     * desc：获取表注释
     *
     * author：wh
     */
    function getTableComment($table){
        $prefix = config('database.prefix');
        $table = $prefix.$table;
        $sql = 'show table status;';
        $arr = Db::query($sql);
        $tmp = Tools::key_val_arr($arr,'Name','Comment');
        return empty($tmp[$table])?'':$tmp[$table];
    }
}