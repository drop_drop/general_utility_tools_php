<?php
/*
 * description：
 * author：wh
 * email：
 * createTime：{2024/8/8} {16:11} 
 */
namespace wanghua\general_utility_tools_php\sms;

use AlibabaCloud\SDK\Dysmsapi\V20170525\Dysmsapi;
use AlibabaCloud\SDK\Dysmsapi\V20170525\Models\AddSmsSignRequest;
use \Exception;
use AlibabaCloud\Tea\Exception\TeaError;
use AlibabaCloud\Tea\Utils\Utils;

use Darabonba\OpenApi\Models\Config;
use AlibabaCloud\SDK\Dysmsapi\V20170525\Models\SendSmsRequest;
use AlibabaCloud\Tea\Utils\Utils\RuntimeOptions;
use wanghua\general_utility_tools_php\tool\Tools;

/**
 * 阿里云短信接口文档地址
 *
 * https://next.api.aliyun.com/api/Dysmsapi/2017-05-25/SendSms?params={%22SignName%22:%22%E4%BC%98%E4%B8%80%E7%A7%91%E6%8A%80%22,%22TemplateCode%22:%22SMS_471455330%22,%22PhoneNumbers%22:%2218290416033%22,%22TemplateParam%22:%22%7B%5C%22code%5C%22%3A%5C%221234%5C%22%7D%22}&RegionId=cn-hangzhou&tab=DEMO&lang=PHP
 * Class AliSms
 * @package wanghua\general_utility_tools_php\sms
 */
class AliSms
{

    private $accessKeyId;
    private $accessKeySecret;
    private $sms_template_code = '';//模板码
    private $sms_sign_name = '';//签名

    private $aliyun_oss_config=[];

    /**
     * AliSms constructor.
     * @param $aliyun_oss_config
     * @param $sms_template_code
     * @param $sms_sign_name
     */
    public function __construct($aliyun_oss_config,$sms_sign_name,$sms_template_code)
    {
        $this->accessKeyId = $aliyun_oss_config['AccessKeyId'];
        $this->accessKeySecret = $aliyun_oss_config['AccessKeySecret'];
        $this->aliyun_oss_config = $aliyun_oss_config;
        $this->sms_template_code = $sms_template_code;
        $this->sms_sign_name = $sms_sign_name;
    }

    /**
     * 使用AK&SK初始化账号Client
     * @return Dysmsapi Client
     */
    private function createClient(){
        // 工程代码泄露可能会导致 AccessKey 泄露，并威胁账号下所有资源的安全性。以下代码示例仅供参考。
        // 建议使用更安全的 STS 方式，更多鉴权访问方式请参见：https://help.aliyun.com/document_detail/311677.html。
        $conf = [
            // 必填，请确保代码运行环境设置了环境变量 ALIBABA_CLOUD_ACCESS_KEY_ID。
            "accessKeyId" => $this->accessKeyId,//getenv("ALIBABA_CLOUD_ACCESS_KEY_ID"),
            // 必填，请确保代码运行环境设置了环境变量 ALIBABA_CLOUD_ACCESS_KEY_SECRET。
            "accessKeySecret" => $this->accessKeySecret,//getenv("ALIBABA_CLOUD_ACCESS_KEY_SECRET")
        ];
        $config = new Config($conf);
        // Endpoint 请参考 https://api.aliyun.com/product/Dysmsapi
        $config->endpoint = "dysmsapi.aliyuncs.com";
        return new Dysmsapi($config);
    }

    /**
     * desc：添加短信模板
     * author：wh
     */
    function addTemplate($SignName,$SignSource,$Remark){
        $client = $this->createClient();
        $addSmsSignRequest = new AddSmsSignRequest([]);
        $runtime = new RuntimeOptions([]);

        // 复制代码运行请自行打印 API 的返回值
        $res = $client->addSmsSignWithOptions($addSmsSignRequest, $runtime);
        dump($res);
    }

    /**
     * 发送短信
     *
     * 文档地址：https://next.api.aliyun.com/api/Dysmsapi/2017-05-25/SendSms?params={%22SignName%22:%22%E4%BC%98%E4%B8%80%E7%A7%91%E6%8A%80%22,%22TemplateCode%22:%22SMS_471455330%22,%22PhoneNumbers%22:%2218290416033%22,%22TemplateParam%22:%22%7B%5C%22code%5C%22%3A%5C%221234%5C%22%7D%22}&RegionId=cn-hangzhou&tab=DEMO&lang=PHP
     */
    public function send(string $phoneNumbers,string $templateParam){
        $client = $this->createClient();
        $conf = [
            //签名
            "signName" => $this->sms_sign_name,//"优一科技",
            //模板code
            "templateCode" => $this->sms_template_code,//"SMS_471455330",
            //手机号（多个逗号隔开，上限1000）
            "phoneNumbers" => $phoneNumbers,
            "templateParam" => $templateParam,//{"code":"1234"}
        ];
        $sendSmsRequest = new SendSmsRequest($conf);
        // 运行请自行打印 API 的返回值
        $res = $client->sendSmsWithOptions($sendSmsRequest, new RuntimeOptions([]));
        return Tools::set_ok('ok', $res);
    }
}