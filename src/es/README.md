##### Elasticsearch数据库操作类库使用教程

#### 一、Elasticsearch原生cUrl查询库
##### 说明：该库只提供查询请求，开发者只需要关注查询参数怎么写，请求之后会把结果按原样返回，不会有任何修改。

****

###### 【查询参数案例：查询一条数据】
###### 只需要简单的查询条件即可；from=0，size=1表示从第0条数据查询1条，from可省略，等于mysql的limit 1。
###### 这里unixtime和statFunc为数据结构(表)中的字段名称，gt大于，lt小于，gte大于等于，lte小于等于
###### 这里register是字段statFunc的值，而unixtime存的是毫秒时间戳
###### 注：数据字段尽量不存小数，小数太小的时候，在内存中计算时会出现误差，比如0.001+0.001=0

#### 代码使用示例:
```

//======【有规律的日期索引】【特殊】=======
$query_params = "{
               "query": {
                 "bool": {
                   "must": [
                     {
                       "range": {
                         "unixtime": {
                           "gt": 1623254400000,
                           "lt": 2623340800000
                         }
                       }
                     },
                     {
                       "terms": {
                         "statFunc": [
                           "signup",
                           "returncard"
                         ]
                       }
                     },
                     {
                       "term": {
                         "matchTypeNum": 3
                       }
                     }
                   ]
                 }
               },
               "size": 1,
               "aggs": {
                 "group_by_day": {
                   "date_histogram": {
                     "field": "@timestamp",
                     "interval": "day",
                     "min_doc_count": 0,
                     "order": {
                       "_key": "asc"
                     },
                     "time_zone": "+08:00",
                     "format": "yyyy-MM-dd"
                   },
                   "aggs": {
                     "item1000_sum": {
                       "sum": {
                         "field": "item1000"
                       }
                     }
                   }
                 }
               }
             }";
$es = new Elasticsearch('http://49.4.3.4:3201','qa-item-');
$es->setIndexAllDate();
$es->setQueryParam($query_params);
dump($es->execute());

dump('==================================================================');

//======【无规律的普通索引】【推荐】=======
$query_params = "{
    'query': {
        'bool': {
            'must': [
                     {
                         'range': {
                         'unixtime': {
                             'gt': 1617206400000,
                           'lt': 1619712000000
                         }
                       }
                     },
                     {
                         'term': {
                         'statFunc': 'register'
                       }
                     }
                   ]
                 }
               },
               'size': 1
             }";
dump($query_params);
$es = new Elasticsearch('http://49.4.3.4:3201','');
$es->setIndexNormal('qa-item-2021.06.*');
$es->setQueryParam($query_params);
dump($es->execute());


//以上都经过测试
    
```

#### 查询参数示例 
###### 示例1: 这里展示了must查询
```
{
  "query": {
    "bool": {
      "must": [
        {
          "range": {
            "unixtime": {
              "gt": 1617206400000,
              "lt": 1619712000000
            }
          }
        },
        {
          "term": {
            "statFunc": "register"
          }
        }
      ]
    }
  },
  "size": 1
}
``` 
###### 示例2: 这里展示了混合查询
###### must表示必须，等于mysql的and；
###### should表示应该，等于mysql的or；
###### 模糊查询用missing，等于mysql的like，但是不用%%通配符；
###### must_not表示必须不是，等于mysql的!=；
###### 它们可以一起使用。
###### 仔细观察发现，query，bool基本是固定的，而里面的参数写法都差不多。

```
{
  "query": {
    "bool": {
      "must": [
        {
          "match": {
            "statFunc": "signup"
          }
        }
      ],
      "must_not": [],
      "should": [
        {
          "term": {
            "gameTagNum": "210"
          }
        },
        {
          "term": {
            "gameTagNum": "300"
          }
        }
      ]
    }
  },
  "size": 1
}
```

****

###### 【查询参数案例：查询多条数据】
###### 只需要简单的查询条件即可，这里只改变了size字段值。
###### eg:
```
{
  "query": {
    "bool": {
      "must": [
        {
          "range": {
            "unixtime": {
              "gt": 1617206400000,
              "lt": 1619712000000
            }
          }
        },
        {
          "term": {
            "statFunc": "register"
          }
        }
      ]
    }
  },
  "size": 10
}
```

****

###### 【查询参数案例：简单聚合】
###### 根据时间和某个字段查询数据，并且基于查询结果再次按时间分组，分组段按天分段，同时基于查询结果再次聚合某个字段
###### 为了便于演示，这里查询条件改为容易理解的字段："统计张三一段时间内已下单的订单总金额"
###### order_money_sum是自定义名称，等于mysql的别名
###### eg: 
```
{
  "query": {
    "bool": {
      "must": [
        {
          "range": {
            "order_time": {
              "gt": 1617206400000,
              "lt": 1619712000000
            }
          }
        },
        {
          "term": {
            "name": "张三"
          }
        }
      ]
    }
  },
  "size": 0,
  "aggs": {
    "order_money_sum": {
      "sum": {
        "field": "order_money"
      }
    }
  }
}
```

###### 【查询参数案例：复杂聚合】
###### aggs可以嵌套，嵌套时的含义表示基于上一个聚合结果再次聚合
###### 这里表示先按day（天）分组，再统计字段名为item1000的字段，day可以改为月（month）年（year）
###### eg:
```
{
  "query": {
    "bool": {
      "must": [
        {
          "range": {
            "unixtime": {
              "gt": 1617206400000,
              "lt": 1619712000000
            }
          }
        },
        {
          "term": {
            "statFunc": "register"
          }
        }
      ]
    }
  },
  "size": 0,
  "aggs": {
    "group_by_day": {
      "date_histogram": {
        "field": "@timestamp",
        "interval": "day",
        "min_doc_count": 0,
        "order": {
          "_key": "desc"
        },
        "time_zone": "+08:00",
        "format": "yyyy-MM-dd"
      },
      "aggs": {
        "item1000_sum": {
          "sum": {
            "field": "item1000"
          }
        }
      }
    }
  }
}
```


****

#### 二、Elasticsearch助手库（不推荐，因为不灵活。掌握了一的查询参数，其它都是小问题）

##### 说明： 

##### 通用es查询多条数据
* （这一种方式可实现大部分列表查询功能）
* （where条件跟随业务需要修改）
* （多条件组合查询满足不同查询业务）

#### 查询列表: 
```
    //获取分页数据
    $offset = input('offset', 5);
    $limit = input('limit', 5);


    //获取数据
    $input_data = $_POST;
    初始化es
    $es = new BaseElasticsearch();


    //（必须）判断时间查询设置索引
    if(!empty($input_data['sign_time'])){
        $start_time = explode(' - ', $input_data['sign_time'])[0];
        $end_time = explode(' - ', $input_data['sign_time'])[1];
        //这里动态生成索引
        $es->index = $es->setIndexDate($start_time, $end_time);
    }else{
        //设置默认索引
        $es->index = $es->setDefaultDate();//默认一个月
    }


    //设置查询的文档类型
    $es->type = 'logs';//本例是查询日志类型的数据
    
    //分页计算得到当前页码(可根据自身情况设置)
    $es->current_page = $offset/$limit;
    $es->limit = $limit;


    //关键步骤
    //设置查询条件
    $where = [
        [
            'term' => [
                'statType' => 'draw'
            ]
        ]
    ];
    //可选
    if(!empty($input_data['gameid'])){
        $where[] = [
            'term' => [
                'uid' => $input_data['gameid']
            ]
        ];
    }
    //时间
    if(!empty($input_data['sign_time'])){
        //这里将字符串日期转换为unix时间戳(精确到毫秒)
        $date_arr = $es->strtounixtime($input_data['sign_time']);
        $where[] = [
            'range' => [
                'unixtime' => [
                    'gte' => $date_arr['start_time'],
                    'lte' => $date_arr['end_time'],
                ]
            ]
        ];
    }
    //排序
    $es->order('unixtime', 'desc');

    //设置bool查询模式
    $es->setQueryBool();
    //必须[setQueryBool & must、must_not、should方法必须在一起使用]
    $es->must($where);


    //执行查询
    $res = $es->query();
    dump($res);

```

#### 查询一条数据(完全可以用查询列表功能，组合多条件筛选出一条数据)：

```
    //根据id查询一条数据
    //初始化es
    $es = new BaseElasticsearch();

    $es->index_sign = 'qa-item-';//设置index标识

    $es->index = $es->setDefaultDate();//设置文档索引

    $es->type = 'logs';//设置文档类型
    $res = $es->getById('AXbaw_CGbqvMMYZl-lOz');
    dump($res);
```


####  查询案例

```
<?php
/*
 * description：
 * author：wh
 * email：
 * createTime：{2021/1/4} {9:30} 
 */

namespace app\admin\logic;

//帐变
use think\Db;
use think\Exception;
use wanghua\general_utility_tools_php\tool\Tools;

class EsAccountChangeRecord extends EsBaseLogic
{
    //参赛券
    function match(){
        try{
            //帐变前数额字段
            $amount_field = 'item1000';//只能是字符串
            //帐变后数额字段
            $after_amount_field = 'package1000';//只能是字符串

            $input_data = json_decode(input('filter'), true);
            $offset = input('offset', 5);
            $limit = input('limit', 5);
            $es = new BaseElasticsearch();
            //判断时间查询，动态生成索引
            if(!empty($input_data['change_time'])){
                $start_time = explode(' - ', $input_data['change_time'])[0];
                $end_time = explode(' - ', $input_data['change_time'])[1];
                $es->index = $es->setIndexDate($start_time, $end_time);
            }else{
                $es->index = $es->setDefaultDate();//默认一个月
            }
            $es->type = 'logs';
            $es->current_page = $limit ? intval($offset / $limit) + 1 : 1;
            $es->limit = $limit;
            //关键步骤
            //设置查询条件

            //帐变类型:4=后台赠送,6首充奖励,7=报名,8=充值
            $change_type_arr = ['backsend'=>4,'firstprize'=>6,'signup'=>7,'recharge'=>8];
            //必须
            $where = [
                [
                    'range' => [
                        $amount_field => ['gt' => -999999]
                    ]
                ]
            ];

            //可选
            if(!empty($input_data['gameid'])){
                $where[] = [
                    'term' => [
                        'uid' => $input_data['gameid']
                    ]
                ];
            }
            if(!empty($input_data['nickname'])){
                $where[] = [
                    'term' => [
                        'nickname' => $input_data['nickname']
                    ]
                ];
            }
            //帐变类型
            if(!empty($input_data['change_type'])){
                $where[] = [
                    'term' => [
                        'statType' => array_flip($change_type_arr)[$input_data['change_type']]
                    ]
                ];
            }
            //帐变时间
            if(!empty($input_data['change_time'])){
                $date_arr = $es->strtounixtime($input_data['change_time']);

                $where[] = [
                    'range' => [
                        'unixtime' => [
                            'gte' => $date_arr['start_time'],
                            'lte' => $date_arr['end_time'],
                        ]
                    ]
                ];
            }

            //排序
            $es->order('unixtime', 'desc');

            //昵称查询要先查uid 再去es查昵称

            //$es->queryRange('unixtime', [
            //    'gt' => 1609750662180,
            //    'lt' => 1609750662184
            //]);
            //$es->queryFieldExist($amount_field);
            $es->setQueryBool();
            //必须[setQueryBool & must,must_not, should必须在一起使用]
            $es->must($where);

            //执行查询
            Tools::log_to_write_txt(['执行查询,入参：',$es->params], 'es_player_ticket_change_record');
            $res = $es->query();
            Tools::log_to_write_txt(['执行查询,出参：',$res], 'es_player_ticket_change_record');

            //删除本地临时数据
            Db::execute('truncate table fa_zc_player_ticket_change_record');

            if(empty($res['hits']['hits'])) return set_result(500, '未查询到相关数据.');

            $lists = $res['hits']['hits'];
            $this->total = $res['hits']['total'];

            //组装入库字段
            $items = [];
            foreach ($lists as $list){
                $item = [];
                $item['gameid'] = empty($list['_source']['uid'])?'':$list['_source']['uid'];
                $item['nickname'] = empty($list['_source']['nickname'])?'':$list['_source']['nickname'];
                $item['change_type'] = empty($change_type_arr[$list['_source']['statType']])?'0':$change_type_arr[$list['_source']['statType']].'';

                $item['amount'] = empty($list['_source'][$amount_field])?0:$list['_source'][$amount_field];//帐变额

                $after_amount = empty($list['_source'][$after_amount_field])?0:$list['_source'][$after_amount_field];
                $item['after_amount'] = $after_amount;
                $item['before_amount'] = $item['after_amount']-$item['amount'];//帐变前=帐变后-帐变额
                $item['change_time'] = floor($list['_source']['unixtime']/1000);
                $item['api_key'] = $list['_id'];
                $items[] = $item;
            }

            //记录入库数据
            Tools::log_to_write_txt('记录入库数据 | '.json_encode($items, JSON_UNESCAPED_UNICODE), 'es_player_ticket_change_record');

            //入库
            Db::table('fa_zc_player_ticket_change_record')->insertAll($items);
            return set_result(200, 'ok');
        }catch (Exception $exception){
            Tools::log_to_write_txt('未查询到相关数据. error: '.$exception->getMessage().' | '.$exception->getTraceAsString(), 'es_player_ticket_change_record');
            return set_result(500, '未查询到相关数据.'.$exception->getMessage());
        }
    }

    //现金
    function money(){
        try{
            //帐变前数额字段
            $amount_field = 'item1002';//现金帐变   只能是字符
            //帐变后数额字段
            $after_amount_field = 'package1002';//现金帐变   只能是字符

            $input_data = json_decode(input('filter'), true);
            $offset = input('offset', 5);
            $limit = input('limit', 5);
            $es = new BaseElasticsearch();
            //判断时间查询，动态生成索引
            if(!empty($input_data['change_time'])){
                $start_time = explode(' - ', $input_data['change_time'])[0];
                $end_time = explode(' - ', $input_data['change_time'])[1];
                $es->index = $es->setIndexDate($start_time, $end_time);
            }else{
                $es->index = $es->setDefaultDate();//默认一个月
            }
            $es->type = 'logs';
            $es->current_page = $limit ? intval($offset / $limit) + 1 : 1;
            $es->limit = $limit;
            //关键步骤
            //设置查询条件
            //帐变类型:3=比赛奖励,4=后台赠送 99暂定提现
            $change_type_arr = [
                //账变来源大分类 (draw：抽奖，exchange：兑换，winprize：比赛奖励，backsend：后台赠送，sign：签到，firstprize：首充奖励，signup：报名，recharge：充值，useprop：使用道具，cashout：提现，regsend：注册赠送)
                'winprize'=>3,
                'backsend'=>4,
                'cashout'=>99
            ];
            //必须
            $where = [
                [
                    'range' => [
                        $amount_field => ['gt' => -999999]
                    ]
                ],
            ];

            //可选
            if(!empty($input_data['gameid'])){
                $where[] = [
                    'term' => [
                        'uid' => $input_data['gameid']
                    ]
                ];
            }
            if(!empty($input_data['nickname'])){
                $where[] = [
                    'term' => [
                        'nickname' => $input_data['nickname']
                    ]
                ];
            }
            //帐变类型
            if(!empty($input_data['change_type'])){
                $where[] = [
                    'term' => [
                        'statType' => array_flip($change_type_arr)[$input_data['change_type']]
                    ]
                ];
            }
            //帐变时间
            if(!empty($input_data['change_time'])){
                $date_arr = $es->strtounixtime($input_data['change_time']);

                $where[] = [
                    'range' => [
                        'unixtime' => [
                            'gte' => $date_arr['start_time'],
                            'lte' => $date_arr['end_time'],
                        ]
                    ]
                ];
            }

            //排序
            $es->order('unixtime', 'desc');

            //$es->queryRange('unixtime', [
            //    'gt' => 1609750662180,
            //    'lt' => 1609750662184
            //]);
            //$es->queryFieldExist($amount_field);
            $es->setQueryBool();
            //必须[setQueryBool & must,must_not, should必须在一起使用]
            $es->must($where);

            //执行查询
            Tools::log_to_write_txt(['执行查询,入参：',$es->params], 'es_player_cash_change_record');
            $res = $es->query();
            Tools::log_to_write_txt(['执行查询,出参：',$res], 'es_player_cash_change_record');

            //删除本地临时数据
            Db::execute('truncate table fa_zc_player_cash_change_record');

            if(empty($res['hits']['hits'])) return set_result(500, '未查询到相关数据.');

            $lists = $res['hits']['hits'];
            $this->total = $res['hits']['total'];
            //组装入库字段
            $items = [];
            foreach ($lists as $list){
                $item = [];
                $item['gameid'] = empty($list['_source']['uid'])?'':$list['_source']['uid'];
                $item['nickname'] = empty($list['_source']['nickname'])?'':$list['_source']['nickname'];
                $item['change_type'] = empty($change_type_arr[$list['_source']['statType']])?'0':$change_type_arr[$list['_source']['statType']].'';

                $item['amount'] = empty($list['_source'][$amount_field])?'0':$list['_source'][$amount_field].'';//帐变额

                $after_amount = empty($list['_source'][$after_amount_field])?0:$list['_source'][$after_amount_field];
                $item['after_amount'] = $after_amount;
                $item['before_amount'] = $item['after_amount']-$item['amount'];//帐变前=帐变后-帐变额
                $item['change_time'] = floor($list['_source']['unixtime']/1000);
                $item['api_key'] = $list['_id'];
                $items[] = $item;
            }
            //记录入库数据
            Tools::log_to_write_txt('记录入库数据 | '.json_encode($items, JSON_UNESCAPED_UNICODE), 'es_player_cash_change_record');

            //入库
            Db::table('fa_zc_player_cash_change_record')->insertAll($items);
            return set_result(200, 'ok');
        }catch (Exception $exception){
            Tools::log_to_write_txt('未查询到相关数据. error: '.$exception->getMessage().' | '.$exception->getTraceAsString(), 'es_player_cash_change_record');
            return set_result(500, '未查询到相关数据.'.$exception->getMessage());
        }
    }

    //礼券
    function gift(){
        try{
            //帐变前数额字段
            $amount_field = 'item1001';//只能是字符
            //帐变后数额字段
            $after_amount_field = 'package1001';//只能是字符

            $input_data = json_decode(input('filter'), true);
            $offset = input('offset', 5);

            $limit = input('limit', 5);
            $es = new BaseElasticsearch();
            //判断时间查询，动态生成索引
            if(!empty($input_data['change_time'])){
                $start_time = explode(' - ', $input_data['change_time'])[0];
                $end_time = explode(' - ', $input_data['change_time'])[1];
                $es->index = $es->setIndexDate($start_time, $end_time);
            }else{
                $es->index = $es->setDefaultDate();//默认一个月
            }
            $es->type = 'logs';
            $es->current_page = $limit ? intval($offset / $limit) + 1 : 1;
            $es->limit = $limit;
            //关键步骤
            //设置查询条件
            //帐变类型:1=抽奖,2=兑换,3=比赛奖励,4=后台赠送,5=签到
            $change_type_arr = ['draw'=>1,'exchange'=>2,'winprize'=>3,'backsend'=>4,'sign'=>5];
            //必须
            $where = [
                [
                    'range' => [
                        $amount_field => ['gt' => -999999]
                    ]
                ],
            ];

            //可选
            if(!empty($input_data['gameid'])){
                $where[] = [
                    'term' => [
                        'uid' => $input_data['gameid']
                    ]
                ];
            }
            if(!empty($input_data['nickname'])){
                $where[] = [
                    'term' => [
                        'nickname' => $input_data['nickname']
                    ]
                ];
            }
            //帐变类型
            if(!empty($input_data['change_type'])){
                $where[] = [
                    'term' => [
                        'statType' => array_flip($change_type_arr)[$input_data['change_type']]
                    ]
                ];
            }
            //帐变时间
            if(!empty($input_data['change_time'])){
                $date_arr = $es->strtounixtime($input_data['change_time']);

                $where[] = [
                    'range' => [
                        'unixtime' => [
                            'gte' => $date_arr['start_time'],
                            'lte' => $date_arr['end_time'],
                        ]
                    ]
                ];
            }

            //排序
            $es->order('unixtime', 'desc');

            //$es->queryRange('unixtime', [
            //    'gt' => 1609750662180,
            //    'lt' => 1609750662184
            //]);
            //$es->queryFieldExist($amount_field);
            $es->setQueryBool();
            //必须[setQueryBool & must,must_not, should必须在一起使用]
            $es->must($where);

            //执行查询
            Tools::log_to_write_txt(['执行查询,入参：'], 'es_player_gift_change_record');
            $res = $es->query();

            Tools::log_to_write_txt(['执行查询,出参：', $es->params, $res], 'es_player_gift_change_record');

            //删除本地临时数据
            Db::execute('truncate table fa_zc_player_gift_change_record');

            if(empty($res['hits']['hits'])) return set_result(500, '未查询到相关数据.');

            $lists = $res['hits']['hits'];

            $this->total = $res['hits']['total'];
            //组装入库字段
            $items = [];
            foreach ($lists as $list){
                $item = [];
                $item['gameid'] = empty($list['_source']['uid'])?'':$list['_source']['uid'];
                $item['nickname'] = empty($list['_source']['nickname'])?'':$list['_source']['nickname'];
                $item['change_type'] = empty($change_type_arr[$list['_source']['statType']])?'0':$change_type_arr[$list['_source']['statType']].'';

                $item['amount'] = empty($list['_source'][$amount_field])?'0':$list['_source'][$amount_field].'';//帐变额

                $after_amount = empty($list['_source'][$after_amount_field])?0:$list['_source'][$after_amount_field];
                $item['after_amount'] = $after_amount;
                $item['before_amount'] = $item['after_amount']-$item['amount'];//帐变前=帐变后-帐变额
                $item['change_time'] = floor($list['_source']['unixtime']/1000);
                $item['api_key'] = $list['_id'];
                $items[] = $item;
            }
            //记录入库数据
            Tools::log_to_write_txt(['记录入库数据',$items], 'es_player_gift_change_record');

            //入库
            Db::table('fa_zc_player_gift_change_record')->insertAll($items);
            return set_result(200, 'ok', $res);
        }catch (Exception $exception){
            Tools::log_to_write_txt('未查询到相关数据. error: '.$exception->getMessage().' | '.$exception->getTraceAsString(), 'es_player_gift_change_record');
            return set_result(500, '未查询到相关数据.'.$exception->getMessage());
        }

    }

}
```

#### 聚合案例

##### sum求和
``` 
<?php
/*
 * description：
 * author：wh
 * email：
 * createTime：{2021/1/18} {11:37} 
 */

namespace app\admin\logic;


use think\Db;
use think\Exception;
use wanghua\general_utility_tools_php\Date;
use wanghua\general_utility_tools_php\tool\Tools;

class EsMatchCardChargeStatistics extends EsBaseLogic
{


    /**
     * desc：统计总充值记录
     * author：wh
     * @return array|\think\response\Json
     */
    function total(){

        $input_data = json_decode(input('filter'), true);

        $es = new BaseElasticsearch();
        //判断时间查询，动态生成索引
        if(!empty($input_data['order_itme'])){
            $start_time = explode(' - ', $input_data['order_itme'])[0];
            $end_time = explode(' - ', $input_data['order_itme'])[1];
            $es->index = $es->setIndexDate($start_time, $end_time);
        }else{
            $es->index = $es->setDefaultDate();//默认一个月
        }

        $es->type = 'logs';
        $es->current_page = 1;
        $es->limit = 99999999;
        //关键步骤
        //设置查询条件
        $where = [
            [
                'term' => [
                    'statType' => 'recharge'//充值类型
                ]
            ],
            [
                'range' => [
                    //参赛券
                    'item1000' => [
                        'gte' => 0,//不可能是负数
                    ]
                ]
            ],
            [
                'range' => [
                    //参赛券
                    'item1002' => [
                        'lt' => 0,//item1002(奖励金)<0
                    ]
                ]
            ]
        ];
        //可选
        if(!empty($input_data['gameid'])){
            $where[] = [
                'term' => [
                    'uid' => $input_data['gameid']
                ]
            ];
        }
        //时间
        if(!empty($input_data['order_itme'])){
            $date_arr = $es->strtounixtime($input_data['order_itme']);
            $where[] = [
                'range' => [
                    'unixtime' => [
                        'gte' => $date_arr['start_time'],
                        'lte' => $date_arr['end_time'],
                    ]
                ]
            ];
        }
        if(!empty($input_data['order_status'])){
            $where[] = [
                'term' => [
                    'status' => $input_data['order_status']
                ]
            ];
        }

        //排序
        //$es->order('unixtime', 'desc');
        //设置bool查询模式
        $es->setQueryBool();
        //必须[setQueryBool & must,must_not, should必须在一起使用]
        $es->must($where);
        $es->setAggs('item1000', 0);
        //执行查询
        $res = $es->sum();


        return $res['aggregations']['sum']['value'];
    }


    /**
     * desc：查询本月总充值
     * author：wh
     * @return array|\think\response\Json
     */
    function month(){

        $input_data = json_decode(input('filter'), true);

        $es = new BaseElasticsearch();
        //判断时间查询，动态生成索引
        if(!empty($input_data['order_itme'])){
            $start_time = explode(' - ', $input_data['order_itme'])[0];
            $end_time = explode(' - ', $input_data['order_itme'])[1];
            $es->index = $es->setIndexDate($start_time, $end_time);
        }else{
            $es->index = $es->setDefaultDate();//默认一个月
        }

        $es->type = 'logs';
        $es->current_page = 1;
        $es->limit = 99999999;
        //关键步骤
        //设置查询条件
        $where = [
            [
                'term' => [
                    'statType' => 'recharge'//充值类型
                ]
            ],
            [
                'range' => [
                    //参赛券
                    'item1000' => [
                        'gte' => 0,//不可能是负数
                    ]
                ]
            ],
            [
                'range' => [
                    //参赛券
                    'item1002' => [
                        'lt' => 0,//item1002(奖励金)<0
                    ]
                ]
            ],
            [
                'range' => [
                    //参赛券
                    'unixtime' => [
                        'gt' => strtotime(date('Y-m').'-01').'000',
                    ]
                ]
            ]
        ];
        //可选
        if(!empty($input_data['gameid'])){
            $where[] = [
                'term' => [
                    'uid' => $input_data['gameid']
                ]
            ];
        }
        //时间
        if(!empty($input_data['order_itme'])){
            $date_arr = $es->strtounixtime($input_data['order_itme']);
            $where[] = [
                'range' => [
                    'unixtime' => [
                        'gte' => $date_arr['start_time'],
                        'lte' => $date_arr['end_time'],
                    ]
                ]
            ];
        }
        //订单状态（es的充值记录都是充值成功的数据）
        //if(!empty($input_data['order_status'])){
        //    $where[] = [
        //        'term' => [
        //            'status' => $input_data['order_status']
        //        ]
        //    ];
        //}

        //排序
        //$es->order('unixtime', 'desc');
        //设置bool查询模式
        $es->setQueryBool();
        //必须[setQueryBool & must,must_not, should必须在一起使用]
        $es->must($where);
        $es->setAggs('item1000', 0);
        //执行查询
        $res = $es->sum();


        return $res['aggregations']['sum']['value'];
    }
    /**
     * desc：查询本周总充值
     * author：wh
     * @return array|\think\response\Json
     */
    function week(){

        $input_data = json_decode(input('filter'), true);

        $es = new BaseElasticsearch();
        //判断时间查询，动态生成索引
        if(!empty($input_data['order_itme'])){
            $start_time = explode(' - ', $input_data['order_itme'])[0];
            $end_time = explode(' - ', $input_data['order_itme'])[1];
            $es->index = $es->setIndexDate($start_time, $end_time);
        }else{
            $es->index = $es->setDefaultDate();//默认一个月
        }

        $es->type = 'logs';
        $es->current_page = 1;
        $es->limit = 99999999;
        //关键步骤
        //设置查询条件
        $where = [
            [
                'term' => [
                    'statType' => 'recharge'//充值类型
                ]
            ],
            [
                'range' => [
                    //参赛券
                    'item1000' => [
                        'gte' => 0,//不可能是负数
                    ]
                ]
            ],
            [
                'range' => [
                    //参赛券
                    'item1002' => [
                        'lt' => 0,//item1002(奖励金)<0
                    ]
                ]
            ],
            [
                'range' => [
                    //本周
                    'unixtime' => [
                        'gte' => strtotime((new Date())->beginWeek()).'000',
                        'lte' => strtotime((new Date())->endWeek()).'000',
                    ]
                ]
            ]
        ];
        //可选
        if(!empty($input_data['gameid'])){
            $where[] = [
                'term' => [
                    'uid' => $input_data['gameid']
                ]
            ];
        }
        //时间
        if(!empty($input_data['order_itme'])){
            $date_arr = $es->strtounixtime($input_data['order_itme']);
            $where[] = [
                'range' => [
                    'unixtime' => [
                        'gte' => $date_arr['start_time'],
                        'lte' => $date_arr['end_time'],
                    ]
                ]
            ];
        }
        //订单状态（es的充值记录都是充值成功的数据）
        //if(!empty($input_data['order_status'])){
        //    $where[] = [
        //        'term' => [
        //            'status' => $input_data['order_status']
        //        ]
        //    ];
        //}

        //排序
        //$es->order('unixtime', 'desc');
        //设置bool查询模式
        $es->setQueryBool();
        //必须[setQueryBool & must,must_not, should必须在一起使用]
        $es->must($where);
        $es->setAggs('item1000', 0);
        //执行查询
        $res = $es->sum();


        return $res['aggregations']['sum']['value'];
    }
    /**
     * desc：查询今日总充值
     * author：wh
     * @return array|\think\response\Json
     */
    function today(){

        $input_data = json_decode(input('filter'), true);

        $es = new BaseElasticsearch();
        //判断时间查询，动态生成索引
        if(!empty($input_data['order_itme'])){
            $start_time = explode(' - ', $input_data['order_itme'])[0];
            $end_time = explode(' - ', $input_data['order_itme'])[1];
            $es->index = $es->setIndexDate($start_time, $end_time);
        }else{
            $es->index = $es->setDefaultDate();//默认一个月
        }

        $es->type = 'logs';
        $es->current_page = 1;
        $es->limit = 99999999;
        //关键步骤
        //设置查询条件
        $where = [
            [
                'term' => [
                    'statType' => 'recharge'//充值类型
                ]
            ],
            [
                'range' => [
                    //参赛券
                    'item1000' => [
                        'gte' => 0,//不可能是负数
                    ]
                ]
            ],
            [
                'range' => [
                    //参赛券
                    'item1002' => [
                        'lt' => 0,//item1002(奖励金)<0
                    ]
                ]
            ],
            [
                'range' => [
                    //今日
                    'unixtime' => [
                        'gt' => strtotime(date('Y-m-d')).'000',
                    ]
                ]
            ]
        ];
        //可选
        if(!empty($input_data['gameid'])){
            $where[] = [
                'term' => [
                    'uid' => $input_data['gameid']
                ]
            ];
        }
        //时间
        if(!empty($input_data['order_itme'])){
            $date_arr = $es->strtounixtime($input_data['order_itme']);
            $where[] = [
                'range' => [
                    'unixtime' => [
                        'gte' => $date_arr['start_time'],
                        'lte' => $date_arr['end_time'],
                    ]
                ]
            ];
        }
        //订单状态（es的充值记录都是充值成功的数据）
        //if(!empty($input_data['order_status'])){
        //    $where[] = [
        //        'term' => [
        //            'status' => $input_data['order_status']
        //        ]
        //    ];
        //}

        //排序
        //$es->order('unixtime', 'desc');
        //设置bool查询模式
        $es->setQueryBool();
        //必须[setQueryBool & must,must_not, should必须在一起使用]
        $es->must($where);
        $es->setAggs('item1000', 0);
        //执行查询
        $res = $es->sum();


        return $res['aggregations']['sum']['value'];
    }





}
```

#### 或查询（相当于mysql or查询）
``` 
/**
     * desc：或查询（相当于mysql or查询）
     * author：wh
     * @return array|\think\response\Json
     */
    function total(){

        $input_data = json_decode(input('filter'), true);

        $es = new BaseElasticsearch();
        //判断时间查询，动态生成索引
        if(!empty($input_data['conversion_time'])){
            $start_time = explode(' - ', $input_data['conversion_time'])[0];
            $end_time = explode(' - ', $input_data['conversion_time'])[1];
            $es->index = $es->setIndexDate($start_time, $end_time);
        }else{
            //$es->index = $es->setDefaultDate();//默认一个月
            //默认查询所有历史记录
            $es->index = $es->setIndexAllDate();
        }

        $es->type = 'logs';
        $es->current_page = 1;
        $es->limit = 999999999;
        //关键步骤
        //设置查询条件
        //
        $where = [
            [
                'term' => [
                    'statType' => 'exchange'//兑换类型
                ]
            ],

        ];
        //可选
        if(!empty($input_data['gameid'])){
            $where[] = [
                'term' => [
                    'uid' => $input_data['gameid']
                ]
            ];
        }
        //昵称
        if(!empty($input_data['nickname'])){
            $where[] = [
                'term' => [
                    'nickname' => $input_data['nickname']
                ]
            ];
        }
        //时间
        if(!empty($input_data['conversion_time'])){
            $date_arr = $es->strtounixtime($input_data['conversion_time']);
            $where[] = [
                'range' => [
                    'unixtime' => [
                        'gte' => $date_arr['start_time'],
                        'lte' => $date_arr['end_time'],
                    ]
                ]
            ];
        }

        $should_where = [
            [
                "range"=> [
                    "item2001"=> [
                        "gte"=> -9999999
                    ]
                ]
            ],
            [
                "range"=> [
                    "item2002"=> [
                        "gte"=> -9999999
                    ]
                ]
            ],
            [
                "range"=> [
                    "item2005"=> [
                        "gte"=> -9999999
                    ]
                ]
            ],
            [
                "range"=> [
                    "item2010"=> [
                        "gte"=> -9999999
                    ]
                ]
            ]
        ];


        //排序
        //$es->order('unixtime', 'desc');
        //设置bool查询模式
        $es->setQueryBool();
        //必须[setQueryBool & must,must_not, should必须在一起使用]
        $es->must($where);
        $es->should($should_where);
        $es->setAggs('item1001', 1);
        //执行查询
        $res = $es->sum();

        return $res['aggregations']['sum']['value'];
    }
```