<?php
/*
 * description：
 * author：wh
 * email：
 * createTime：{2020/11/11} {10:23} 
 */

namespace wanghua\general_utility_tools_php\log;

/**
 * @deprecated 预计2025年01月01日移除
 *
 * 仅支持ThinkPHP框架
 * Class Mysql
 * @package wanghua\general_utility_tools_php\log
 */
class Mysql
{
    //日志数据表前缀,如果没有前缀，可能和已有表名称冲突
    protected static $prefix = 'fa_sys_';
    //日志表名
    protected static $table = 'log';

    public function __construct()
    {

    }

    /**
     * desc：
     * author：wh
     * @param $title
     * @param string $content
     */
    function write($title, $content=''){
        Db::table(self::$prefix.self::$table)->insert([
            'title'=>$title, 'content'=>$content,
        ]);
    }
}