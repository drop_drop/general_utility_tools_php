<?php
/*
 * description：
 * author：wh
 * email：
 * createTime：{2020/11/11} {10:08} 
 */

namespace wanghua\general_utility_tools_php\log;

/**
 * @deprecated 预计2025年01月01日移除
 * Class File
 * @package wanghua\general_utility_tools_php\log
 */
class File
{
    //目录
    protected $log_dir = '';

    public function __construct()
    {
        $vendor_dir = __DIR__;
        $index = strpos($vendor_dir, 'vendor')?:0;

        $this->log_dir = (substr($vendor_dir,0,$index)).'runtime/toollog/'.date('Ymd');
        file_exists($this->log_dir)?:mkdir($this->log_dir, 0777, true);
        $this->log_dir = $this->log_dir.'/'.date('Y-m-d-H').'.log';
    }

    /**
     * desc：
     * author：wh
     * @param $title
     * @param string $content
     */
    function write(string $title, string $content=''){
        $string = "\r\n".$title.' '. $content;
        file_put_contents($this->log_dir, $string, FILE_APPEND);
    }
}