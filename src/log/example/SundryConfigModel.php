<?php
/*
 * description：
 * author：wh
 * email：
 * createTime：{2021/08/25} {17:37} 
 */

namespace app\common\model;


use think\Db;

/**
 * @deprecated 预计2025年01月01日移除
 *
 * Class SundryConfigModel
 * @package app\common\model
 */
class SundryConfigModel
{
    protected static $self_table = 'fa_zc_sundry_config';


    static function getConfigVal(string $key){

        return Db::table(self::$self_table)->where('key',$key)->value('val');
    }

}