<?php
/*
 * description：
 * author：wh
 * email：
 * createTime：{2022/02/14} {16:50} 
 */
namespace wanghua\general_utility_tools_php\log\example;

use wanghua\general_utility_tools_php\log\Driver;

/**
 * 行为定义
 *
 * 使用方法，请阅读example/README.MD，第“使用驱动记录日志”项。
 *
 *
 * Class LoggerBehavior
 * @package app\behavior
 */
class LoggerBehavior
{
    //function run(){
    //
    //}


    function appInit(){
        if(!request()->LogObj) {
            request()->LogObj = new Driver();
        }
        request()->LogObj->write(['app start']);



        //业务日志对象
        if(!request()->ServeLogObj) {
            request()->ServeLogObj = new Driver();
            request()->ServeLogObj->setDriver('file');
        }
        request()->ServeLogObj->write(['app serve start']);
    }
    function appEnd(){
        //框架生命周期日志对象
        request()->LogObj->write(['app end']);
        request()->LogObj->flush();



        //业务日志对象
        request()->ServeLogObj->write(['app serve end']);
        request()->ServeLogObj->flush();
    }
}