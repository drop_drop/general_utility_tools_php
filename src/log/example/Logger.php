<?php
/*
 * description：
 * author：wh
 * email：
 * createTime：{2021/12/07} {13:44} 
 */

namespace app\common\tools;


use app\common\model\SundryConfigModel;
use wanghua\general_utility_tools_php\log\Logger as LoggerObj;
use wanghua\general_utility_tools_php\tool\Tools;

/**
 * @deprecated 预计2025年01月01日移除
 *
 * 通用日志工具类
 *
 * 请将此类放入项目目录app\common\tools中再调用
 * 请将SundryConfigModel类放入项目目录app\common\model中再调用
 *
 * 使用案例：
 *
$rspArray = ['asfdasd'=>['1111'=>['222'=>['测试'=>'测试内容']]]];
$res = Logger::log_email(['aaaaaaaa'=>['1111'=>['222']]],'test_log');
dump($res);
$res = Logger::log_email(['bbbbbbbbbbbbbbb',$rspArray],'test_log');
dump($res);
 *
 * Class Logger
 * @package app\common\tools
 */
class Logger
{

    /**
     * desc：开启调试模式
     * author：wh
     */
    static function open_debug(){
        LoggerObj::open_debug();
    }

    /**
     * desc：关闭调试模式
     * author：wh
     */
    static function close_debug(){
        LoggerObj::close_debug();
    }
    /**
     * desc：系统未来通用统一日志记录
     *
     * eg：Logger::log(['测试测试测试测试.',$rspArray],$this->wechat_pay_log);//普通日志写入
     *
     * author：wh
     * @param array $data
     * @param string $file_log_name
     */
    static function log(array $data=[], string $file_log_name){
        try {
            //服务器配置 start
            $send_server_nickname = SundryConfigModel::getConfigVal('send_server_username');
            $send_server_username = SundryConfigModel::getConfigVal('send_server_username');
            $send_server_pwd = SundryConfigModel::getConfigVal('send_server_pwd');
            $send_server_host = SundryConfigModel::getConfigVal('send_server_host');
            //服务器配置 end

            //收件人信息 start
            $receiver = SundryConfigModel::getConfigVal('admin_error_log_email');
            $receiver_nickname = '日志管理员';//默认收件人昵称
            //收件人信息 end

            //写入日志
            LoggerObj::set_email_config($send_server_nickname,$send_server_username,$send_server_pwd,$send_server_host);
            LoggerObj::set_receiver_config($receiver,$receiver_nickname);
            return LoggerObj::write_text($data,$file_log_name);
        }catch (\Exception $e){
            Tools::log_to_write_txt(['error'=>'系统未来通用统一日志记录，出错.'.$e->getMessage(),'日志入参：'=>$data,'error_info'=>$e->getTraceAsString()],$file_log_name);
            return ['code'=>500,'msg'=>'日志写入异常.'.$e->getMessage()];
        }
    }

    /**
     * desc：记录错误信息并发送邮件
     *
     * eg：Logger::log_email(['收银宝统一支付-支付失败.',$rspArray],$this->wechat_pay_log);
     *
     * 小贴士：即将写入的数据将被添加一个error键名，再调用底层日志写入
     *
     * 使用案例：
     *
    $rspArray = ['asfdasd'=>['1111'=>['222'=>['测试'=>'测试内容']]]];
    $res = Logger::log_email(['aaaaaaaa'=>['1111'=>['222']]],'test_log');
    dump($res);
    $res = Logger::log_email(['bbbbbbbbbbbbbbb',$rspArray],'test_log');
    dump($res);
     *
     *
     * 【注】
     * 必须的邮件配置包含：
     *
    send_server_nickname    邮件发送服务器昵称
    send_server_username    邮件发送服务器账号
    send_server_pwd     邮件发送服务器密码
    send_server_host    邮件发送服务器
    admin_error_log_email   接收异常日志的管理员邮箱
     *
     * sql，复制可用：
    INSERT INTO `fa_zc_sundry_config` ( `name`, `key`, `val`, `msg`, `group`, `ext_one`, `ext_two`, `ext_three`, `create_time`, `update_time`) VALUES ( '邮件发送服务器昵称', 'send_server_nickname', '掌电竞技', '', 'email', '', '', '', '2021-12-07 11:37:11', NULL);
    INSERT INTO `fa_zc_sundry_config` ( `name`, `key`, `val`, `msg`, `group`, `ext_one`, `ext_two`, `ext_three`, `create_time`, `update_time`) VALUES ( '邮件发送服务器账号', 'send_server_username', '1003076950@qq.com', '', 'email', '', '', '', '2021-12-07 11:37:11', NULL);
    INSERT INTO `fa_zc_sundry_config` ( `name`, `key`, `val`, `msg`, `group`, `ext_one`, `ext_two`, `ext_three`, `create_time`, `update_time`) VALUES ( '邮件发送服务器密码', 'send_server_pwd', 'pdosdyjuurowbcfc', '', 'email', '', '', '', '2021-12-07 11:37:11', NULL);
    INSERT INTO `fa_zc_sundry_config` ( `name`, `key`, `val`, `msg`, `group`, `ext_one`, `ext_two`, `ext_three`, `create_time`, `update_time`) VALUES ( '邮件发送服务器', 'send_server_host', 'smtp.qq.com', '', 'email', '', '', '', '2021-12-07 11:37:11', NULL);
    INSERT INTO `fa_zc_sundry_config` ( `name`, `key`, `val`, `msg`, `group`, `ext_one`, `ext_two`, `ext_three`, `create_time`, `update_time`) VALUES ( '接收异常日志的管理员邮箱', 'admin_error_log_email', '1003076950@qq.com', '', 'email', '', '', '', '2021-12-07 13:48:18', '2021-12-07 13:48:27');

     *
     *
     * author：wh
     * @param array $data
     * @param string $file_log_name
     */
    static function log_email(array $data=[], string $file_log_name){
        try {
            //服务器配置 start
            $send_server_nickname = SundryConfigModel::getConfigVal('send_server_username');
            $send_server_username = SundryConfigModel::getConfigVal('send_server_username');
            $send_server_pwd = SundryConfigModel::getConfigVal('send_server_pwd');
            $send_server_host = SundryConfigModel::getConfigVal('send_server_host');
            //服务器配置 end
            if(empty($send_server_nickname)){
                Tools::log_to_write_txt(['服务器配置错误.发件服务器昵称为空.'],$file_log_name);
                return Tools::set_res(500,'服务器配置错误.发件服务器昵称为空');
            }
            if(empty($send_server_username)){
                Tools::log_to_write_txt(['服务器配置错误.发件服务器邮箱为空.'],$file_log_name);
                return Tools::set_res(500,'服务器配置错误.发件服务器邮箱为空');
            }
            if(empty($send_server_pwd)){
                Tools::log_to_write_txt(['服务器配置错误.发件服务器密码为空.'],$file_log_name);
                return Tools::set_res(500,'服务器配置错误.发件服务器密码为空.');
            }
            if(empty($send_server_host)){
                Tools::log_to_write_txt(['服务器配置错误.发件服务器HOST为空.'],$file_log_name);
                return Tools::set_res(500,'服务器配置错误.发件服务器HOST为空..');
            }

            //收件人信息 start
            $receiver = SundryConfigModel::getConfigVal('admin_error_log_email');
            $receiver_nickname = '日志管理员';//默认收件人昵称
            //收件人信息 end
            if(empty($send_server_host)){
                Tools::log_to_write_txt(['服务器配置错误.发件服务器HOST为空.'],$file_log_name);
                return Tools::set_res(500,'服务器配置错误.发件服务器HOST为空..');
            }

            LoggerObj::set_email_config($send_server_nickname,$send_server_username,$send_server_pwd,$send_server_host);
            LoggerObj::set_receiver_config($receiver,$receiver_nickname);

            //写入日志
            return LoggerObj::write_text_email($data,$file_log_name);
        }catch (\Exception $e){
            Tools::log_to_write_txt(['error'=>'系统未来通用统一日志记录，出错.'.$e->getMessage(),'日志入参：'=>$data,'error_info'=>$e->getTraceAsString()],$file_log_name);
            return ['code'=>500,'msg'=>'日志写入异常.'.$e->getMessage()];
        }
    }
}