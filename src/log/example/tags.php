<?php

// +----------------------------------------------------------------------
// | ThinkPHP [ WE CAN DO IT JUST THINK ]
// +----------------------------------------------------------------------
// | Copyright (c) 2006~2016 http://thinkphp.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: liu21st <liu21st@gmail.com>
// +----------------------------------------------------------------------

// 应用行为扩展定义文件


// 动态绑定Request对象属性
//框架生命周期日志（用mysql驱动）
\think\Request::instance()->bind('LogObj',new \wanghua\general_utility_tools_php\log\Driver('mysql'));
//业务日志对象（用文件驱动）
\think\Request::instance()->bind('ServeLogObj',new \wanghua\general_utility_tools_php\log\Driver('file'));


// 注册 app\index\behavior\CheckLang行为类到app_init标签位
\think\Hook::add('app_init', '\\wanghua\\general_utility_tools_php\\log\\example\\LoggerBehavior');
//注册 app\admin\behavior\CronRun行为类到app_init标签位
\think\Hook::add('app_end', '\\wanghua\\general_utility_tools_php\\log\\example\\LoggerBehavior');




return [
    // 应用初始化
    'app_init'     => [],
    // 应用开始
    'app_begin'    => [],
    // 应用调度
    'app_dispatch' => [
        'app\\common\\behavior\\Common',
    ],
    // 模块初始化
    'module_init'  => [
        'app\\common\\behavior\\Common',
    ],
    // 插件开始
    'addon_begin'  => [
        'app\\common\\behavior\\Common',
    ],
    // 操作开始执行
    'action_begin' => [],
    // 视图内容过滤
    'view_filter'  => [],
    // 日志写入
    'log_write'    => [],
    // 应用结束
    'app_end'      => [],
];
