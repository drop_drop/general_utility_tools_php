<?php
/*
 * description：
 * author：wh
 * email：
 * createTime：{2021/12/07} {10:48} 
 */

namespace wanghua\general_utility_tools_php\log;


use wanghua\general_utility_tools_php\phpmailer\Mail;
use wanghua\general_utility_tools_php\phpmailer\SMTP;
use wanghua\general_utility_tools_php\tool\Tools;

/**
 * @deprecated 预计2025年01月01日移除
 *
 * 文件日志专用
 *
 * 邮件配置必须
 *
 * Class Logger
 * @package app\common\tools
 */
class Logger
{
    protected static $debug = 0;//调试模式
    //发件服务器 start
    //qq邮件服务器配置参考：https://service.mail.qq.com/cgi-bin/help?subtype=1&&id=28&&no=331
    protected static $send_server_nickname = '系统异常';//发送方邮件服务器昵称 必须
    protected static $send_server_username = '';//发送方邮件服务器账号 必须 //'1003076966@qq.com';
    protected static $send_server_pwd = '';//发送方邮件服务器密码 必须 //'pdosdyjuurowbcfc';
    protected static $send_server_host = 'smtp.qq.com';//发送方邮件服务器 qq服务器：smtp.qq.com 必须
    //发件服务器 end

    //收件邮箱 start
    protected static $receiver_email = '';//收件人邮箱 必须 eg: 1003076966@qq.com
    protected static $receiver_nickname = '收件人昵称';//收件人昵称 必须
    //收件邮箱 end

    protected static $filter = [];//过滤指定日志，不发送至email

    /**
     * desc：开启调试模式
     * author：wh
     */
    static function open_debug(){
        self::$debug = SMTP::DEBUG_SERVER;
    }

    /**
     * desc：关闭调试模式
     * author：wh
     */
    static function close_debug(){
        self::$debug = SMTP::DEBUG_OFF;
    }
    /**
     * 【必须】
     * desc：设置邮件服务器配置
     * author：wh
     * @param $send_server_nickname
     * @param $send_server_username
     * @param $send_server_pwd
     * @param $send_server_host
     */
    static function set_email_config(string $send_server_nickname,string $send_server_username,string $send_server_pwd,string $send_server_host){
        self::$send_server_nickname = $send_server_nickname;
        self::$send_server_username = $send_server_username;
        self::$send_server_pwd = $send_server_pwd;
        self::$send_server_host = $send_server_host;
    }

    /**
     * 【必须】
     * desc：设置收件人配置
     * author：wh
     * @param $receiver_email
     * @param $receiver_nickname
     */
    static function set_receiver_config(string $receiver_email,string $receiver_nickname){
        self::$receiver_email = $receiver_email;
        self::$receiver_nickname = $receiver_nickname;
    }

    /**
     * desc：过滤指定日志，不发送至email
     * author：wh
     */
    static function set_filter(array $filter=[]){
        self::$filter = $filter;
    }

    /**
     * desc：写入普通日志
     * author：wh
     * @param array $data
     * @param string $log_file_name
     * @return array
     */
    static function write_text(array $data,string $log_file_name){
        try {
            $result = ['code'=>200,'msg'=>'ok'];
            Tools::log_to_write_txt($data,$log_file_name);
            return $result;//含 code、msg、data结构
        }catch (\Exception $e){
            //一般不会出错
            return ['code'=>500,'msg'=>'写入普通日志,日志记录失败.','data'=>['error'=>$e->getMessage(),'error_info'=>$e->getTraceAsString()]];
        }
    }
    /**
     * desc：记录普通日志同时记录邮件日志
     *
     * author：wh
     * @param array $data 如果data里面的一维数组的key包含了被过滤的配置，则当次不发送邮件
     * @param string $log_file_name
     * @return array
     */
    static function write_text_email(array $data,string $log_file_name){
        $keys = array_keys($data);
        try {
            Tools::log_to_write_txt($data,$log_file_name);
            if(self::$filter){
                $is_send = true;//发送
                foreach ($keys as $key){
                    //过滤不发送邮件的key,可以是任意字符串
                    if(in_array($key, self::$filter)){
                        $is_send = false;
                    }
                }
                if($is_send){
                    return self::send($data);
                }
            }
            return self::send($data);
        }catch (\Exception $e){
            //一般不会出错
            return ['code'=>500,'msg'=>'普通日志邮件日志记录失败.','data'=>['error'=>$e->getMessage(),'error_info'=>$e->getTraceAsString()]];
        }
    }

    /**
     * desc：
     * author：wh
     * @param array $data
     * @param string $log_file_name
     * @return array
     */
    protected static function send(array $data = []){
        $body = json_encode($data,JSON_UNESCAPED_UNICODE);
        $body = str_replace('\\"','"',$body);
        $body = str_replace('\\n"','',$body);
        $mail = new Mail(self::$send_server_nickname,self::$send_server_host,self::$send_server_username,self::$send_server_pwd);
        $mail->debug = self::$debug;
        return $mail->send(self::$receiver_email,self::$receiver_nickname,'EMAIL TITLE',$body);
    }
}