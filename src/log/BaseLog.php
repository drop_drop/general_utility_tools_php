<?php
/*
 * description：
 * author：wh
 * email：
 * createTime：{2020/11/10} {18:00} 
 */

namespace wanghua\general_utility_tools_php\log;

/**
 * @deprecated 预计2025年01月01日移除
 *
 * 使用本类之前，将fa_zc_log.sql导入数据库，配置可根据实际需求修改
 * Class BaseLog
 * @package wanghua\general_utility_tools_php\log
 */
class BaseLog
{
    /**
     * 日志存储类型：文件、数据库(mysql)
     * 如果是文件类型，则在runtime目录中tool_log目录按日期生成文件
     * eg:/runtime/tool_log/2020-01-01.log
     * @var string
     */
    protected static $log_type = 'file';//file  or  db

    /**
     * desc：入参
     * author：wh
     * @param string $title
     * @param string $content
     */
    static function in($content='', $title=''){
        $content = $content?$content:input();
        $title = is_string($title)?$title:json_encode($title, JSON_UNESCAPED_UNICODE);
        $content = is_string($content)?$content:json_encode($content);

        $title = date('Ymd H:i:s').' ___IN | '.$title;
        self::ready($title, $content);
    }

    /**
     * desc：出参
     * author：wh
     * @param string $title
     * @param string $content
     */
    static function out($content='', $title=''){
        $title = is_string($title)?$title:json_encode($title, JSON_UNESCAPED_UNICODE);
        $content = is_string($content)?$content:json_encode($content);

        $title = date('Ymd H:i:s').' __OUT | '.$title;
        self::ready($title, $content);
    }

    /**
     * desc：错误信息
     * author：wh
     * @param string $title
     * @param string $content
     */
    static function err($content='', $title=''){
        $content = $content?$content:input();
        $title = is_string($title)?$title:json_encode($title, JSON_UNESCAPED_UNICODE);
        $content = is_string($content)?$content:json_encode($content);

        $title = date('Ymd H:i:s').' ERROR | '.$title;
        self::ready($title, $content);
    }

    /**
     * desc：
     * author：wh
     * @param $title
     * @param $content
     */
    protected static function ready($title, $content){
        self::$log_type != 'file'?:(new File())->write($title, $content);
        self::$log_type != 'db'?:(new Mysql())->write($title, $content);
    }
}