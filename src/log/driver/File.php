<?php
/*
 * description：
 * author：wh
 * email：
 * createTime：{2020/11/11} {10:08} 
 */

namespace wanghua\general_utility_tools_php\log\driver;

use wanghua\general_utility_tools_php\tool\Tools;

/**
 * 文件日志驱动
 *
 * 说明：日志记录在文件中，默认目录 runtime/log，你可以自定义目录，参考setLogDir方法，都将记录在默认目录中。
 *
 * 用法参考：general_utility_tools_php\src\log\Driver.php类
 *
 * Class File
 * @package wanghua\general_utility_tools_php\log\lib
 *
 * 普通使用方法：
 *  $LogObj = new Driver();
    * $LogObj->write('333333');
    * $LogObj->write('444444');
    * $LogObj->write('55555');
    * $LogObj->flush();
 *
 * tp框架使用方法：
 * 1、注册全局LogObj对象属性（参考example/tags.php）
 * 2、使用全局LogObj对象功能
 * eg:
    * //写入数据到内存
    * request()->LogObj->write('333333');
    * request()->LogObj->write('测试');
    * request()->LogObj->write('444444');
    * //持久化存储
    * request()->LogObj->flush();
 *
 *
 * tp框架行为使用方法：
 * 1、在应用目录中添加行为定义文件LoggerBehavior.php（参考example/LoggerBehavior.php）
 * 2、注册全局LogObj对象属性
 * 3、注册行为(侦听标签位)
 *
 */
class File extends Base
{
    //目录
    private $log_dir = 'runtime/log/sys_log';//默认日志目录

    private $log_data = [];
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * desc：设置日志目录
     *
     * write之前调用
     *
     * author：wh
     * @param $log_dir
     */
    function setLogDir($log_dir){
        $this->log_dir = 'runtime/log/'.$log_dir;
    }

    /**
     * desc：获取日志目录
     *
     * author：wh
     * @return string
     */
    function getLogDir(){
        return $this->log_dir;
    }

    /**
     * desc：写日志
     *
     * 先压到内存中，调用flush方法写入磁盘，建议一次不要压太多
     *
     * author：wh
     * @param $content
     */
    function write($content){
        $ip = request()->ip();
        $write_time = $this->getMicrosecond();
        $date = date('Y-m-d H:i:s');
        $tmp = [];
        $tmp['all'] = 0;
        $tmp['date'] = $date;
        $tmp['ip'] = $ip;
        $tmp['url'] = request()->baseUrl();
        $tmp['reqid'] = $this->reqid;

        $tmp_data = [];
        if($this->log_data){
            $use_time = $write_time-$this->log_data['content'][count($this->log_data['content'])-1]['time'];
            $tmp_data['total'] = (array_sum(array_column($this->log_data['content'], 'use')) + $use_time);
            $tmp_data['use'] = ($write_time-$this->log_data['content'][count($this->log_data['content'])-1]['time']);
            $tmp_data['time'] = $write_time;
            $tmp_data['log_info'] = $content;
        }else{
            $tmp_data['total'] = 0;
            $tmp_data['use'] = 0;
            $tmp_data['time'] = $write_time;
            $tmp_data['log_info'] = $content;
        }
        $tmp['input'] = $_POST;
        $this->log_data['base'] = $tmp;
        $this->log_data['content'][] = $tmp_data;
    }

    /**
     * desc：刷新日志数据到文件
     *
     * 仅调用一次
     *
     * author：wh
     */
    function flush(){
        //计算单次request消耗
        $this->log_data['base']['all'] = array_sum(array_column($this->log_data['content'], 'use'));

        //dir
        $this->log_dir = Tools::get_root_path().$this->log_dir.'/'.date('Ymd');

        is_dir($this->log_dir)?:mkdir($this->log_dir, 0777, true);

        $log_file = $this->log_dir.'/log'.date('YmdH').'.txt';

        $base = json_encode($this->log_data['base'], JSON_UNESCAPED_UNICODE);
        $content = json_encode($this->log_data['content'], JSON_UNESCAPED_UNICODE);

        $content_str = str_replace('{"total"',"\n{'total'",$content);
        $content_str = str_replace('"log_info":',"'log_info':\n",$content_str);
        $strdata = "\n".$base ."\n".$content_str;

        file_put_contents($log_file, "\n".$strdata, FILE_APPEND);
    }

    /**
     * desc：获取当次请求日志数据
     *
     * 注：需在write方法之后调用，否则可能不准确
     *
     * author：wh
     * @return array
     */
    function getLogData(){
        return $this->log_data;
    }
}