<?php
/*
 * description：
 * author：wh
 * email：
 * createTime：{2022/02/14} {16:04} 
 */

namespace wanghua\general_utility_tools_php\log\driver;

/**
 * desc：日志驱动基类
 *
 * 用法参考：general_utility_tools_php\src\log\Driver.php类
 *
 * author：wh
 * Class Base
 * @package wanghua\general_utility_tools_php\log\driver
 */
class Base
{

    protected $reqid = '';

    public function __construct()
    {
        $this->reqid = $this->initReqID();
    }

    /**
     * desc：获取日志目录
     *
     * 子类根据情况实现（Email，MySQL这种不需要目录则不实现）
     *
     * author：wh
     * @return string
     */
    function getLogDir(){
        return '';
    }
    /**
     * 获取当前系统时间(精确到毫秒)
     * @return float
     */
    protected function getMillisecond()
    {
        list($t1, $t2) = explode(' ', microtime());
        return sprintf('%.0f', (floatval($t1) + floatval($t2)) * 1000);
    }
    /**
     * 获取当前系统时间(精确到微秒)
     * @return float
     */
    protected function getMicrosecond()
    {
        list($t1, $t2) = explode(' ', microtime());
        return sprintf('%.0f', (floatval($t1) + floatval($t2)) * 1000000);
    }

    /**
     * desc：初始化请求id
     *
     * author：wh
     * @return float
     */
    function initReqID(){
        return $this->getMicrosecond();
    }
    /**
     * desc：获取请求id
     *
     * author：wh
     * @return float
     */
    function getReqID(){
        return $this->reqid;
    }

    function write($content){

    }

    function flush(){

    }

    function getLogData(){

    }
}