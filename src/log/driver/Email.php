<?php
/*
 * description：
 * author：wh
 * email：
 * createTime：{2021/12/07} {10:48} 
 */

namespace wanghua\general_utility_tools_php\log\driver;


use wanghua\general_utility_tools_php\phpmailer\Mail;
use wanghua\general_utility_tools_php\phpmailer\SMTP;
use wanghua\general_utility_tools_php\tool\Tools;

/**
 * 邮件日志驱动
 *
 * 说明：邮件发送到指定邮箱，使用之前请配置邮件账号、密码、服务器等必要参数
 *
 * 用法参考：general_utility_tools_php\src\log\Driver.php类
 *
 * 1. 邮件配置必须;
 * 2. 支持普通文本和html文本;
 *
 *
 * Class Logger
 * @package app\common\tools
 */
class Email extends Base
{
    //file:文本(写文本文件), mysql:数据库, esdb:es数据库(直接存es数据库)，esapi:es接口(调用接口向es存储数据)
    private $driver = 'file';
    protected $debug = 0;//调试模式
    //发件服务器 start
    //qq邮件服务器配置参考：https://service.mail.qq.com/cgi-bin/help?subtype=1&&id=28&&no=331
    protected $send_server_nickname = '系统异常';//发送方邮件服务器昵称 必须
    protected $send_server_username = '';//发送方邮件服务器账号 必须 //'1003076966@qq.com';
    protected $send_server_pwd = '';//发送方邮件服务器密码 必须 //'pdosdyjuurowbcfc';
    protected $send_server_host = 'smtp.qq.com';//发送方邮件服务器 qq服务器：smtp.qq.com 必须
    //发件服务器 end

    //收件邮箱 start
    protected $receiver_email = '';//收件人邮箱 必须 eg: 1003076966@qq.com
    protected $receiver_nickname = '收件人昵称';//收件人昵称 必须
    //收件邮箱 end

    protected $filter = [];//过滤指定日志，不发送至email

    public $port = 465;//国内qq服务器，亚马逊 25、587 或 2587
    public $protocol = 'ssl';//国内QQ是ssl，亚马逊TLS
    public $sender_email = '';//发送方邮箱

    /**
     * desc：开启调试模式
     * author：wh
     */
    function open_debug(){
        $this->debug = SMTP::DEBUG_SERVER;
    }

    /**
     * desc：关闭调试模式
     * author：wh
     */
    function close_debug(){
        $this->debug = SMTP::DEBUG_OFF;
    }
    /**
     * 【必须】
     * desc：设置邮件服务器配置
     * author：wh
     * @param $send_server_nickname
     * @param $send_server_username
     * @param $send_server_pwd
     * @param $send_server_host
     */
    function set_email_config(string $send_server_nickname,string $send_server_username,string $send_server_pwd,string $send_server_host){
        $this->send_server_nickname = $send_server_nickname;
        $this->send_server_username = $send_server_username;
        $this->send_server_pwd = $send_server_pwd;
        $this->send_server_host = $send_server_host;
    }

    /**
     * 【必须】
     * desc：设置收件人配置
     * author：wh
     * @param $receiver_email
     * @param $receiver_nickname
     */
    function set_receiver_config($receiver_email,string $receiver_nickname){
        $this->receiver_email = $receiver_email;
        $this->receiver_nickname = $receiver_nickname;
    }

    /**
     * desc：过滤指定日志，不发送至email
     * author：wh
     */
    function set_filter(array $filter=[]){
        $this->filter = $filter;
    }

    /**
     * desc：记录普通日志同时记录邮件日志
     *
     * author：wh
     * @param array $data 如果data里面的一维数组的key包含了被过滤的配置，则当次不发送邮件
     * @param string $log_file_name
     * @return array
     */
    function write_text_email(array $data,string $log_file_name){
        $keys = array_keys($data);
        try {
            Tools::log_to_write_txt($data,$log_file_name);
            if($this->filter){
                $is_send = true;//发送
                foreach ($keys as $key){
                    //过滤不发送邮件的key,可以是任意字符串
                    if(in_array($key, $this->filter)){
                        $is_send = false;
                    }
                }
                if($is_send){
                    return $this->send($data);
                }
            }
            return $this->send($data);
        }catch (\Exception $e){
            //一般不会出错
            return ['code'=>500,'msg'=>'普通日志邮件日志记录失败.','data'=>['error'=>$e->getMessage(),'error_info'=>$e->getTraceAsString()]];
        }
    }
    /**
     * desc：记录普通日志同时记录邮件日志
     *
     * author：wh
     * @param array $data 如果data里面的一维数组的key包含了被过滤的配置，则当次不发送邮件
     * @param string $log_file_name
     * @return array
     */
    function write_text(string $title,string $body){
        return $this->sendText($title, $body);
    }
    /**
     * desc：记录普通日志同时记录邮件日志
     *
     * author：wh
     * @param array $data 如果data里面的一维数组的key包含了被过滤的配置，则当次不发送邮件
     * @param string $log_file_name
     * @return array
     */
    function write_text_aws(string $title,string $body){
        return $this->sendTextAWS($title, $body);
    }

    /**
     * desc：
     * author：wh
     * @return array|void
     */
    function write($content){
        return $this->sendText($content['title'], $content['body']);
    }
    /**
     * desc：数组格式
     * author：wh
     * @param array $data
     * @param string $log_file_name
     * @return array
     */
    protected function send(array $data = []){
        $body = json_encode($data,JSON_UNESCAPED_UNICODE);
        $body = str_replace('\\"','"',$body);
        $body = str_replace('\\n"','',$body);
        $mail = new Mail($this->send_server_nickname,$this->send_server_host,$this->send_server_username,$this->send_server_pwd,$this->send_server_username,$this->port,$this->protocol);
        $mail->debug = $this->debug;
        return $mail->send($this->receiver_email,$this->receiver_nickname,'EMAIL TITLE',$body);
    }
    /**
     * desc：文本格式
     * author：wh
     * @param array $data
     * @param string $log_file_name
     * @return array
     */
    protected function sendText(string $title='邮件标题',string $body='邮件正文'){
        $mail = new Mail($this->send_server_nickname,$this->send_server_host,$this->send_server_username,$this->send_server_pwd,$this->send_server_username,$this->port,$this->protocol);
        $mail->debug = $this->debug;
        return $mail->send($this->receiver_email,$this->receiver_nickname,$title,$body);
    }

    /**
     * desc：文本格式
     * author：wh
     * @param array $data
     * @param string $log_file_name
     * @return array
     */
    protected function sendTextAWS(string $title='邮件标题',string $body='邮件正文'){
        $mail = new Mail($this->send_server_nickname,$this->send_server_host,$this->send_server_username,$this->send_server_pwd,$this->sender_email,$this->port,$this->protocol);
        $mail->debug = $this->debug;
        return $mail->sendAWS($this->receiver_email,$this->receiver_nickname,$title,$body);
    }
}