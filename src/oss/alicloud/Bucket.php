<?php
/*
 * description：
 * author：wh
 * email：
 * createTime：{2024/5/16} {17:42} 
 */

namespace wanghua\general_utility_tools_php\oss\alicloud;


use OSS\Core\OssException;
use OSS\OssClient;
use wanghua\general_utility_tools_php\Mmodel;

class Bucket extends AliyunOSS
{

    /**
     * desc：创建Bucket
     * author：wh
     * @param string $bucket_name bucket名称 为空则使用bucket，否则以bucket+$bucket_name组成
     */
    function createBucket($bucket_name='',$opts=[],$oss_acl_type = OssClient::OSS_ACL_TYPE_PRIVATE){
        if(empty($this->aliyun_config['bucket'])){
            throw new \Exception('OSS bucket必须');
        }
        $bucket_name = !$bucket_name?$this->aliyun_config['bucket']:$this->aliyun_config['bucket'].'-'.$bucket_name;
        return Mmodel::catch(function () use ($bucket_name,$opts,$oss_acl_type){
            // 填写Bucket名称，例如examplebucket。
            //$bucket_name = "examplebucket";
            // 设置Bucket的存储类型为标准类型。
            $options = $opts?:array(
                OssClient::OSS_STORAGE => OssClient::OSS_STORAGE_STANDARD
            );
            return $this->ossClient->createBucket($bucket_name, $oss_acl_type, $options);
        });
    }


    /**
     * desc：列出所有bucket
     * author：wh
     * @return array
     */
    function getAllBucket(){
        return Mmodel::catch(function (){
            // 列举当前账号所有地域下的存储空间。
            $bucketListInfo = $this->ossClient->listBuckets();

            $bucketList = $bucketListInfo->getBucketList();
            $arr = [];
            foreach($bucketList as $bucket) {
                $arr['name'] = $bucket->getName();
                $arr['location'] = $bucket->getLocation();
                $arr['create_time'] = $bucket->getCreatedate();
            }
            return $arr;
        });
    }

    /**
     * desc：查询指定前缀bucket
     * author：wh
     * @param $prefix 指定前缀
     * @throws OssException
     * @throws \OSS\Http\RequestCore_Exception
     */
    function getBucketPrefix($prefix){

        return Mmodel::catch(function () use ($prefix){

            $options = array(OssClient::OSS_QUERY_STRING => array(OssClient::OSS_PREFIX => $prefix));
            // 列举当前账号所有地域下指定前缀的存储空间。
            $bucketListInfo = $this->ossClient->listBuckets($options);

            $bucketList = $bucketListInfo->getBucketList();
            $arr = [];
            foreach($bucketList as $bucket) {
                $arr['name'] = $bucket->getName();
                $arr['location'] = $bucket->getLocation();
                $arr['create_time'] = $bucket->getCreatedate();
            }
            return $arr;
        });
    }

    /**
     * desc：删除bucket
     * author：wh
     * @param $bucket_name
     * doc:https://help.aliyun.com/zh/oss/developer-reference/delete-buckets-3?spm=a2c4g.11186623.0.0.61556dd5SHNYgV
     * @return array
     */
    function deleteBucket($bucket_name){
        return Mmodel::catch(function () use ($bucket_name){
            // 填写Bucket名称，例如examplebucket。
            return $this->ossClient->deleteBucket($bucket_name);
        });
    }
}