## 华为OBS-对象存储服务
###支持常规功能，例如文件上传下载。
####温馨提示：你可能会遇到权限拒绝问题，请确认在华为云控制台已给用户授权
资料参考地址：https://support.huaweicloud.com/productdesc-obs/zh-cn_topic_0045829060.html
