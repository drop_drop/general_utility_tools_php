<?php
/*
 * description：
 * author：wh
 * email：
 * createTime：{2020/6/22} {15:26} 
 */

namespace wanghua\general_utility_tools_php\huawei\obs\service;


class Download extends BaseObs
{

    /**
     * desc：文本下载
     * 文本下载方式下返回结果中的Body是包含文本内容的GuzzleHttp\Psr7\StreamInterface对象。
     * author：wh
     * @param string $objectname
     * @return array
     */
    function text(string $objectname){
        $resp = $this->obsClient -> getObject([
            'Bucket' => $this->bucketName,
            'Key' => $objectname
        ]);
        $str = '';
        // 获取对象内容
        while(!$resp['Body'] -> eof()){
            $str .= $resp['Body'] -> read(65536);
        }
        return [
            'RequestId'=>$resp ['RequestId'],
            'text'=>$str
        ];
    }

    /**
     * desc：流式下载（向浏览器输出流直接下载文件）
     *
     * 使用SaveAsStream参数指定使用流式下载。
     * 返回结果中的Body是一个可读的GuzzleHttp\Psr7\StreamInterface对象，可将对象的内容读取到本地文件或者内存中。
     * author：wh
     * @param string $objectname
     * @param string $filename
     */
    function stream(string $objectname, string $filename){
        $resp = $this->obsClient -> getObject([
            'Bucket' => $this->bucketName,
            'Key' => $objectname,
            'SaveAsStream' => true
        ]);
        header("Content-Disposition:attachment; filename={$filename}");
        while(!$resp['Body'] -> eof()){
            echo $resp['Body'] -> read(65536);//开始下载
        }
    }

    /**
     * desc：文件下载（将文件从obs下载到当前应用所在服务器）
     * 说明：
     * 使用SaveAsFile参数指定文件下载的路径。
     * author：wh
     * @param string $objectname
     * @param $localfile "将文件从obs下载到当前应用所在服务器"
     * @return array
     */
    function file(string $objectname,string $localfile){
        $resp = $this->obsClient -> getObject([
            'Bucket' => $this->bucketName,
            'Key' => $objectname,
            'SaveAsFile' => $localfile,
        ]);

        return [
            'RequestId'=>$resp ['RequestId'],
        ];
    }
}