<?php
/*
 * description：
 * author：wh
 * email：
 * createTime：{2020/6/19} {17:25} 
 */

namespace wanghua\general_utility_tools_php\huawei\obs\service;


use Obs\ObsClient;

/**
 * 华为云obs桶管理
 * 包括创建桶、删除、修改属性、修改权限等
 * 资料参考地址：https://support.huaweicloud.com/sdk-php-devg-obs/obs_28_0301.html
 * Class BucketManage
 * @package libraries\huawei\obs\service
 */
class BucketManage extends BaseObs
{

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * desc：列举桶
     * 包含名称、创建时间、区域位置
     * author：wh
     * @return array
     */
    function getBuckList(){
        $resp = $this->obsClient->listBuckets([
            'QueryLocation' => true
        ]);
        $data = [];
        $data['RequestId'] = $resp['RequestId'];
        $data['OwnerID'] = $resp['Owner']['ID'];
        $data['OwnerName'] = $resp ['Owner']['DisplayName'];

        $tmp = [];
        foreach ($resp['Buckets'] as $index => $bucket){
            array_push($tmp, [
                'Name'=> $bucket['Name'],
                'CreationDate'=> $bucket['CreationDate'],
                'Location'=> $bucket['Location'],
            ]);
        }
        $data['Buckets'] = $tmp;
        return $data;
    }

    /**
     * desc：创建桶
     * 桶的名字是全局唯一的，所以您需要确保不与已有的桶名称重复。
     * 同一用户在同一区域多次创建同名桶不会报错，创建的桶属性以第一次请求为准。
     * 本示例创建的桶的访问权限默认是私有读写，存储类型默认是标准类型，区域位置是默认区域华北-北京一（cn-north-1）。
     * 须知：
     * 创建桶时，如果使用的终端节点归属于默认区域华北-北京一（cn-north-1），则可以不指定区域；如果使用的终端节点归属于其他区域，
     * 则必须指定区域，且指定的区域必须与终端节点归属的区域一致。当前有效的区域名称可从这里查询。
     * 您可以使用带参数创建方式，在创建桶时，指定桶的区域位置。
     * 资料参考地址：https://support.huaweicloud.com/sdk-php-devg-obs/obs_28_0301.html
     * author：wh
     * @return array
     */
    function createBucket(){
        // 创建桶
        $resp = $this->obsClient->createBucket([
            'Bucket' => $this->bucketName
        ]);

        return [
            'RequestId'=>$resp['RequestId'],
        ];
    }

    /**
     * desc：带参数创建桶
     * 说明：
     * 使用ACL参数指定桶的访问权限；使用StorageClass参数指定桶的存储类型；使用LocationConstraint参数指定桶的区域位置。
     * author：wh
     * @return array
     */
    function createBucketByParam(){
        // 创建桶
        $resp = $this->obsClient->createBucket([
            'Bucket' => $this->bucketName,
            // 设置桶访问权限为公共读写，默认是私有读写
            'ACL' => ObsClient::AclPublicReadWrite,
            // 设置桶的存储类型为归档存储类型
            'StorageClass' => ObsClient::StorageClassCold,
            // 设置桶区域位置
            'LocationConstraint' => 'bucketlocation'
        ]);

        return [
            'RequestId'=>$resp['RequestId'],
        ];
    }

    /**
     * desc：删除桶
     * author：wh
     * @return array
     */
    function deleteBucket(){
        // 删除桶
        $resp = $this->obsClient->deleteBucket([
            'Bucket' => $this->bucketName
        ]);

        return [
            'RequestId'=>$resp['RequestId'],
        ];
    }

    /**
     * desc：获取桶存量信息
     * 桶存量信息包括桶已使用的空间大小以及桶包含的对象个数。
     * 您可以通过ObsClient->getBucketStorageInfo获取桶的存量信息。
     * author：wh
     * @return array
     */
    function getBucketStorageInfo(){
        $resp = $this->obsClient->getBucketStorageInfo([
            'Bucket' => $this->bucketName
        ]);

        return [
            'RequestId'=>$resp['RequestId'],
            'Size'=>$resp['Size'],
            'ObjectNumber'=>$resp['ObjectNumber'],
        ];
    }

    /**
     * desc：获取桶配额
     * author：wh
     * @return array
     */
    function getBucketQuota(){
        $resp = $this->obsClient->getBucketQuota([
            'Bucket' => $this->bucketName
        ]);

        return [
            'RequestId'=>$resp['RequestId'],
            'StorageQuota'=>$resp['StorageQuota'],
        ];
    }

    /**
     * desc：设置桶配额
     * 使用StorageQuota参数指定桶的配额大小。
     * 桶配额值必须为非负整数，单位为字节，支持的最大值为2^63 - 1。
     * author：wh
     * @param float|int $StorageQuota
     * @return array
     */
    function setBucketQuota($StorageQuota = 1024 * 1024 * 100){
        $resp = $this->obsClient->setBucketQuota([
            'Bucket' => $this->bucketName,
            'StorageQuota' => $StorageQuota
        ]);

        return [
            'RequestId'=>$resp['RequestId'],
        ];
    }

    /**
     * desc：获取桶存储类型
     * author：wh
     * @return array
     */
    function getBucketStoragePolicy(){
        $resp = $this->obsClient->getBucketStoragePolicy([
            'Bucket' => $this->bucketName
        ]);

        return [
            'RequestId'=>$resp['RequestId'],
            'StorageClass'=>$resp['StorageClass'],
        ];
    }

    /**
     * desc：设置桶存储类型
     * OBS允许您对桶配置不同的存储类型，桶中对象的存储类型默认将与桶的存储类型保持一致。
     * 不同的存储类型可以满足客户业务对存储性能、成本的不同诉求。桶的存储类型分为三类
     *  标准存储
            标准存储拥有低访问时延和较高的吞吐量，适用于有大量热点对象（平均一个月多次）或小对象（<1MB），且需要频繁访问数据的业务场景。
            ObsClient::StorageClassStandard
        低频访问存储
            低频访问存储适用于不频繁访问（平均一年少于12次）但在需要时也要求能够快速访问数据的业务场景。
            ObsClient::StorageClassWarm
        归档存储
            归档存储适用于很少访问（平均一年访问一次）数据的业务场景。
            ObsClient::StorageClassCold
     * 资料参考地址：https://support.huaweicloud.com/sdk-php-devg-obs/obs_28_0311.html
     * author：wh
     * @param $StorageClass
     * @return array
     */
    function setBucketStoragePolicy($StorageClass){
        $resp = $this->obsClient->setBucketStoragePolicy([
            'Bucket' => $this->bucketName,
            'StorageClass' => $StorageClass
        ]);

        return [
            'RequestId'=>$resp['RequestId'],
        ];
    }

    /**
     * desc：获取桶区域位置
     * 说明：
     * 创建桶时可以指定桶的区域位置，请参见创建桶。
     * author：wh
     * @return array
     */
    function getBucketLocation(){
        $resp = $this->obsClient->getBucketLocation([
            'Bucket' => $this->bucketName
        ]);

        return [
            'RequestId'=>$resp['RequestId'],
            'Location'=>$resp['Location'],
        ];
    }

    /**
     * desc：获取桶策略
     * author：wh
     * @return array
     */
    function getBucketPolicy(){
        $resp = $this->obsClient->getBucketPolicy([
            'Bucket' => $this->bucketName
        ]);

        return [
            'RequestId'=>$resp['RequestId'],
            'Policy'=>$resp['Policy'],
        ];
    }

    /**
     * @deprecated 未完成
     * desc：设置桶策略[此方法待扩展]
     * 除了桶访问权限外，桶的拥有者还可以通过桶策略，提供对桶和桶内对象的集中访问控制。
     * 更多关于桶策略的内容请参考桶策略 https://support.huaweicloud.com/usermanual-obs/zh-cn_topic_0045829071.html
     * 说明：
     * 桶策略内容的具体格式（JSON格式字符串）请参考《对象存储服务API参考》。
     * author：wh
     * @return array
     */
    function setBucketPolicy(){
        $resp = $this->obsClient->setBucketPolicy([
            'Bucket' => $this->bucketName,
            'Policy' => 'your policy'
        ]);

        return [
            'RequestId'=>$resp['RequestId'],
        ];
    }

    /**
     * desc：删除桶策略
     * author：wh
     * @return array
     */
    function deleteBucketPolicy(){
        $resp = $this->obsClient->deleteBucketPolicy([
            'Bucket' => $this->bucketName
        ]);

        return [
            'RequestId'=>$resp['RequestId'],
        ];
    }
}