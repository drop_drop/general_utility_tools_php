<?php
/*
 * description：
 * author：wh
 * email：
 * createTime：{2020/6/19} {18:09} 
 */

namespace wanghua\general_utility_tools_php\huawei\obs\service;



use wanghua\general_utility_tools_php\huawei\obs\InitObs;

/**
 * obs基类负责初始化客户端
 * 注意：所有操作必须具有权限,否则会报权限错误
 * Class BaseObs
 * @package libraries\huawei\obs\service
 */
class BaseObs
{

    //桶名
    protected $bucketName = 'bs-obs1';
    //文件夹
    public $dir = '';

    //protected $origin = '';
    //客户端
    protected $obsClient = null;

    public function __construct($dir='bs')
    {
        $this->obsClient = InitObs::getObsClient();
        if($dir) {
            $this->dir = (false !== strpos($dir, '/'))?$dir:$dir.'/';
        }
    }
}