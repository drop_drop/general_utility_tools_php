<?php
/*
 * description：
 * author：wh
 * email：
 * createTime：{2020/6/22} {13:57} 
 */

namespace wanghua\general_utility_tools_php\huawei\obs\service;


/**
 * 多版本控制
 * Class VersionControl
 * @package libraries\huawei\obs\service
 */
class VersionControl extends BaseObs
{

    /**
     * desc：启用桶多版本状态
     *
     * 上传对象时，系统为每一个对象创建一个唯一版本号，上传同名的对象将不再覆盖旧的对象，而是创建新的不同版本号的同名对象。
     * 可以指定版本号下载对象，不指定版本号默认下载最新对象。
     * 删除对象时可以指定版本号删除，不带版本号删除对象仅产生一个带唯一版本号的删除标记，并不删除对象。
     * 列出桶内对象列表（ObsClient->listObjects）时默认列出最新对象列表，可以指定列出桶内所有版本对象列表（ObsClient->listVersions）。
     * 除了删除标记外，每个版本的对象存储均需计费。
     * author：wh
     * @return array
     */
    function setBucketVersionEnabled(){
        // 启用桶多版本状态
        $resp = $this->obsClient->setBucketVersioning([
            'Bucket' => $this->bucketName,
            'Status' => 'Enabled'
        ]);

        return [
            'RequestId'=>$resp['RequestId'],
        ];
    }
    /**
     * desc：停用桶多版本状态
     *
     * 旧的版本数据继续保留。
     * 上传对象时创建对象的版本号为null，上传同名的对象将覆盖原有同名的版本号为null的对象。
     * 可以指定版本号下载对象，不指定版本号默认下载最新对象。
     * 删除对象时可以指定版本号删除，不带版本号删除对象将产生一个版本号为null的删除标记，并删除版本号为null的对象。
     * 除了删除标记外，每个版本的对象存储均需计费。
     * author：wh
     * @return array
     */
    function setBucketVersionSuspended(){
        // 停用桶多版本状态
        $resp = $this->obsClient->setBucketVersioning([
            'Bucket' => $this->bucketName,
            'Status' => 'Suspended'
        ]);

        return [
            'RequestId'=>$resp['RequestId'],
        ];
    }

    /**
     * desc：查看桶多版本状态
     * author：wh
     * @return array
     */
    function getBucketVersion(){
        $resp = $this->obsClient->getBucketVersioning([
            'Bucket' => $this->bucketName,
        ]);

        return [
            'RequestId'=>$resp['RequestId'],
            'Status'=>$resp['Status'],
        ];
    }
}