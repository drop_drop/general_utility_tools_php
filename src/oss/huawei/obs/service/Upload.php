<?php
/*
 * description：
 * author：wh
 * email：
 * createTime：{2020/6/19} {18:09} 
 */

namespace wanghua\general_utility_tools_php\huawei\obs\service;

/**
 * 在OBS中，用户操作的基本数据单元是对象。OBS PHP SDK提供了丰富的对象上传接口，可以通过以下方式上传对象：
 * 文本上传
 * 流式上传
 * 文件上传
 * 分段上传
 * 基于表单上传
 *
 * SDK支持上传0KB~5GB的对象。流式上传和文件上传的内容大小不能超过5GB；当上传较大文件时，请使用分段上传，分段上传每段内容大小不能超过5GB；
 * 基于表单上传提供了基于浏览器表单上传对象的方式。
 * 若上传的对象权限设置为匿名用户读取权限，对象上传成功后，匿名用户可通过链接地址访问该对象数据。
 * 对象链接地址格式为：https://桶名.域名/文件夹目录层级/对象名。如果该对象存在于桶的根目录下，则链接地址将不需要有文件夹目录层级。
 *
 * 资料参考地址：https://support.huaweicloud.com/sdk-php-devg-obs/obs_28_0401.html
 * Class Upload
 * @package libraries\huawei\obs\service
 */
class Upload extends BaseObs
{
    public function __construct($dir='bs')
    {
        parent::__construct($dir);
    }

    /**
     * desc：文本上传
     * 文本上传用于直接上传字符串。您可以通过ObsClient->putObject直接上传字符串到OBS.
     * 说明：
     * 使用Body参数指定待上传的字符串。
     * 资料参考：https://support.huaweicloud.com/sdk-php-devg-obs/obs_28_0403.html
     * author：wh
     * @param string $objectname 桶内对象名称
     * @param string $text 要上传的文本
     * @return array
     */
    function putObjectText(string $objectname, string $text){
        $resp = $this->obsClient->putObject([
            'Bucket' => $this->bucketName,
            'Key' => $this->dir.$objectname,
            'Body' => $text //'Hello OBS'
        ]);

        return [
            'RequestId'=>$resp['RequestId']
        ];
    }

    /**
     * desc：上传网络流
     * 流式上传使用resource或GuzzleHttp\Psr7\StreamInterface作为对象的数据源
     * 资料参考：https://support.huaweicloud.com/sdk-php-devg-obs/obs_28_0403.html
     * author：wh
     * @param string $objectname 桶内对象名称
     * @param string $url
     * @return array
     */
    function putObjectNetStream(string $objectname, string $url){
        $resp = $this->obsClient->putObject([
            'Bucket' => $this->bucketName,
            'Key' => $this->dir.$objectname,
            // 创建网络流  eg: $url='http://www.a.com'
            'Body' => fopen($url,'r')
        ]);

        return [
            'RequestId'=>$resp['RequestId']
        ];
    }

    /**
     * desc：上传文件流
     * 使用Body参数指定待上传的流数据时，其值必须是一个resource对象或GuzzleHttp\Psr7\StreamInterface对象。
     * 大文件上传建议使用分段上传。
     * 资料参考：https://support.huaweicloud.com/sdk-php-devg-obs/obs_28_0403.html
     * 分段上传资料参考：https://support.huaweicloud.com/sdk-php-devg-obs/obs_28_0407.html
     * author：wh
     * @param string $objectname 桶内对象名称
     * @param string $localfile 本地文件，必须是一个resource对象或GuzzleHttp\Psr7\StreamInterface对象
     * @return array
     */
    function putObjectFileStream(string $objectname, string $localfile){
        $resp = $this->obsClient->putObject([
            'Bucket' => $this->bucketName,
            'Key' => $this->dir.$objectname,
            'Body' => fopen($localfile, 'r')
        ]);

        return [
            'RequestId'=>$resp['RequestId']
        ];
    }

    /**
     * desc：文件上传
     * 使用SourceFile参数指定待上传的文件路径。
     * SourceFile参数和Body参数不能同时使用。
     * 上传内容大小不能超过5GB。
     * 资料参考：https://support.huaweicloud.com/sdk-php-devg-obs/obs_28_0404.html
     * author：wh
     * @param string $objectname 文件名称
     * @param string $localfile
     * @return array
     */
    function putObjectLocalFile(string $objectname, string $localfile){
        $resp = $this->obsClient->putObject([
            'Bucket' => $this->bucketName,
            'Key' => $this->dir.$objectname,
            'SourceFile' => $localfile  // localfile为待上传的本地文件路径，需要指定到具体的文件名
        ]);

        return [
            'RequestId'=>$resp['RequestId']
        ];
    }
}