<?php
/*
 * description：
 * author：wh
 * email：
 * createTime：{2020/6/17} {18:22} 
 */

namespace wanghua\general_utility_tools_php\huawei\obs;


use Obs\ObsClient;

/**
 * 初始化OBS
 *
 * Composer依赖：obs/esdk-obs-php
 *
 * Class InitObs
 * @package wanghua\general_utility_tools_php\huawei\obs
 */
class InitObs
{
    protected static $obsClient = null;

    /**
     * desc：
     * author：wh
     * @return ObsClient|null
     */
    static function getObsClient(int $socket_timeout=30, int $connect_timeout=10){
        if(null === self::$obsClient){
            self::$obsClient = ObsClient::factory ( [
                'key' => Config::$key,
                'secret' => Config::$secret,
                'endpoint' => Config::$endpoint,
                'socket_timeout' => $socket_timeout,
                'connect_timeout' => $connect_timeout
            ] );
        }
        return self::$obsClient;
    }

    /**
     * desc：关闭obsClient
     * author：wh
     */
    static function close(){
        // 关闭obsClient
        return self::$obsClient->close();
    }
}