<?php
/*
 * description：
 * author：wh
 * email：
 * createTime：{2020/5/22} {16:43} 
 */

namespace wanghua\db\general_utility_tools_php;


use think\cache\driver\Redis as RedisDriver;
use think\Db;
use think\facade\Cache;

/**
 * Redis操作工具
 *
 * 环境要求：
 *      ThinkPHP5.1+,PHP7.0+,MySQL8.0+
 *
 *
 * Class RedisUtility
 * @package libraries
 */
class RedisUtility
{
    protected static $redisHandle = null;

    /**
     * desc：初始化Redis对象
     * author：wh
     * @return object|null
     */
    static function redisObject(){
        if(empty(self::$redisHandle)){
            //初始化驱动拿到句柄
            self::$redisHandle = (new RedisDriver(config('cache.redis')))->handler();
        }
        return self::$redisHandle;
    }
    /**
     * desc：设置 0 永久有效
     * author：wh
     * @param $key_name 键名
     * @param $value 值 json数据
     * @return bool
     */
    static function set($key_name, $value){
        return Cache::store('redis')->set($key_name, json_encode($value, JSON_UNESCAPED_UNICODE));
    }

    /**
     * desc：重置
     * author：wh
     * @param $key_name
     * @return bool
     */
    static function reset($key_name){
        return Cache::store('redis')->set($key_name, null);
    }

    /**
     * desc：设置过期时间
     * author：wh
     * @param $key_name 键名
     * @param $value 值 json数据
     * @param int $expire 过期时间 0 永久有效
     * @return bool
     */
    static function setExpire($key_name, $value, $expire=0){
        return Cache::store('redis')->set($key_name, json_encode($value), $expire);
    }

    /**
     * desc：存储hash类型
     * author：wh
     * @param $key
     * @param $field
     * @param $value
     */
    static function hSet($key, $field, $value){
        return self::redisObject()->hSet($key, $field, json_encode($value, JSON_UNESCAPED_UNICODE));
    }

    /**
     * desc：删除指定的键。
     * author：wh
     * @param $key
     * @return mixed
     */
    static function delete($key){
        return self::redisObject()->delete($key);
    }

    /**
     * desc：从存储在键上的哈希中移除一个值。
     * 如果哈希表不存在，或者键不存在，则返回 FALSE 。
     * author：wh
     * @param $key
     * @param $hashKey1
     * @return mixed
     */
    static function hDel($key, $hashKey1){
        return self::redisObject()->hDel($key, $hashKey1);
    }


}