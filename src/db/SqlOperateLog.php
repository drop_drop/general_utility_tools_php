<?php
/*
 * description：
 * author：wh
 * email：
 * createTime：{2019/10/31} {9:25} 
 */

namespace wanghua\db\general_utility_tools_php;


use think\Db;
use think\facade\Log;

/**
 * 环境要求：
 *      ThinkPHP5.1+,PHP7.0+,MySQL8.0+
 *
 * 日志操作类，配合DbCacheUtility.php使用。
 * 使用前先在系统config.php文件中添加配置:
 * 1、is_sql_slow_log:是否记录sql慢日志，true or false
 * 2、新建表（log_sql_operate）字段：action,is_cache,duration_time,type,is_cache,sql,md5_name
 * 3、如果什么错都没有报，但是程序就是没反应，则去runtime目录中找错误。
 *
 * Class SqlOperateLog
 * @package wanghua\general_utility_tools_php
 */
class SqlOperateLog
{

    static function add($sql,$end_time,$begin_time,$md5_name,$type,$is_cache){
        $data = [
            'action'=>request()->baseUrl(),
            'duration_time'=>$end_time - $begin_time,
            'type'=>$type,
            'is_cache'=>$is_cache?1:0,
            'sql'=>$sql,
            'md5_name'=>$md5_name,
        ];
        try{
            if(config('app.is_sql_slow_log') && ($end_time - $begin_time) >= config('app.is_sql_slow_log')){
                Db::table('log_sql_operate')->insert($data);
            }
        }catch (\Exception $e){
            //DATABASE BOOM
            //tp6
            //Log::error('========[数据库异常:DATABASE BOOM]========'.$e->getTraceAsString());
            //Log::close();
            //tp5.1
            Log::write('========[DATABASE ERROR:DATABASE BOOM]========【'.$e->getMessage().'】'.$e->getTraceAsString());
        }
    }


}