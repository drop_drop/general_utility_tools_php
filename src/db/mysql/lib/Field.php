<?php
/*
 * description：
 * author：wh
 * email：
 * createTime：{2022/02/24} {10:57} 
 */

namespace wanghua\general_utility_tools_php\db\mysql\lib;


use think\Db;

class Field
{


    /**
     * desc：创建字段，存在则修改字段
     * author：wh
     * @param array $table
     */
    function createfield(array $table){
        $table_name = $table['tablename'];
        //新增字段
        $dec_num = '';

        if(in_array(strtoupper($table['type']), ['TINYINT','SMALLINT','MEDIUMINT','INT','BIGINT','FLOAT','DOUBLE','DECIMAL'])){
            $table['default'] = '0';//整型默认值 浮点类型自动转为0.00
            if(in_array(strtoupper($table['type']), ['FLOAT','DOUBLE','DECIMAL'])){
                $dec_num = ','.$table['decimals_size'];//浮点类型小数位位数
            }
        }elseif ($table['type'] == 'enum'){
            //枚举值不转换
        }else{
            $table['default'] = '""';
        }

        if(in_array(strtolower($table['type']), ['timestamp','datetime','date','time','text','mediumtext','longtext'])){
            $table['size'] = 0;//重写 默认0
            $table['default'] = 'null';//重写
        }
        //是否可以负数
        if(in_array(strtolower($table['type']), ['int','tinyint','bigint','smallint', 'double', 'float', 'decimal']) && !empty($post['is_unsigned'])){//不是
            $is_unsigned = 'unsigned';
        }else{//是 表单可选值
            $is_unsigned = '';
        }

        if (in_array($table['fields_name'], ['create_time', 'update_time'])){
            if($table['fields_name'] == 'create_time'){
                $table['default'] = 'CURRENT_TIMESTAMP';
            }
            if($table['fields_name'] == 'update_time'){
                $table['default'] = 'NULL ON UPDATE CURRENT_TIMESTAMP';
            }
        }
        //验证字段是否存在
        $f = DB::table($table_name)->getTableFields();
        if(in_array($table['fields_name'], $f)){
            //枚举类型
            if ($table['type'] == 'enum'){
                $enum_str = '';
                $enum_default = '';
                //组合枚举值
                if($table['default']){
                    $enum_str.='ENUM('.$table['default'].')';
                    $enum_default = explode(',', $table['default'])[0];
                }
                $sql2 = "ALTER TABLE {$table_name} MODIFY {$table['fields_name']} {$enum_str} NOT NULL DEFAULT {$enum_default} COMMENT '{$table['title']}';";
            }else{
                $sql2 = "ALTER TABLE {$table_name} MODIFY COLUMN {$table['fields_name']} {$table['type']}(".$table['size'].$dec_num.") {$is_unsigned} DEFAULT {$table['default']} COMMENT '{$table['title']}';";
            }
        }else{

            if(in_array($table['type'], ['text','longtext'])){
                $sql2 = "ALTER TABLE {$table_name} ADD COLUMN {$table['fields_name']} {$table['type']} COMMENT '{$table['title']}' AFTER id;";
            }elseif ($table['type'] == 'enum'){//枚举类型
                $enum_str = '';
                $enum_default = '';
                //组合枚举值
                if($table['default']){
                    $enum_str.='ENUM('.$table['default'].')';
                    $enum_default = explode(',', $table['default'])[0];
                }
                $sql2 = "ALTER TABLE {$table_name} ADD {$table['fields_name']} {$enum_str} NOT NULL DEFAULT {$enum_default} COMMENT '{$table['title']}' AFTER id;";
            }else{
                $sql2 = "ALTER TABLE {$table_name} ADD COLUMN {$table['fields_name']} {$table['type']}(".$table['size'].$dec_num.") {$is_unsigned} DEFAULT {$table['default']} COMMENT '{$table['title']}' AFTER id;";
            }
        }

        DB::execute($sql2);
    }

    /**
     * desc：删除字段
     * author：wh
     * @param $tablename
     * @param $fieldname
     */
    function dropFieldName($tablename, $fieldname){
        $sql = "ALTER TABLE {$tablename} DROP COLUMN {$fieldname};";
        DB::execute($sql);
    }

    /**
     * desc：改注释
     * author：wh
     * @param $tablename
     * @param $comment
     * @return bool
     */
    function updateComment($tablename,$field, $comment){

        $sql = "ALTER TABLE {$tablename} MODIFY COLUMN {$field} INT COMMENT '{$comment}';";
        Db::execute($sql);
    }

    /**
     * desc：获取数据表中所有字段的数据类型（含其它属性）
     *
     * 查询结果字段名说明
     *
     * F_FIELD：字段名称
     * F_DATATYPE：数据类型
     * F_DATALENGTH：数据长度（int类型默认为null,业务处理时默认为10即可）
     * F_PRECISION：精度
     * F_DECIMAL_DIGITS：小数位数
     * F_ALLOWNULL：是否允许为null值(1是，0否)
     * F_FIELDNAME：字段名称
     * F_PRIMARYKEY：是否主键(1是，0否)
     * F_DEFAULTS：字段默认值
     *
     * author：wh
     * @param string $dbname
     * @param string $tablename
     * @param string $fieldname
     * @return mixed
     */
    function getFieldsDataType(string $dbname,string $tablename){
        $sql = "SELECT
    COLUMN_NAME F_FIELD,
    data_type F_DATATYPE,
    CHARACTER_MAXIMUM_LENGTH F_DATALENGTH,
    NUMERIC_PRECISION F_PRECISION,
    NUMERIC_SCALE F_DECIMAL_DIGITS,
IF
    ( IS_NULLABLE = 'YES', '1', '0' ) F_ALLOWNULL,
    COLUMN_COMMENT F_FIELDNAME,
IF
    ( COLUMN_KEY = 'PRI', '1', '0' ) F_PRIMARYKEY,
    column_default F_DEFAULTS,
    CONCAT( upper( COLUMN_NAME ), '(', COLUMN_COMMENT, ')' ) AS 'F_DESCRIPTION' 
FROM
    INFORMATION_SCHEMA.COLUMNS 
WHERE
    TABLE_NAME = '$tablename' 
    AND TABLE_SCHEMA = '$dbname'";
        return Db::query($sql);
    }


    /**
     * desc：获取数据表中某个字段的数据类型（含这个字段的其它属性）
     *
     * 查询结果字段名说明
     *
     * F_FIELD：字段名称
     * F_DATATYPE：数据类型
     * F_DATALENGTH：数据长度（int类型默认为null,业务处理时默认为10即可）
     * F_PRECISION：精度
     * F_DECIMAL_DIGITS：小数位数
     * F_ALLOWNULL：是否允许为null值(1是，0否)
     * F_FIELDNAME：字段名称
     * F_PRIMARYKEY：是否主键(1是，0否)
     * F_DEFAULTS：字段默认值
     *
     * author：wh
     * @param string $dbname
     * @param string $tablename
     * @param string $fieldname
     * @return mixed
     */
    function getFieldDataType(string $dbname,string $tablename,string $fieldname){
        $sql = "SELECT
    COLUMN_NAME F_FIELD,
    data_type F_DATATYPE,
    CHARACTER_MAXIMUM_LENGTH F_DATALENGTH,
    NUMERIC_PRECISION F_PRECISION,
    NUMERIC_SCALE F_DECIMAL_DIGITS,
IF
    ( IS_NULLABLE = 'YES', '1', '0' ) F_ALLOWNULL,
    COLUMN_COMMENT F_FIELDNAME,
IF
    ( COLUMN_KEY = 'PRI', '1', '0' ) F_PRIMARYKEY,
    column_default F_DEFAULTS,
    CONCAT( upper( COLUMN_NAME ), '(', COLUMN_COMMENT, ')' ) AS 'F_DESCRIPTION' 
FROM
    INFORMATION_SCHEMA.COLUMNS 
WHERE
    TABLE_NAME = '$tablename' 
    AND TABLE_SCHEMA = '$dbname'
    AND COLUMN_NAME='$fieldname'
    ";
        $ar = Db::query($sql);
        return $ar[0];
    }


    /**
     * desc：获取数据表中某个字段的数据类型
     *
     * 不含这个字段的其它属性
     *
     *
     * 查询结果字段名说明
     *
     * F_FIELD：字段名称
     * F_DATATYPE：数据类型
     * F_DATALENGTH：数据长度（int类型默认为null,业务处理时默认为10即可）
     * F_PRECISION：精度
     * F_DECIMAL_DIGITS：小数位数
     * F_ALLOWNULL：是否允许为null值(1是，0否)
     * F_FIELDNAME：字段名称
     * F_PRIMARYKEY：是否主键(1是，0否)
     * F_DEFAULTS：字段默认值
     *
     * author：wh
     * @param string $dbname
     * @param string $tablename
     * @param string $fieldname
     * @return mixed
     */
    function getFieldDataTypeVal(string $dbname,string $tablename,string $fieldname){
        $sql = "SELECT
    COLUMN_NAME F_FIELD,
    data_type F_DATATYPE,
    CHARACTER_MAXIMUM_LENGTH F_DATALENGTH,
    NUMERIC_PRECISION F_PRECISION,
    NUMERIC_SCALE F_DECIMAL_DIGITS,
IF
    ( IS_NULLABLE = 'YES', '1', '0' ) F_ALLOWNULL,
    COLUMN_COMMENT F_FIELDNAME,
IF
    ( COLUMN_KEY = 'PRI', '1', '0' ) F_PRIMARYKEY,
    column_default F_DEFAULTS,
    CONCAT( upper( COLUMN_NAME ), '(', COLUMN_COMMENT, ')' ) AS 'F_DESCRIPTION' 
FROM
    INFORMATION_SCHEMA.COLUMNS 
WHERE
    TABLE_NAME = '$tablename' 
    AND TABLE_SCHEMA = '$dbname'
    AND COLUMN_NAME='$fieldname'
    ";
        $ar = Db::query($sql);
        return $ar[0]['F_DATATYPE'];
    }

    /**
     * desc：获取数据表中某个字段的属性
     *
     * 查询结果字段名说明
     *
     * F_FIELD：字段名称
     * F_DATATYPE：数据类型
     * F_DATALENGTH：数据长度（int类型默认为null,业务处理时默认为10即可）
     * F_PRECISION：精度
     * F_DECIMAL_DIGITS：小数位数
     * F_ALLOWNULL：是否允许为null值(1是，0否)
     * F_FIELDNAME：字段名称
     * F_PRIMARYKEY：是否主键(1是，0否)
     * F_DEFAULTS：字段默认值
     *
     * author：wh
     * @param string $dbname
     * @param string $tablename
     * @param string $fieldname
     * @return mixed
     */
    function getFieldAttrVal(string $dbname,string $tablename,string $fieldname,string $attr){
        $sql = "SELECT
    COLUMN_NAME F_FIELD,
    data_type F_DATATYPE,
    CHARACTER_MAXIMUM_LENGTH F_DATALENGTH,
    NUMERIC_PRECISION F_PRECISION,
    NUMERIC_SCALE F_DECIMAL_DIGITS,
IF
    ( IS_NULLABLE = 'YES', '1', '0' ) F_ALLOWNULL,
    COLUMN_COMMENT F_FIELDNAME,
IF
    ( COLUMN_KEY = 'PRI', '1', '0' ) F_PRIMARYKEY,
    column_default F_DEFAULTS,
    CONCAT( upper( COLUMN_NAME ), '(', COLUMN_COMMENT, ')' ) AS 'F_DESCRIPTION' 
FROM
    INFORMATION_SCHEMA.COLUMNS 
WHERE
    TABLE_NAME = '$tablename' 
    AND TABLE_SCHEMA = '$dbname'
    AND COLUMN_NAME='$fieldname'
    ";
        $ar = Db::query($sql);
        return $ar[0][$attr];
    }
}