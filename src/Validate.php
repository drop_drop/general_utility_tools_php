<?php
/*
 * description：
 * author：wh
 * email：
 * createTime：{2019/11/1} {15:09} 
 */

namespace wanghua\general_utility_tools_php;


class Validate
{

    /**
     * desc：验证手机号是否正确
     * author：wh
     * @param $m
     * @return bool
     */
    static function is_mobile($m) {
        return preg_match('/^1[3,4,5,6,7,8,9]{1}\d{9}$/', $m) ? true : false;
    }

    /**
     * 验证邮箱
     * author：wh
     * @param $email
     * @return bool
     */
    static function is_email($email){
        return preg_match("/^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,})$/",$email)?true:false;
    }

    /**
     * [is_IDCard 验证身份证号码]
     * @Author
     * @Date   2019-11-26
     * @param  [string]     $string [身份证号]
     * @return boolean         [description]
     */
    static function is_IDCard($string){
        return preg_match("/^[1-9]\d{5}(18|19|([23]\d))\d{2}((0[1-9])|(10|11|12))(([0-2][1-9])|10|20|30|31)\d{3}[0-9Xx]$/", $string)?true:false;
    }

    /**
     * [is_money_times 验证金额是否是100的整数倍]
     * @Author
     * @Date   2019-12-12
     * @param  [type]     $money [金额]
     * @return boolean           [description]
     */
    static function is_money_times($money){
        if(!is_numeric($money) || $money <= 0){
            return false;
        }
        return $money%100===0;
    }

    /**
     * desc：是否是金额
     * author：wh
     * @param $money
     * @return bool
     */
    static function is_money($money){
        return (preg_match('/^[0-9]+.[0-9]+$/', $money) || preg_match('/^\d+$/', $money))?true:false;
    }
    /**
     * desc：是否是金额，并且最多只有两位小数
     *
     * 严格金额格式校验
     *
     * author：wh
     * @param $money
     * @return bool
     */
    static function is_money_double($money){

        $r = (preg_match('/^[0-9]+.[0-9]+$/', $money) || preg_match('/^\d+$/', $money))?true:false;

        $exp = explode('.',$money);

        if(strpos($money,'.') && preg_match('/^[0-9]+.[0-9]+$/', $money) && strlen($exp[1])>2){
            //返回false，表示非严格的2位数金额
            return false;
        }


        return $r;
    }

    /**
     * desc：是否是小数格式
     * author：wh
     * @param string $num
     * @return bool
     */
    static function is_float_number(string $num){
        return preg_match('/^[0-9]+.[0-9]+$/', $num)?true:false;
    }

    /**
     * desc：是否是数字
     * author：wh
     * @param string $num
     * @return bool
     */
    static function is_number(string $num){
        return preg_match('/^\d+$/', $num)?true:false;
    }

    /**
     * desc：是否是字母
     * author： wh
     * @param string $str
     * @return bool
     */
    static function is_letter(string $str){
        return preg_match('/^[a-zA-Z]+$/', $str) ? true : false;
    }

    /**
     * desc：是否字母或数字
     * author：wh
     * @param string $str
     * @return bool
     */
    static function is_letter_or_number(string $str){
        return preg_match('/^[a-zA-Z|0-9]+$/', $str) ? true : false;
    }

    /**
     * desc：验证数组是否存在空值
     * 仅针对基本数据类型
     * 仅针对一维数组
     * author：wh
     * @param $array
     * @return bool
     */
    static function check_array_val_empty($array){
        $is_empty = false;
        foreach ($array as $value){
            if(($value!==0 && $value !=='0') && empty($value)){
                $is_empty = true;
                break;
            }
            if(is_int($value) && empty(1*$value)){
                $is_empty = true;
                break;
            }
        }
        return $is_empty;
    }

    /**
     * desc：是否是url
     * author：wh
     * @param $v
     * @return bool
     */
    static function is_url($v){
        $pattern="#(http|https)://(.*\.)?.*\..*#i";
        return preg_match($pattern,$v)?true:false;
    }


    /**
     * [是否全部大写]
     *
     * @param $str
     * @return bool
     * @example
     * @see
     * @link
     */
    static function is_upper($str){
        return preg_match('/^[A-Z]+$/', $str)?true:false;
    }

    /**
     * [是否全部小写]
     *
     * @param $str
     * @return bool
     * @example
     * @see
     * @link
     */
    static function is_lower($str){
        return preg_match('/^[a-z]+$/', $str)?true:false;
    }


    /**
     * desc：验证是微信内还是微信外
     *
     * 是否在微信内，是否在微信中
     *
     * author：wh
     * @return bool
     */
    static function is_weixin(){
        return strpos($_SERVER['HTTP_USER_AGENT'], 'MicroMessenger') !== false;
    }

    /**
     * desc：是否包含特殊字符
     * author：wh
     * @param string $str
     * @return bool
     */
    static function is_special_character(string $str){
        $res = preg_match ( '/[\Q~!@#$%^&*()+-_=.:?<>\E]/', $str );
        return $res ? true : false;
    }

    /**
     * desc：验证字符串是否全部是中文
     *
     * 返回 true表示全部是中文，false表示部分是中文或没有中文
     *
     * author：wh
     * @param string $str
     * @return bool
     */
    static function is_all_chinese(string $str){
        return preg_match("/^[\x{4e00}-\x{9fa5}]+$/u",$str)?true:false;
    }

    /**
     * desc：验证两个浮点数值是否相等
     *
     * 例如：
     *  $num1 = 0.1;
        $num2 = 0.7;

        $res = $num1 + $num2;

        var_dump($res);

        var_dump($res == 0.8);//false 不相等

        var_dump(intval(strval($res)) == intval(strval(0.8)));//true 相等
     *
     * author：wh
     * @param $float_num1
     * @param $float_num2
     * @return bool
     */
    static function is_equal_num_val($float_num1, $float_num2){

        return intval(strval($float_num1)) == intval(strval($float_num2));
    }

    /**
     * desc：是否包含给定的主域名
     * author：wh
     * @param $domain
     * @param $main_domain
     * @return bool
     */
    static function is_main_domain($domain,$main_domain){
        $exp_arr = explode('.',$domain);
        $str = $exp_arr[count($exp_arr)-2] .'.'. $exp_arr[count($exp_arr)-1];
        return $str==$main_domain;
    }

    /**
     * desc：国内国外IP校验，校验IP来源
     *
     * author：wh
     * @param $ip
     * @return bool
     */
    static function validate_ip($ip) {
        $chinaStart = ip2long('1.0.1.0'); // 中国大陆起始IP
        $chinaEnd = ip2long('254.254.254.254'); // 中国大陆结束IP

        $hongKongStart = ip2long('8.36.0.0'); // 香港起始IP
        $hongKongEnd = ip2long('8.37.255.255'); // 香港结束IP

        $taiwanStart = ip2long('192.168.0.0'); // 台湾起始IP
        $taiwanEnd = ip2long('192.168.255.255'); // 台湾结束IP

        $ipLong = ip2long($ip);

        if ($ipLong >= $chinaStart && $ipLong <= $chinaEnd ||
            $ipLong >= $hongKongStart && $ipLong <= $hongKongEnd ||
            $ipLong >= $taiwanStart && $ipLong <= $taiwanEnd) {
            return true; // IP属于国内
        } else {
            return false; // IP不属于国内
        }
    }


    /**
     * 是否移动端访问访问
     *
     * @return bool
     */
    static function is_mobile_client()
    {
        // 如果有HTTP_X_WAP_PROFILE则一定是移动设备
        if (isset($_SERVER['HTTP_X_WAP_PROFILE'])) {
            return true;
        }

//如果via信息含有wap则一定是移动设备,部分服务商会屏蔽该信息
        if (isset($_SERVER['HTTP_VIA'])) {
            //找不到为flase,否则为true
            return stristr($_SERVER['HTTP_VIA'], "wap") ? true : false;
        }

//判断手机发送的客户端标志
        if (isset($_SERVER['HTTP_USER_AGENT'])) {
            $clientkeywords = [
                'nokia', 'sony', 'ericsson', 'mot', 'samsung', 'htc', 'sgh', 'lg', 'sharp',
                'sie-', 'philips', 'panasonic', 'alcatel', 'lenovo', 'iphone', 'ipod', 'blackberry', 'meizu',
                'android', 'netfront', 'symbian', 'ucweb', 'windowsce', 'palm', 'operamini', 'operamobi',
                'openwave', 'nexusone', 'cldc', 'midp', 'wap', 'mobile','alipay'
            ];

// 从HTTP_USER_AGENT中查找手机浏览器的关键字
            if (preg_match("/(".implode('|', $clientkeywords).")/i", strtolower($_SERVER['HTTP_USER_AGENT']))) {
                return true;
            }
        }

//协议法，因为有可能不准确，放到最后判断
        if (isset($_SERVER['HTTP_ACCEPT'])) {
            // 如果只支持wml并且不支持html那一定是移动设备
            // 如果支持wml和html但是wml在html之前则是移动设备
            if ((strpos($_SERVER['HTTP_ACCEPT'], 'vnd.wap.wml') !== false) && (strpos($_SERVER['HTTP_ACCEPT'], 'text/html') === false || (strpos($_SERVER['HTTP_ACCEPT'], 'vnd.wap.wml') < strpos($_SERVER['HTTP_ACCEPT'], 'text/html')))) {
                return true;
            }
        }

        return false;
    }

    /**
     * 判断是否微信内置浏览器访问
     * @return bool
     */
    static function is_wx_client()
    {
        return strpos($_SERVER['HTTP_USER_AGENT'], 'MicroMessenger') !== false;
    }

    /**
     * 判断是否支付宝内置浏览器访问
     * @return bool
     */
    static function is_ali_client()
    {
        return strpos($_SERVER['HTTP_USER_AGENT'], 'Alipay') !== false;
    }
}