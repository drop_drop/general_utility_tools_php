# PHP ftp类库
#### 实现用ftp方式上传、复制、移动、重命名、删除、下载文件

##### 案例代码：
```
    $ftp = new Ftp();//初始化
    //$ftp->up_file('D:\whua\test.zip','test.zip'); // 上传文件
    //$ftp->rename_file('test.zip','test-rename.zip'); // 移动文件
    //$ftp->move_file('/test2.zip','/testdir2/test2.zip', '/www/wwwroot/testftp/'); // 移动文件2
    //$ftp->copy_file('test.zip','test-copy.zip'); // 复制文件
    //$ftp->del_file('/www/aaa.txt'); // 删除具体文件
    $ftp->del_dir('/www'); // 删除目录（错误示范），必须精确到具体文件夹删除
    $ftp->del_dir('/ftp/test/copy-test/a-dir'); // 删除目录（正确示范），必须精确到具体文件夹删除，批量删除请多次调用
    
    //下载文件
    //$ftp->download('D:\whua\projects\zc_game_admin3.0\data\local.zip', 'test.zip');
    //$ftp->close(); // 关闭FTP连接
    
    //dump($ftp->ftp_pwd());die;
    //dump($ftp->ftp_size('test2.zip'));die;
```