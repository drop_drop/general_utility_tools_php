<?php
/*
 * description：
 * author：wh
 * email：
 * createTime：{2024/3/26} {16:06} 
 */

namespace wanghua\general_utility_tools_php\gpt\chat;


use wanghua\general_utility_tools_php\tool\Tools;

class BaseChat
{
    public $chatId = '';
    public function __construct()
    {
        $this->chatId = Tools::getMillisecond();
    }

    /**
     * desc：将字符串里含有json_encode后的一维数组提取出来
     * @param $str
     * @return string
     */
    function dealstr($str){
        $rep_str = str_replace("\n",'',$str);
        $rep_str = str_replace("\r",'',$rep_str);
        $rep_str = str_replace("    ",'',$rep_str);

        $start_sit = strpos($rep_str,"[{");
        $end_sit = strpos($rep_str,"}]");

        $last_json_str = substr($rep_str,$start_sit,$end_sit-$start_sit);

        $last_json_str.="}]";

        return $last_json_str;
    }

}