<?php
/*
 * description：
 * author：wh
 * email：
 * createTime：{2024/2/26} {10:54} 
 */

namespace wanghua\general_utility_tools_php\api;


use app\common\model\TabConf;
use think\Db;
use wanghua\general_utility_tools_php\tool\Tools;

class BaseUserLogic
{
    public $user_table = 'fa_users';

    /**
     * desc：【通用】用户登录逻辑
     *
     * 请求端加密，服务端校验
     * 
     * 签名规则
     *  请求参数：
    username    //用户名
    password    //密码 md5(md5('a123456'))   d477887b0636e5d87f79cc25c99d7dc9
    timestamp  //请求时间戳 2020-01-01 12:34:07
    noncestr //随机字符串8位
    sign    //签名串，参考全站签名规则

    other_params //其它接口的其它参数
     *  全站sign规则：
    1. 对关联数组按照键升序排序
    2. 拼接成字符串 keyvalkey2val2key3val3
    3. 再2次md5生成32位小写串
    4. 与请求参数一起发送到服务器
     *
     * author：wh
     * @return array
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     */
    function tologin(){
        if(!request()->isPost()){
            return Tools::set_fail('错误');
        }
        Tools::log_to_write_txt(['登录入参：'=>input()]);
        $input_data = input();
        if(empty($input_data)){
            return Tools::set_fail('请求参数错误');
        }
        if(empty($input_data['username'])){
            return Tools::set_fail('用户名或密码错误');
        }
        if(empty($input_data['password'])){
            return Tools::set_fail('用户名或密码错误.');
        }
        // start 这段代码可放在请求合法性里面校验 Tools::signature
        if(empty($input_data['timestamp'])){
            return Tools::set_fail('请求错误..');
        }
        if(empty($input_data['noncestr'])){
            return Tools::set_fail('请求错误...');
        }
        if(empty($input_data['sign'])){
            return Tools::set_fail('请求错误。');
        }

        $sign = $input_data['sign'];//md5后的字符串
        unset($input_data['sign']);
        //验签
        $signstr = Tools::signature($input_data);
        //Tools::log_to_write_txt([
        //    '签名日志:',
        //    $signstr,
        //    $sign,
        //    'input'=>input(),
        //]);
        if($signstr != $sign){
            return Tools::set_fail('非法请求');
        }

        //请求时间超过有效期   N分钟内有效
        if(time()-5*60 > strtotime($input_data['timestamp'])){
            return Tools::set_fail('请求失效');
        }
        // end 这段代码可放在请求合法性里面校验 Tools::signature

        $username = $input_data['username'];
        $user = Db::table($this->user_table)->where('username',$username)->find();
        if(empty($user)){
            return Tools::set_fail('账号密码错误！');
        }
        //校验密码
        if($user['password'] != $input_data['password']){
            return Tools::set_fail('账号密码错误！!');
        }

        $expires = time()+12*60*60;



        //返回票据
        $ticketstr = md5($user['username'].$user['expires']);
        //保存ticket
        $user[$ticketstr] = $expires;//N秒后过期
        //修改有效期
        Db::table($this->user_table)
            ->data([
                'ticket'=>$ticketstr,
                'expires'=>7*86400+time(),//7天
            ])
            ->where('username',$username)
            ->update();
        session('api_user',$user);//备用(跨环境情况下session不生效)

        return Tools::set_ok('登录成功',['ticket'=>$ticketstr]);
    }

    /**
     * desc：验证登录，根据提交的ticket来校验是否登录，适用于跨环境登录
     *
     * author：wh
     * @return bool
     */
    function isLogin(){

        $ticket = input('ticket');
        if(empty($ticket)){
            Tools::log_to_write_txt(['title'=>'业务ticket字段不存在','input'=>input()]);
            return false;
        }
        $user = Db::table(TabConf::$fa_users)
            ->where('ticket',$ticket)
            ->find();
        if(empty($user)){
            Tools::log_to_write_txt(['title'=>'用户未登录']);
            return false;
        }
        //无效票据
        //if(empty($user[$ticket])){
        //    Tools::log_to_write_txt(['title'=>'未获取到用户ticket',$ticket=>$user]);
        //
        //    return false;
        //}
        //已过期
        if(time() > $user['expires']){
            Tools::log_to_write_txt(['title'=>'ticket已过期',$ticket=>$user]);
            return false;
        }
        return true;
    }
    /**
     * desc：用户注册
     *
     * 此模块通用性不高，按需使用
     *
     * author：wh
     */
    function reg($input_data){
        try {
            if(empty($input_data['username'])){
                return Tools::set_fail('用户名错误');
            }
            if(empty($input_data['password'])){
                return Tools::set_fail('密码错误.');
            }
            if(empty($input_data['password2'])){
                return Tools::set_fail('密码错误!');
            }
            if($input_data['password'] != $input_data['password2']){
                return Tools::set_fail('密码不一致!');
            }
            $insert_data = [
                'username'=>$input_data['username'],
                'password'=>$input_data['password'],
            ];
            if(!empty($input_data['phone'])){
                $insert_data['phone'] = $input_data['phone'];
            }
            Db::table($this->user_table)
                ->data($insert_data)
                ->insert();
            return Tools::set_ok();
        }catch (\Exception $e){
            Tools::error_txt_log($e);
            return Tools::set_fail();
        }
    }
}