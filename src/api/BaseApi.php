<?php
/*
 * description：
 * author：wh
 * email：
 * createTime：{2020/11/11} {16:14} 
 */


namespace wanghua\general_utility_tools_php\api;

use wanghua\general_utility_tools_php\exception\api\ApiException;
use wanghua\general_utility_tools_php\tool\Tools;

/**
 * @deprecated 废弃
 * desc：api交互
 *
 * author：wh
 * Class BaseApi
 * @package app\apidata
 */
class BaseApi
{
    protected $post_url = '';
    private $prefix = 'api_';
    protected $log_file_name = 'log';
    protected $__sit__ = '';
    function __construct(string $url)
    {
        $this->post_url = $url;
        if(empty($this->post_url)) {
            Tools::log_to_write_txt(['error'=>'API OPERATE: '.ApiException::EMPTY_URL_ERROR], $this->getLogFileName());
            throw new \Exception(ApiException::EMPTY_URL_ERROR);
        }
    }

    /**
     * desc：设置api请求日志文件名称
     *
     * 日志保存在runtime中
     *
     * author：wh
     * @param string $log_file_name
     */
    function setLogFileName(string $log_file_name=''){
        if($log_file_name)$this->log_file_name = $log_file_name;
    }

    /**
     * desc 获取记录日志的文件名
     * author：wh
     * @return string
     */
    function getLogFileName(){
        return $this->prefix.$this->log_file_name;
    }

    /**
     * desc：设置调用位置
     * author：wh
     * @param $__file__
     * @param $__line__
     */
    function setFileDir($__file__, $__line__){
        $this->__sit__ = $__file__.' > '.$__line__;
    }

    /**
     * desc：错误位置
     * author：wh
     * @return string
     */
    function getErrorSit(){
        return $this->__sit__;
    }

    /**
     * desc：
     *
     * author：wh
     * @param array $params
     */
    private function mergeAuthParams(array &$params){
        $params['nonce'] = Tools::rand_str(10);
        $params['timestamp'] = time();
        $token = config('service_framework_config.sign_token');
        $params['sign'] = Tools::signature($params, $token);
    }
    /**
     * desc：执行post请求, 带权限参数
     *
     * 同框架、内部系统推荐
     *
     * 返回结果由code 错误码和msg 错误信息组成
     *
     * author：wh
     * @param $param 以表单格式提交数据
     * @return array|bool|int|mixed|string
     * @throws ApiException
     * @throws \think\Exception
     */
    function apiPost($param){
        $this->mergeAuthParams($param);

        Tools::log_to_write_txt(['API OPERATE, IN',$this->post_url,$param], $this->getLogFileName());
        $res = Tools::curl_post($this->post_url, $param);
        Tools::log_to_write_txt(['API OPERATE, OUT',$this->post_url, $res], $this->getLogFileName());

        if(empty($res['data'])) return $res;

        $data = json_decode($res['data'], true);
        if(empty($data['code'])) {
            Tools::log_to_write_txt([
                'error: api操作,错误',
                'post_url'=>$this->post_url,
                'error_info'=>ApiException::API_RESPONSE_FORMAT_ERROR,
                'error_sit'=>$this->getErrorSit(),
                'result'=>$res,
            ], $this->getLogFileName());
            throw new \Exception(ApiException::API_RESPONSE_FORMAT_ERROR);
        }
        return $data;
    }

    /**
     *
     * desc：执行post请求
     *
     * 返回结果由code 错误码和msg 错误信息组成
     *
     * author：wh
     * @param $param 以json格式提交数据
     * @return array|bool|int|mixed|string
     * @throws ApiException
     * @throws \think\Exception
     */
    function do($param){

        Tools::log_to_write_txt(['API OPERATE, IN',$this->post_url,$param], $this->getLogFileName());
        $res = Tools::curl_post($this->post_url, json_encode($param));
        Tools::log_to_write_txt(['API OPERATE, OUT',$this->post_url, $res], $this->getLogFileName());

        if(empty($res['data'])) return $res;

        $data = json_decode($res['data'], true);
        if(empty($data['code'])) {
            Tools::log_to_write_txt([
                'error: api操作,错误',
                'post_url'=>$this->post_url,
                'error_info'=>ApiException::API_RESPONSE_FORMAT_ERROR,
                'error_sit'=>$this->getErrorSit(),
                'result'=>$res,
            ], $this->getLogFileName());
            throw new \Exception(ApiException::API_RESPONSE_FORMAT_ERROR.$this->post_url);
        }
        return $data;
    }

    /**
     *
     * desc：执行请求
     *
     * do 方法的改版，返回结果由state 错误码和err 错误信息组成
     *
     * author：wh
     * @param $param 以json格式提交数据
     * @return array|bool|int|mixed|string
     * @throws ApiException
     * @throws \think\Exception
     */
    function apiDo($param){

        Tools::log_to_write_txt(['API OPERATE, IN',$this->post_url,$param], $this->getLogFileName());
        $res = Tools::curl_post($this->post_url, json_encode($param));
        Tools::log_to_write_txt(['API OPERATE, OUT',$this->post_url, $res], $this->getLogFileName());
        if(empty($res['data'])) return $res;


        $data = json_decode($res['data'], true);

        if(is_null($data['state'])) {
            Tools::log_to_write_txt([
                'error: api操作,错误',
                'post_url'=>$this->post_url,
                'error_info'=>ApiException::API_RESPONSE_FORMAT_ERROR,
                'error_sit'=>$this->getErrorSit(),
                'result'=>$res,
            ], $this->getLogFileName());
            throw new \Exception(ApiException::API_RESPONSE_FORMAT_ERROR);
        }
        $data['code'] = isset($data['state'])&&$data['state']==0?200:$data['state'];
        $data['msg'] = isset($data['err'])?$data['err']:'api接口错误';
        return $data;
    }

    /**
     *
     * desc：执行请求
     *
     * 同框架、内部系统推荐
     *
     * author：wh
     * @param $param 以表单方式提交数据
     * @return array|bool|int|mixed|string
     * @throws ApiException
     * @throws \think\Exception
     */
    function doPost($param){

        Tools::log_to_write_txt(['API OPERATE, IN',$this->post_url,$param], $this->getLogFileName());
        $res = Tools::curl_post($this->post_url, $param);
        Tools::log_to_write_txt(['API OPERATE, OUT',$this->post_url, $res], $this->getLogFileName());

        if(empty($res['data'])) return $res;

        $data = json_decode($res['data'], true);
        if(empty($data['code'])) {
            Tools::log_to_write_txt([
                'error: api操作,错误',
                'post_url'=>$this->post_url,
                'error_info'=>ApiException::API_RESPONSE_FORMAT_ERROR,
                'error_sit'=>$this->getErrorSit(),
                'result'=>$res,
            ], $this->getLogFileName());
            throw new \Exception(ApiException::API_RESPONSE_FORMAT_ERROR);
        }
        return $data;
    }
}