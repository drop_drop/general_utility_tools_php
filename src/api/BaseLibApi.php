<?php
/*
 * description：
 * author：wh
 * email：
 * createTime：{2022/02/24} {15:01} 
 */

namespace wanghua\general_utility_tools_php\api;


class BaseLibApi
{

    /**
     * desc：创建目录
     *
     * author：wh
     * @param $apidir
     */
    function createDir($apidir){
        if(!is_dir($apidir)){
            mkdir($apidir,0777,true);
        }
    }

    /**
     * desc：文件存在不写入
     *
     * author：wh
     * @param $php_file
     * @param $php
     */
    function createFile($php_file,$php){
        if(!file_exists($php_file)){
            file_put_contents($php_file,$php);
        }
    }

    /**
     * desc：追加文本
     *
     * author：wh
     * @param $php_file
     * @param $php
     */
    function createFileExists($php_file,$php){
        file_put_contents($php_file,$php,FILE_APPEND);
    }

    function ext(){
        return '.php';
    }
}