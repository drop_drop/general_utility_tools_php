<?php
/*
 * description：
 * author：wh
 * email：
 * createTime：{2021/11/02} {20:17} 
 */

namespace wanghua\general_utility_tools_php\md5rsa;


/**
 * md5rsa对称加密
 *
 * Class Md5RSA
 * @package app\index\pay
 */
class Md5RSA{

    /**
     * 利用约定数据和私钥生成数字签名
     * @param string $data 待签数据
     * @return String 返回签名
     *
     * 使用示例：
     *
     *  $crypted = (new Md5RSA())->sign($signMsg='待加密字符串',$mo_bao_cust_privite_key='私钥');
        if(empty($crypted)){
            return Tools::set_res(500, '加密失败');
        }

        $eb64_cry = base64_encode($crypted);


     *
     * 私钥格式参考：
     * -----BEGIN PRIVATE KEY-----
    MIICdgIBADANBgkqhkiG9w0BAQEFAASCAmAwggJcAgEAAoGBAKaQA2YLzj0g6e1h
    BB1KAu8wwabxX+7zfwBMdqhnC/B6hjJ37/tbH5vHlt2S0lvjphupLW/PdVF1ezIk
    haGk1ya0qFiKfByXzdou3Ml4qzOemmozIAix5ooh4Cbq1Py6202SvbM7KYv89syk
    HJNB4QWXLLWmgsHCuiXQXhRJz6jDAgMBAAECgYAIF5cSriAm+CJlVgFNKvtZg5Tk
    93UhttLEwPJC3D7IQCuk6A7Qt2yhtOCvgyKVNEotrdp3RCz++CY0GXIkmE2bj7i0
    fv5vT3kWvO9nImGhTBH6QlFDxc9+p3ukwsonnCshkSV9gmH5NB/yFoH1m8tck2Gm
    BXDj+bBGUoKGWtQ7gQJBANR/jd5ZKf6unLsgpFUS/kNBgUa+EhVg2tfr9OMioWDv
    MSqzG/sARQ2AbO00ytpkbAKxxKkObPYsn47MWsf5970CQQDIqRiGmCY5QDAaejW4
    HbOcsSovoxTqu1scGc3Qd6GYvLHujKDoubZdXCVOYQUMEnCD5j7kdNxPbVzdzXll
    9+p/AkEAu/34iXwCbgEWQWp4V5dNAD0kXGxs3SLpmNpztLn/YR1bNvZry5wKew5h
    z1zEFX+AGsYgQJu1g/goVJGvwnj/VQJAOe6f9xPsTTEb8jkAU2S323BG1rQFsPNg
    jY9hnWM8k2U/FbkiJ66eWPvmhWd7Vo3oUBxkYf7fMEtJuXu+JdNarwJAAwJK0YmO
    LxP4U+gTrj7y/j/feArDqBukSngcDFnAKu1hsc68FJ/vT5iOC6S7YpRJkp8egj5o
    pCcWaTO3GgC5Kg==
    -----END PRIVATE KEY-----
     */
    public function sign(string $data, string $private_key)
    {
        if (empty($data) || empty($private_key))
        {
            return false;
        }
        //$private_key = file_get_contents(dirname(__FILE__).'/rsa_private_key.pem');
        //if (empty($private_key))
        //{
        //    echo "Private Key error!";
        //    return False;
        //}

        $pkeyid = openssl_get_privatekey($private_key);
        if (empty($pkeyid))
        {
            // "private key resource identifier False!";
            return false;
        }

        $verify = openssl_sign($data, $signature, $pkeyid, OPENSSL_ALGO_MD5);
        openssl_free_key($pkeyid);
        return $signature;
    }

    /**
     * 利用公钥和数字签名以及约定数据验证合法性
     * @param string $data 待验证数据
     * @param string $signature 数字签名
     * @return -1:error验证错误 1:correct验证成功 0:incorrect验证失败
     */
    public function isValid(string $data, string $signature, string $public_key)
    {
        if (empty($data) || empty($signature) || empty($public_key))
        {
            return false;
        }

        //$public_key = file_get_contents(dirname(__FILE__).'/rsa_public_key.pem');
        //if (empty($public_key))
        //{
        //    echo "Public Key error!";
        //    return False;
        //}

        $pkeyid = openssl_get_publickey($public_key);
        if (empty($pkeyid))
        {
            // "public key resource identifier False!";
            return false;
        }

        $ret = openssl_verify($data, $signature, $pkeyid, OPENSSL_ALGO_MD5);
        //switch ($ret)
        //{
        //    case -1:
        //        echo "error";
        //        break;
        //    default:
        //        echo $ret==1 ? "correct" : "incorrect";//0:incorrect
        //        break;
        //}
        return $ret==1;
    }

}