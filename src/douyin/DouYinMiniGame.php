<?php
/*
 * description：
 * author：wh
 * email：
 * createTime：{2024/4/4} {12:36} 
 */

namespace wanghua\general_utility_tools_php\douyin;


use wanghua\general_utility_tools_php\http\Curl;
use wanghua\general_utility_tools_php\tool\Tools;

class DouYinMiniGame extends BaseDouYin
{
    public $appid = 'tt3c974a2aa3a6ee7c02';
    public $secret = '5c2414e47a18b9655a164b14ef31ac7cb58179ca';


    /**
     * desc：每2小时更新一次
     *
     * https://developer.open-douyin.com/docs/resource/zh-CN/mini-game/develop/server/interface-request-credential/get-access-token
     *
     * {"err_no":0,"err_tips":"success","data":{"access_token":"0801121846384c316e52536e5a76396769565145696f597742673d3d","expires_in":7200,"expiresAt":1712208063,"expires_at":1712208063}}
     *
     * author：wh
     */
    function getAccessToken(){
        $url = "https://minigame.zijieapi.com/mgplatform/api/apps/v2/token";
        $method = 'POST';
        //$Scope = 'open.ttgame.mgplatform';

        $params = [

            'appid'=>$this->appid,
            'secret'=>$this->secret,
            'grant_type'=>'client_credential',
        ];
        $header = [
            'Accept: application/json',
            'content-type: application/json'
        ];
        $res = Curl::curl_request($url,$method,json_encode($params),$header);

        $json_arr = json_decode($res, true);
        if($json_arr['err_no']){
            return Tools::set_res($json_arr['err_no'],$json_arr['err_tips']);
        }
        //保存
        session('ses_dou_yin_access_token',$json_arr['data']);
        return Tools::set_ok('ok',$json_arr['data']);
    }

    /**
     * desc：实时获取token
     * author：wh
     * @return array
     */
    function realTimeGetAccessToken(){
        $json_arr = session('ses_dou_yin_access_token');
        if(empty($json_arr)){
            return $this->getAccessToken();
        }
        if(time() >= $json_arr['expiresAt']){
            //过期
            return $this->getAccessToken();
        }
        return Tools::set_ok('ok',$json_arr);
    }

    /**
     * desc：前端tt.login触发调用
     *
     * author：wh
     */
    function jscode2session($code='',$anonymous_code=''){
        $url = 'https://minigame.zijieapi.com/mgplatform/api/apps/jscode2session';
        //$Scope = 'open.ttgame.mgplatform';
        $method = 'GET';

        if(empty($code) && empty($anonymous_code)){
            return Tools::set_fail('tt.login 接口返回的匿名登录凭证（code 和 anonymous_code 至少要有一个）');
        }
        $params = [
            'appid'=>$this->appid,
            'secret'=>$this->secret,
        ];
        if($code){
            $params['code'] = $code;
        }
        if($anonymous_code){
            $params['anonymous_code'] = $anonymous_code;
        }
        $header = [
            'Accept: application/json',
            'content-type: application/json'
        ];
        $url .= '?'.http_build_query($params);
        //dump($url);
        Tools::log_to_write_txt(['input'=>$url]);
        $res = Curl::curl_request($url,$method,[],$header);
        Tools::log_to_write_txt(['output'=>$res]);
        //dump($res);
        $json_arr = json_decode($res, true);
        //dump($json_arr);die;
        if($json_arr['error']){
            return Tools::set_res($json_arr['errcode'],$json_arr['errmsg']);
        }
        //保存
        session('ses_dou_yin_js_code_2_session',$json_arr);
        return Tools::set_ok('ok',$json_arr);
    }

    /**
     * desc：创建二维码
     * 接口说明
     *  获取小程序/小游戏的二维码。该二维码可通过任意 app 扫码打开，
     * 能跳转到开发者指定的对应字节系 app 内拉起小程序/小游戏，并传入开发者指定的参数。
     * 通过该接口生成的二维码，永久有效，暂无数量限制。
     *
     * ⚠ Tip：在使用该功能之前请记得先配置您的默认分享文案和图片，配置方式可参考论坛。
     * ⚠ Tip：小程序的 path 要 encode 一次，如 pages%3fparam%3dtrue，小游戏的 path 为 JSON 字符串，
     *    如{"param":true}，否则会导致取不到。
     *
     * 参数：
     * code和anonymous_code二选一 必须
     * appname 可选，目标打开应用名称 默认douyin
     * background 可选，背景色，rgb格式，英文逗号隔开，默认透明色
     * path 可选，小程序/小游戏启动参数，小程序则格式为 encode({path}?{query})，小游戏则格式为 JSON 字符串，默认为空
     * width 宽度 可选，二维码宽度，单位 px，最小 280px，最大 1280px，默认为 430px
     * line_color 可选，二维码线条颜色，默认为黑色，rgb格式，英文逗号隔开，默认黑色
     * set_icon 可选，是否展示小程序/小游戏 icon，默认不展示，传yes展示，no不展示，默认no
     *
     * author：wh
     */
    function createQRCode($path='',$appname='',$width='',$background='',$line_color='',$set_icon='no'){
        try {
            $url = 'https://minigame.zijieapi.com/mgplatform/api/apps/qrcode';
            //$Scope = 'open.ttgame.mgplatform';
            $method = 'POST';

            $json_arr = $this->realTimeGetAccessToken();
            if(empty($json_arr)){
                return Tools::set_fail('请重新授权');
            }

            $params = [
                'access_token'=>$json_arr['data']['access_token'],
            ];
            if($appname){
                $params['appname'] = $appname;
            }
            if($width){
                $params['width'] = $width;
            }
            //if($path){
            //小程序/小游戏启动参数，小程序则格式为 encode({path}?{query})，小游戏则格式为 JSON 字符串，默认为空
            $params['path'] = $path;
            //}
            //英文逗号隔开，r,g,b格式
            if($background){
                $background_rgb = explode(',',$background);
                $params['r'] = $background_rgb[0];
                $params['g'] = $background_rgb[1];
                $params['b'] = $background_rgb[2];
            }
            if($line_color){
                $line_color_rgb = explode(',',$line_color);
                $params['r'] = $line_color_rgb[0];
                $params['g'] = $line_color_rgb[1];
                $params['b'] = $line_color_rgb[2];
            }
            $params['set_icon'] = $set_icon=='yes';

            $header = [
                'Accept: application/json',
                'content-type: application/json'
            ];

            Tools::log_to_write_txt(['input'=>input(),'$params'=>$params]);
            $res = Curl::curl_request($url,$method,json_encode($params),$header);
            Tools::log_to_write_txt(['output'=>$res]);

            //流
            return $res;
        }catch (\Exception $e){
            Tools::error_txt_log($e);
            return Tools::set_fail();
        }
    }
}