<?php
/*
 * description：
 * author：wh
 * email：
 * createTime：{2024/10/16} {14:07} 
 */
namespace wanghua\general_utility_tools_php\alipay\pay;

use wanghua\general_utility_tools_php\alipay\BaseAlipay;
use wanghua\general_utility_tools_php\Mmodel;
use wanghua\general_utility_tools_php\tool\Tools;


/**
 * 支付宝app支付
 *
 * Class AlipayAppPay
 * @package wanghua\general_utility_tools_php\alipay\pay
 */
class AlipayAppPay extends BaseAlipay
{
    public $notifyUrl;

    /**
     * desc：app支付
     *
     * [备注]
     * 需要引入alipay-sdk-php-all官方SDK类库
     * [使用]
     * [alipay-sdk-php-all.zip]解压后放在项目中，然后在代码中引入
     * [流程]
     * 1、生成订单信息
     * 2、调用支付接口，生成orderStr，返回给客户端
     * 3、客户端用浏览器打开，浏览器会自动跳转到支付宝页面，完成支付
     *
     * api/alipay/apppay
     * author：wh
     */
    function apppay($order){
        $root = Tools::get_root_path();
        require_once $root.'library/alipay-sdk-php-all/aop/AopClient.php';
        require_once $root.'library/alipay-sdk-php-all/aop/AopCertClient.php';
        require_once $root.'library/alipay-sdk-php-all/aop/AopCertification.php';
        require_once $root.'library/alipay-sdk-php-all/aop/AlipayConfig.php';
        require_once $root.'library/alipay-sdk-php-all/aop/request/AlipayTradeAppPayRequest.php';

        // 初始化SDK
        $alipayClient = new \AopClient($this->getAlipayConfig($this->pay_config));
        // 构造请求参数以调用接口
        $request = new \AlipayTradeAppPayRequest();
        $model = array();
        // 设置商户订单号
        $model['out_trade_no'] = $order['orderid'];

        // 设置订单总金额
        $model['total_amount'] = $order['real_amount'];//实付金额

        // 设置订单标题
        $model['subject'] = $order['remark'];

        //// 设置产品码
        //$model['product_code'] = "QUICK_MSECURITY_PAY";
        //
        ////设置订单包含的商品列表信息
        //$goodsDetail = array();
        //$goodsDetail0 = array();
        //$goodsDetail0['goods_name'] = "ipad";
        //$goodsDetail0['alipay_goods_id'] = "20010001";
        //$goodsDetail0['quantity'] = 1;
        //$goodsDetail0['price'] = "2000";
        //$goodsDetail0['goods_id'] = "apple-01";
        //$goodsDetail0['goods_category'] = "34543238";
        //$goodsDetail0['categories_tree'] = "124868003|126232002|126252004";
        //$goodsDetail0['show_url'] = "http://www.alipay.com/xxx.jpg";
        //$goodsDetail[] = $goodsDetail0;
        //$model['goods_detail'] = $goodsDetail;
        //
        ////设置订单绝对超时时间
        //$model['time_expire'] = "2016-12-31 10:05:00";
        //
        ////设置业务扩展参数
        //$extendParams = array();
        //$extendParams['sys_service_provider_id'] = "2088511833207846";
        //$extendParams['hb_fq_seller_percent'] = "100";
        //$extendParams['hb_fq_num'] = "3";
        //$extendParams['industry_reflux_info'] = "{\"scene_code\":\"metro_tradeorder\",\"channel\":\"xxxx\",\"scene_data\":{\"asset_name\":\"ALIPAY\"}}";
        //$extendParams['specified_seller_name'] = "XXX的跨境小铺";
        //$extendParams['royalty_freeze'] = "true";
        //$extendParams['card_type'] = "S0JP0000";
        //$model['extend_params'] = $extendParams;
        //
        ////设置公用回传参数
        //$model['passback_params'] = "merchantBizType%3d3C%26merchantBizNo%3d2016010101111";
        //
        ////设置商户的原始订单号
        //$model['merchant_order_no'] = "20161008001";
        //
        ////设置外部指定买家
        //$extUserInfo = array();
        //$extUserInfo['cert_type'] = "IDENTITY_CARD";
        //$extUserInfo['cert_no'] = "362334768769238881";
        //$extUserInfo['name'] = "李明";
        //$extUserInfo['mobile'] = "16587658765";
        //$extUserInfo['min_age'] = "18";
        //$extUserInfo['need_check_info'] = "F";
        //$extUserInfo['identity_hash'] = "27bfcd1dee4f22c8fe8a2374af9b660419d1361b1c207e9b41a754a113f38fcc";
        //$model['ext_user_info'] = $extUserInfo;
        //
        ////设置通知参数选项
        //$queryOptions = array();
        //$queryOptions[] = "hyb_amount";
        //$queryOptions[] = "enterprise_pay_info";
        //$model['query_options'] = $queryOptions;

        //dump($pay_config);
        $request->setBizContent(json_encode($model, JSON_UNESCAPED_UNICODE));
        $request->setNotifyUrl($this->notifyUrl);
        // 如果是第三方代调用模式，请设置app_auth_token（应用授权令牌）
        $authToken = $this->pay_config['app_auth_token'];//"<-- 请填写应用授权令牌 -->"
        $orderStr = $alipayClient->sdkExecute($request, $authToken);

        return Tools::set_ok('ok',$orderStr);
    }

    private function getAlipayConfig($pay_config)
    {
        $privateKey = $pay_config['alipay_app_private_key'];//'<-- 请填写您的应用私钥，例如：MIIEvQIBADANB ... ... -->';
        $alipayPublicKey = $pay_config['alipay_app_public_key'];//'<-- 请填写您的支付宝公钥，例如：MIIBIjANBg... -->';
        $alipayConfig = new \AlipayConfig();
        $alipayConfig->setServerUrl('https://openapi.alipay.com/gateway.do');
        $alipayConfig->setAppId($pay_config['app_id']);//'<-- 请填写您的AppId，例如：2019091767145019 -->'
        $alipayConfig->setPrivateKey($privateKey);
        $alipayConfig->setFormat('json');
        $alipayConfig->setAlipayPublicKey($alipayPublicKey);
        $alipayConfig->setCharset('UTF-8');
        $alipayConfig->setSignType('RSA2');
        return $alipayConfig;
    }
}