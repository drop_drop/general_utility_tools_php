<?php
/*
 * description：
 * author：wh
 * email：
 * createTime：{2025/2/24} {20:06} 
 */

namespace wanghua\general_utility_tools_php\alipay\pay;


use wanghua\general_utility_tools_php\alipay\BaseAlipay;
use wanghua\general_utility_tools_php\tool\Tools;

/**
 * 支付宝-手机网站支付
 * Class AlipayWapPay
 * @package wanghua\general_utility_tools_php\alipay\pay
 */
class AlipayWapPay extends BaseAlipay
{

    /**
     * desc：移动端wap原生支付宝支付(证书模式-当系统存在转账和支付时，支付必须使用证书模式)
     * 
     * 支付宝手机网站支付，高内聚，低耦合
     * author：wh
     * 
     * @return string html
     */
    function alipayWapH5payCert($order,$notify_url,$pay_end_url){
        $root_path = Tools::get_root_path();
        require $root_path.'library/alipay-sdk-php-all/aop/AopCertClient.php';
        require $root_path.'library/alipay-sdk-php-all/aop/request/AlipayTradeWapPayRequest.php';

        Tools::log_to_write_txt(['支付宝支付(证书模式)-提交支付，参数(出参在回调中)：', $order,$notify_url,$pay_end_url]);


        //商户订单号，商户网站订单系统中唯一订单号，必填
        $orderid = $order['orderid'];
        $total_amount = $order['real_amount'];//交易金额
        $subject = $order['goods_name']?:"购物消费￥{$total_amount}元";//订单标题

        //$game_admin_domain = any_sys_env_domain_conf('store_outer_api_domain');
        //这里可以把支付结果通知到其它系统
        //$notify_url = $game_admin_domain.'/paytool/Alipay/alipayNotify';

        //跳转到支付结果并显示商品推荐
        //$pay_end_url = url('paytool/recommend/payend','',false,true);//支付结果展示

        //$alipayConfig = aliPayConfPublicAcc();
        $alipayConfig = $this->alipayConfig;

        /** 初始化 **/
        $aop = new \AopCertClient();

        /** 支付宝网关 **/
        $aop->gatewayUrl = "https://openapi.alipay.com/gateway.do";

        /** 应用id,如何获取请参考：https://opensupport.alipay.com/support/helpcenter/190/201602493024 **/
        $aop->appId = $alipayConfig['app_id'];

        /** 密钥格式为pkcs1，如何获取私钥请参考：https://opensupport.alipay.com/support/helpcenter/207/201602469554  **/
        $aop->rsaPrivateKey = $alipayConfig['alipay_app_private_key'];

        /** 应用公钥证书路径，下载后保存位置的绝对路径  **/
        $appCertPath = $root_path.$alipayConfig['appCertPublicKey'];

        /** 支付宝公钥证书路径，下载后保存位置的绝对路径 **/
        $alipayCertPath = $root_path.$alipayConfig['cert_path_alipayCertPublicKey_RSA2'];

        /** 支付宝公钥证书路径，下载后保存位置的绝对路径 **/
        $rootCertPath = $root_path.$alipayConfig['cert_path_alipayRootCert'];

        /** 设置签名类型 **/
        $aop->signType= "RSA2";

        /** 设置请求格式，固定值json **/
        $aop->format = "json";

        /** 设置编码格式 **/
        $aop->charset= "utf-8";

        /** 调用getPublicKey从支付宝公钥证书中提取公钥 **/
        $aop->alipayrsaPublicKey = $aop->getPublicKey($alipayCertPath);

        /** 是否校验自动下载的支付宝公钥证书，如果开启校验要保证支付宝根证书在有效期内 **/
        $aop->isCheckAlipayPublicCert = true;

        /** 调用getCertSN获取证书序列号 **/
        $aop->appCertSN = $aop->getCertSN($appCertPath);

        /** 调用getRootCertSN获取支付宝根证书序列号 **/
        $aop->alipayRootCertSN = $aop->getRootCertSN($rootCertPath);

        /** 实例化具体API对应的request类，类名称和接口名称对应，当前调用接口名称：alipay.trade.wap.pay **/
        $request = new  \AlipayTradeWapPayRequest();

        /** 设置业务参数 **/
        $request->setBizContent("{" .

            /** 商户订单号，商户自定义，需保证在商户端不重复，如：20150320010101001 **/
            "'out_trade_no':'$orderid'," .

            /** 销售产品码,固定值：QUICK_WAP_WAY **/
            "'product_code':'QUICK_WAP_WAY'," .

            /** 订单金额，精确到小数点后两位 **/
            "'total_amount':'$total_amount'," .

            /** 订单标题 **/
            "'subject':'$subject'," .

            /** 业务扩展参数 **/
            //  "'extend_params':{" .
            /** 系统商编号，填写服务商的PID用于获取返佣，返佣参数传值前提：传值账号需要签约返佣协议，用于isv商户。 **/
            //"'sys_service_provider_id':'2088511833207846'," .

            /** 花呗分期参数传值前提：必须有该接口花呗收款准入条件，且需签约花呗分期 **/
            /** 指定可选期数，只支持3/6/12期，还款期数越长手续费越高 **/
            // "'hb_fq_num':'3'," .

            /** 指定花呗分期手续费承担方式，手续费可以由用户全承担（该值为0），也可以商户全承担（该值为100），但不可以共同承担，即不可取0和100外的其他值。 **/
            //"'hb_fq_seller_percent':'100'" .
            //  "}," .

            /** 订单描述 **/
            "'body':'$subject'" .
            "}");

        /**注：支付结果以异步通知为准，不能以同步返回为准，因为如果实际支付成功，但因为外力因素，如断网、断电等导致页面没有跳转，则无法接收到同步通知；**/
        /** 支付完成的跳转地址,用于用户视觉感知支付已成功，传值外网可以访问的地址，如果同步未跳转可参考该文档进行确认：https://opensupport.alipay.com/support/helpcenter/193/201602474937 **/
        $request->setReturnUrl($pay_end_url);

        /** 异步通知地址，以http或者https开头的，商户外网可以post访问的异步地址，用于接收支付宝返回的支付结果，如果未收到该通知可参考该文档进行确认：https://opensupport.alipay.com/support/helpcenter/193/201602475759 **/
        $request->setNotifyUrl($notify_url);

        /** 调用SDK生成支付链接，可在浏览器打开链接进入支付页面 **/
        $result = $aop->pageExecute ($request,'get');

        /**第三方调用（服务商模式），传值app_auth_token后，会收款至授权token对应商家账号，如何获传值app_auth_token请参考文档：https://opensupport.alipay.com/support/helpcenter/79/201602494631 **/
        //$result = $aop->pageExecute($request,'get',"传入获取到的app_auth_token值");

        /** 获取接口调用结果，如果调用失败，可根据返回错误信息到该文档寻找排查方案：https://opensupport.alipay.com/support/helpcenter/93 **/
        //print_r(htmlspecialchars($result));
        $html = <<<EOF
<html><head></head><body style="display: none" onload="javascript:location.href='$result';"></body></html>
EOF;
        return $html;
    }
    function alipayWapH5payCertReturnUrl($order,$notify_url,$pay_end_url){
        $root_path = Tools::get_root_path();
        require $root_path.'library/alipay-sdk-php-all/aop/AopCertClient.php';
        require $root_path.'library/alipay-sdk-php-all/aop/request/AlipayTradeWapPayRequest.php';

        Tools::log_to_write_txt(['支付宝支付(证书模式)-提交支付，参数(出参在回调中)：', $order,$notify_url,$pay_end_url]);


        //商户订单号，商户网站订单系统中唯一订单号，必填
        $orderid = $order['orderid'];
        $total_amount = $order['real_amount'];//交易金额
        $subject = $order['goods_name']?:"购物消费￥{$total_amount}元";//订单标题

        //$game_admin_domain = any_sys_env_domain_conf('store_outer_api_domain');
        //这里可以把支付结果通知到其它系统
        //$notify_url = $game_admin_domain.'/paytool/Alipay/alipayNotify';

        //跳转到支付结果并显示商品推荐
        //$pay_end_url = url('paytool/recommend/payend','',false,true);//支付结果展示

        //$alipayConfig = aliPayConfPublicAcc();
        $alipayConfig = $this->alipayConfig;

        /** 初始化 **/
        $aop = new \AopCertClient();

        /** 支付宝网关 **/
        $aop->gatewayUrl = "https://openapi.alipay.com/gateway.do";

        /** 应用id,如何获取请参考：https://opensupport.alipay.com/support/helpcenter/190/201602493024 **/
        $aop->appId = $alipayConfig['app_id'];

        /** 密钥格式为pkcs1，如何获取私钥请参考：https://opensupport.alipay.com/support/helpcenter/207/201602469554  **/
        $aop->rsaPrivateKey = $alipayConfig['alipay_app_private_key'];

        /** 应用公钥证书路径，下载后保存位置的绝对路径  **/
        $appCertPath = $root_path.$alipayConfig['appCertPublicKey'];

        /** 支付宝公钥证书路径，下载后保存位置的绝对路径 **/
        $alipayCertPath = $root_path.$alipayConfig['cert_path_alipayCertPublicKey_RSA2'];

        /** 支付宝公钥证书路径，下载后保存位置的绝对路径 **/
        $rootCertPath = $root_path.$alipayConfig['cert_path_alipayRootCert'];

        /** 设置签名类型 **/
        $aop->signType= "RSA2";

        /** 设置请求格式，固定值json **/
        $aop->format = "json";

        /** 设置编码格式 **/
        $aop->charset= "utf-8";

        /** 调用getPublicKey从支付宝公钥证书中提取公钥 **/
        $aop->alipayrsaPublicKey = $aop->getPublicKey($alipayCertPath);

        /** 是否校验自动下载的支付宝公钥证书，如果开启校验要保证支付宝根证书在有效期内 **/
        $aop->isCheckAlipayPublicCert = true;

        /** 调用getCertSN获取证书序列号 **/
        $aop->appCertSN = $aop->getCertSN($appCertPath);

        /** 调用getRootCertSN获取支付宝根证书序列号 **/
        $aop->alipayRootCertSN = $aop->getRootCertSN($rootCertPath);

        /** 实例化具体API对应的request类，类名称和接口名称对应，当前调用接口名称：alipay.trade.wap.pay **/
        $request = new  \AlipayTradeWapPayRequest();

        /** 设置业务参数 **/
        $request->setBizContent("{" .

            /** 商户订单号，商户自定义，需保证在商户端不重复，如：20150320010101001 **/
            "'out_trade_no':'$orderid'," .

            /** 销售产品码,固定值：QUICK_WAP_WAY **/
            "'product_code':'QUICK_WAP_WAY'," .

            /** 订单金额，精确到小数点后两位 **/
            "'total_amount':'$total_amount'," .

            /** 订单标题 **/
            "'subject':'$subject'," .

            /** 业务扩展参数 **/
            //  "'extend_params':{" .
            /** 系统商编号，填写服务商的PID用于获取返佣，返佣参数传值前提：传值账号需要签约返佣协议，用于isv商户。 **/
            //"'sys_service_provider_id':'2088511833207846'," .

            /** 花呗分期参数传值前提：必须有该接口花呗收款准入条件，且需签约花呗分期 **/
            /** 指定可选期数，只支持3/6/12期，还款期数越长手续费越高 **/
            // "'hb_fq_num':'3'," .

            /** 指定花呗分期手续费承担方式，手续费可以由用户全承担（该值为0），也可以商户全承担（该值为100），但不可以共同承担，即不可取0和100外的其他值。 **/
            //"'hb_fq_seller_percent':'100'" .
            //  "}," .

            /** 订单描述 **/
            "'body':'$subject'" .
            "}");

        /**注：支付结果以异步通知为准，不能以同步返回为准，因为如果实际支付成功，但因为外力因素，如断网、断电等导致页面没有跳转，则无法接收到同步通知；**/
        /** 支付完成的跳转地址,用于用户视觉感知支付已成功，传值外网可以访问的地址，如果同步未跳转可参考该文档进行确认：https://opensupport.alipay.com/support/helpcenter/193/201602474937 **/
        $request->setReturnUrl($pay_end_url);

        /** 异步通知地址，以http或者https开头的，商户外网可以post访问的异步地址，用于接收支付宝返回的支付结果，如果未收到该通知可参考该文档进行确认：https://opensupport.alipay.com/support/helpcenter/193/201602475759 **/
        $request->setNotifyUrl($notify_url);

        /** 调用SDK生成支付链接，可在浏览器打开链接进入支付页面 **/
        $result = $aop->pageExecute ($request,'get');

        /**第三方调用（服务商模式），传值app_auth_token后，会收款至授权token对应商家账号，如何获传值app_auth_token请参考文档：https://opensupport.alipay.com/support/helpcenter/79/201602494631 **/
        //$result = $aop->pageExecute($request,'get',"传入获取到的app_auth_token值");

        /** 获取接口调用结果，如果调用失败，可根据返回错误信息到该文档寻找排查方案：https://opensupport.alipay.com/support/helpcenter/93 **/
        //print_r(htmlspecialchars($result));
//        $html = <<<EOF
//<html><head></head><body style="display: none" onload="javascript:location.href='$result';"></body></html>
//EOF;
        return $result;
    }
}