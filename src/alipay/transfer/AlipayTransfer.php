<?php
/*
 * description：
 * author：wh
 * email：
 * createTime：{2021/09/24} {19:43} 
 */

namespace wanghua\general_utility_tools_php\alipay\transfer;

use Alipay\EasySDK\Kernel\Config;
use Alipay\EasySDK\Kernel\Factory;
use wanghua\general_utility_tools_php\alipay\BaseAlipay;
use wanghua\general_utility_tools_php\tool\Tools;

/**
 * 支付宝转账（独立且区别于支付，连参数都不能用支付参数）
 *
 * 注：换句话说，转账是转账的参数，支付是支付的参数，不能混为一谈（以此备注长记性）。
 *
 * 环境要求：PHP7.0+，ThinkPHP5.0+
 *
 * 依赖：
 * composer require alipaysdk/easysdk:^2.0
 *
 * composer require wanghua/general-utility-tools-php dev-master
 *
 * 重要说明：
 *
 * 支付宝开放平台可能有多个应用，可能存在一个默认应用(应用名称:应用2.0签约2020102692466480)，转账配置就用此应用的参数，而不是其它应用的配置！
 *
 */
class AlipayTransfer extends BaseAlipay
{


    /**
     * desc：转账到支付宝（证书模式）
     * author：wh
     * @param $transferData TransferConfig.php
     * @return mixed
     *
     * 使用案例：
     *
        function transfer(array $trans_data){
            $alipayConfig = \config('pay_config.alipay_transfer');//转账配置

            $trans_config = new TransferConfig();
            $trans_config->cert_path_alipayCertPublicKey_RSA2 = Tools::get_root_path().$alipayConfig['cert_path_alipayCertPublicKey_RSA2'];//'<-- 请填写您的支付宝公钥证书文件路径，例如：/foo/alipayCertPublicKey_RSA2.crt -->';
            $trans_config->cert_path_alipayRootCert = Tools::get_root_path().$alipayConfig['cert_path_alipayRootCert'];//'<-- 请填写您的支付宝根证书文件路径，例如：/foo/alipayRootCert.crt" -->';
            $trans_config->appCertPublicKey = Tools::get_root_path().$alipayConfig['appCertPublicKey'];//'<-- 请填写您的应用公钥证书文件路径，例如：/foo/appCertPublicKey_2019051064521003.crt -->'


            $order_prefix = 'trs';
            $order_id = Tools::to_create_order_no($order_prefix);
            $trans_config->order_id = $order_id;//订单号 必须
            $trans_config->timestamp = Tools::get_now_date();//请求时间 eg：2020-01-08 10:12:50
            $trans_config->trans_amount = $trans_data['trans_amount'];//转账金额 必须 且为字符串 最低0.1元 取值范围[0.1,100000000]
            $trans_config->order_title = $trans_data['order_title'];//订单标题 可选
            $trans_config->phone = $trans_data['phone'];//支付宝登录手机号 必须
            $trans_config->name = $trans_data['name'];//支付宝真实姓名 必须
            $trans_config->remark = $trans_data['remark'];//支付备注 可选


            $trans = new AlipayTransfer($alipayConfig);
            Tools::log_to_write_txt(['转账到支付宝，入参：'=>input(), 'trans_config'=>$trans_config]);
            $pay_res = $trans->alitransfer($trans_config);
            Tools::log_to_write_txt(['转账到支付宝，出参：'=>$pay_res]);
            //直接返回转账结果，自主验证成功失败的status。code=10000表示操作成功，status=SUCCESS表示转账成功
            //由于是同步响应，所以不需要对结果验签
            return $pay_res;
        }
     *
     */
    function alitransfer($transferData){
        Factory::setOptions($this->getAlipayOptions());

        $method = 'alipay.fund.trans.uni.transfer';
        //公共参数
        $textParams = [
            'app_id'=>$this->alipayConfig['app_id'],
            'charset'=>'utf-8',
            'sign_type'=>'RSA2',
            'timestamp'=>$transferData->timestamp,
            'version'=>'1.0',
        ];

        //请求参数
        //订单号前缀
        $biz_content = [
            'out_biz_no'=>$transferData->order_id,//商家支付订单号
            'trans_amount'=>$transferData->trans_amount,
            'product_code'=>'TRANS_ACCOUNT_NO_PWD',
            'biz_scene'=>'DIRECT_TRANSFER',
            'order_title'=>$transferData->order_title,
            'payee_info'=>[
                'identity'=>$transferData->phone,//手机号
                'identity_type'=>'ALIPAY_LOGON_ID',
                'name'=>$transferData->name,//参与方真实姓名，如果非空，将校验收款支付宝账号姓名一致性。当identity_type=ALIPAY_LOGON_ID时，本字段必填。
            ],
            'remark'=>$transferData->remark,
        ];
        $pay_res = Factory::util()->generic()->execute($method,$textParams,$biz_content);

        return $pay_res;
    }

}