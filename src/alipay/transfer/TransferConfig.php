<?php
/*
 * description：
 * author：wh
 * email：
 * createTime：{2021/09/24} {20:07} 
 */

namespace wanghua\general_utility_tools_php\alipay\transfer;


/**
 * 调用转账之前请配置此参数
 *
 * 配置示例：
 *
    $alipayConfig = \config('pay_config.alipay_transfer');//转账配置

    $trans_config = new TransferConfig();
    $trans_config->cert_path_alipayCertPublicKey_RSA2 = Tools::get_root_path().$alipayConfig['cert_path_alipayCertPublicKey_RSA2'];//'<-- 请填写您的支付宝公钥证书文件路径，例如：/foo/alipayCertPublicKey_RSA2.crt -->';
    $trans_config->cert_path_alipayRootCert = Tools::get_root_path().$alipayConfig['cert_path_alipayRootCert'];//'<-- 请填写您的支付宝根证书文件路径，例如：/foo/alipayRootCert.crt" -->';
    $trans_config->appCertPublicKey = Tools::get_root_path().$alipayConfig['appCertPublicKey'];//'<-- 请填写您的应用公钥证书文件路径，例如：/foo/appCertPublicKey_2019051064521003.crt -->'


    $order_prefix = 'trs';
    $order_id = Tools::to_create_order_no($order_prefix);
    $trans_config->order_id = $order_id;//订单号 必须
    $trans_config->timestamp = Tools::get_now_date();//请求时间 eg：2020-01-08 10:12:50
    $trans_config->trans_amount = '0.21';//转账金额 必须 且为字符串 最低0.1元 取值范围[0.1,100000000]
    $trans_config->order_title = '代理提现';//订单标题 可选
    $trans_config->phone = '18290416033';//支付宝登录手机号 必须
    $trans_config->name = '王华';//支付宝真实姓名 必须
    $trans_config->remark = '代理提现到支付宝';//支付备注 可选

    $trans = new AlipayTransfer();
    $pay_res = $trans->alitransfer($alipayConfig, $trans_config);

    dump($pay_res);
 *
 */
class TransferConfig
{
    //'<-- 请填写您的支付宝公钥证书文件路径，例如：/foo/alipayCertPublicKey_RSA2.crt -->';
    public $cert_path_alipayCertPublicKey_RSA2 = '';//必须
    //'<-- 请填写您的支付宝根证书文件路径，例如：/foo/alipayRootCert.crt" -->';
    public $cert_path_alipayRootCert = '';//必须
    //'<-- 请填写您的应用公钥证书文件路径，例如：/foo/appCertPublicKey_2019051064521003.crt -->'
    public $appCertPublicKey = '';//必须

    public $notifyUrl = '';//接收异步通知(可选)


    //业务参数
    public $order_id = '';//订单号 必须
    public $timestamp = '';//请求时间 必须 eg：2020-01-08 10:12:50
    public $trans_amount = '';//转账金额 必须 且为字符串 最低0.1元 取值范围[0.1,100000000]
    public $order_title = '';//订单标题 可选
    public $phone = '';//支付宝登录手机号 必须
    public $name = '';//支付宝真实姓名 必须
    public $remark = '';//支付备注 可选


}