<?php
/*
 * description：
 * author：wh
 * email：
 * createTime：{2021/08/24} {10:20} 
 */

namespace wanghua\general_utility_tools_php\alipay;


use app\index\model\AppOrderPayReqRecordModel;
use wanghua\general_utility_tools_php\tool\Tools;

/**
 * 调起支付宝支付
 *
 * Class AlipayLogic
 *
 * 依赖：
 * composer require wanghua/general-utility-tools-php dev-master
 * @deprecated
 */
class AlipayLogic
{

    /**
     * desc：调起支付
     * author：wh
     * @return false|mixed|\SimpleXMLElement|string|\提交表单HTML文本|null
     * @throws \think\Exception
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @throws \think\exception\PDOException
     */
    //function pay(string $notify_url,string $return_url=''){


        ///* *
        // * 功能：支付宝手机网站支付接口(alipay.trade.wap.pay)接口调试入口页面
        // * 版本：2.0
        // * 修改日期：2016-11-01
        // * 说明：
        // * 以下代码只是为了方便商户测试而提供的样例代码，商户可以根据自己网站的需要，按照技术文档编写,并非一定要使用该代码。
        // 请确保项目文件有可写权限，不然打印不了日志。
        // */
        //
        //header("Content-type: text/html; charset=utf-8");
        //
        //
        //require_once Tools::get_root_path().'library/AlipayTradeWapPay/wappay/service/AlipayTradeService.php';
        //require_once Tools::get_root_path().'library/AlipayTradeWapPay/wappay/buildermodel/AlipayTradeWapPayContentBuilder.php';
        ////require dirname ( __FILE__ ).DIRECTORY_SEPARATOR.'./../config.php';
        //$config = config('pay_config.alipay');
        //if(empty($_POST['WIDout_trade_no']) || trim($_POST['WIDout_trade_no'])=="") return null;
        ////商户订单号，商户网站订单系统中唯一订单号，必填
        //$out_trade_no = input('WIDout_trade_no');
        //if(empty($out_trade_no)){
        //    return '参数错误';
        //}
        //$order = AppOrderPayReqRecordModel::getOrder($out_trade_no);
        //if(empty($order)){
        //    return '订单错误';
        //}
        ////超时时间
        //$timeout_express="1m";
        //
        //$payRequestBuilder = new \AlipayTradeWapPayContentBuilder();
        //$payRequestBuilder->setBody($order['goods_name']);
        //$payRequestBuilder->setSubject($order['goods_name']);
        //$payRequestBuilder->setOutTradeNo($out_trade_no);
        //$payRequestBuilder->setTotalAmount($order['goods_price']);
        //$payRequestBuilder->setTimeExpress($timeout_express);
        //
        //$payResponse = new \AlipayTradeService($config);


        //$result = $payResponse->wapPay($payRequestBuilder,$return_url,$notify_url);
        //return $result;
    //}
}