<?php
/*
 * description：
 * author：wh
 * email：
 * createTime：{2024/1/16} {17:15} 
 */

namespace wanghua\general_utility_tools_php\fastadmin;


use app\common\model\TabConf;
use think\Db;

/**
 * 为fastadmin框架定制的功能
 *
 * Class FastadminTools
 * @package wanghua\general_utility_tools_php\fastadmin
 */
class FastadminTools
{

    /**
     * desc：线上需要自动隐藏的菜单
     *
     * 当你有这种需求：
     * 我的菜单只需要本地显示，线上或非本地环境不需要显示时，就可以使用此方法。
     * 为什么需要此方法？
     * 因为，我每次在本地增加功能并新增菜单之后，都需要把本地的菜单同步到线上，但是命令管理菜单，或者有些只需要开发者才能
     * 动的菜单，需要在线上隐藏。我又不想每次同步之后去改菜单表。
     * 手动去菜单表改也是可以，但是次数多了就很麻烦，有时候会忘记。
     *
     * 如：命令管理菜单、开发者相关配置
     *
     * author：wh
     * @param array $menu_arr 需要隐藏的菜单数组
     * @param string $sys_env
     * @return bool
     */
    static function auto_hidden_menu_online(array $menu_arr, string $sys_env){

        foreach ($menu_arr as $menu_name){
            if($sys_env == 'LOCAL'){
                //本地显示
                Db::table('fa_auth_rule')->where('name',$menu_name)->data(['status'=>'normal'])->update();
            }else{
                Db::table('fa_auth_rule')->where('name',$menu_name)->data(['status'=>'hidden'])->update();
            }
        }
        return true;
    }
}