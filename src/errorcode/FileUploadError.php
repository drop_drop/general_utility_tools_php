<?php
/*
 * description：
 * author：wh
 * email：
 * createTime：{2020/7/1} {18:48} 
 */

namespace wanghua\general_utility_tools_php\errorcode;


class FileUploadError
{
    //region 文件上传
    const UPLOAD_FAILED = [24000, '上传失败'];

    const UPLOAD_FILE_IS_EMPTY = [24100, '无文件上传'];

    const UPLOAD_FILE_SIZE_EMPTY = [24200, '文件内容为空'];

    const UPLOAD_FILE_SIZE_ERROR = [24300, '文件大小超过限制'];

    const UPLOAD_FILE_MIME_ERROR = [24400, '文件类型错误'];

    const UPLOAD_FILE_EXT_ERROR = [24500, '文件扩展名错误'];

    const FILE_SOURCE_ERROR = [24600, '文件来源错误'];
    //endregion
}