<?php
/*
 * description：
 * author：wh
 * email：
 * createTime：{2020/7/1} {19:05} 
 */

namespace wanghua\general_utility_tools_php\errorcode;


class AuthError
{

    const PERMISSION_DENIED = [60000, '权限拒绝'];

    const ACCESS_REFUSE_ERROR = [60100, '访问被拒绝'];//请求不合法资源

    const ACCESS_TIMES_ERROR = [60200, '访问次数限制'];
}