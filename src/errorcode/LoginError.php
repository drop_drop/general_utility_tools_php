<?php
/*
 * description：
 * author：wh
 * email：
 * createTime：{2020/7/1} {18:59} 
 */

namespace wanghua\general_utility_tools_php\errorcode;


class LoginError
{

    const OFF_LINE_ERROR = [30100, '未登录'];

    const LOGIN_SESSION_INVALID = [30200, '登录已过期，请重新登录'];

}