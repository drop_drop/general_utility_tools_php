<?php
/*
 * description：
 * author：wh
 * email：
 * createTime：{2019/11/12} {21:01} 
 */

namespace wanghua\general_utility_tools_php\errorcode;

/**
 * 系统级错误, 业务错误代码放在业务层，不要放在此处
 * Class SystemError
 * @package errorcode
 */
class Code
{
    //region 系统级别错误
    const SUCCESS = [200, 'SUCCESS'];

    const REQUEST_ERROR = [10000, '请求错误'];//一般url资源错误；url错误、敏感参数错误等

    const REQUEST_TYPE_ERROR = [10001, '请求类型错误'];//ajax get post

    const FAILED = [10002, '操作失败'];

    const SOURCE_NOT_FIND = [10003, '资源找不到'];

    const SYSTEM_ERROR = [500, '系统错误'];

    const PERMISSION_DENIED = [10005, '权限拒绝'];

    const ACCESS_REFUSE_ERROR = [10006, '访问被拒绝'];//请求不合法资源

    const ACCESS_TIMES_ERROR = [10007, '访问次数限制'];
    //endregion 系统级别错误



    //region 短信错误

    const MESSAGE_SEND_ERROR = [13500, '短信发送失败'];

    const MESSAGE_SEND_ULTRALIMIT = [13600, '短信发送超限'];

    const MESSAGE_VERIFY_FAILED = [13700, '短信验证失败'];

    const MESSAGE_NUM_LITTLE = [13800, '短信余额不足'];

    const MESSAGE_SEND_EXCEPTION = [13900, '短信发送异常'];
    //endregion

    //region 文件上传
    const UPLOAD_FAILED = [14000, '上传失败'];

    const UPLOAD_FILE_IS_EMPTY = [14000, '无文件上传'];

    const UPLOAD_FILE_SIZE_EMPTY = [14100, '文件内容为空'];

    const UPLOAD_FILE_SIZE_ERROR = [14200, '文件大小超过限制'];

    const UPLOAD_FILE_MIME_ERROR = [14300, '文件类型错误'];

    const UPLOAD_FILE_EXT_ERROR = [14500, '文件扩展名错误'];

    const FILE_FROM_ERROR = [14520, '来源错误'];
    //endregion


    //region 数据库错误
    const DATA_IS_EMPTY = [50000, '未获取到相关数据'];

    const UPDATE_ERROR = [50001, '修改异常'];

    const INSERT_ERROR = [50002, '数据保存异常'];

    const SELECT_ERROR = [50003, '查询异常'];

    const DATA_REPEAT_ERROR = [50005, '数据重复'];

    const DATA_INSERT_ERROR = [50006, '数据入库异常'];

    //endregion
}