<?php
/*
 * description：
 * author：wh
 * email：
 * createTime：{2020/7/1} {18:49} 
 */

namespace wanghua\general_utility_tools_php\errorcode;


class SmsError
{
    //region 短信错误

    const MESSAGE_SEND_ERROR = [43500, '短信发送失败'];

    const MESSAGE_SEND_ULTRALIMIT = [43600, '短信发送超限'];

    const MESSAGE_VERIFY_FAILED = [43700, '短信验证失败'];

    const MESSAGE_NUM_LITTLE = [43800, '短信余额不足'];

    const MESSAGE_SEND_EXCEPTION = [43900, '短信发送异常'];
    //endregion
}