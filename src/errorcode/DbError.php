<?php
/*
 * description：
 * author：wh
 * email：
 * createTime：{2020/7/1} {18:49} 
 */

namespace wanghua\general_utility_tools_php\errorcode;


class DbError
{


    //region 数据库错误
    const DATA_IS_EMPTY = [16000, '未获取到相关数据'];

    const UPDATE_ERROR = [16100, '修改异常'];

    const INSERT_ERROR = [16200, '数据保存异常'];

    const SELECT_ERROR = [16300, '查询异常'];

    const DATA_REPEAT_ERROR = [16400, '数据重复'];

    const DATA_INSERT_ERROR = [16500, '数据入库异常'];

    //endregion
}