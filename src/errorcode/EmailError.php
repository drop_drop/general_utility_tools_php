<?php
/*
 * description：
 * author：wh
 * email：
 * createTime：{2020/7/1} {18:47} 
 */

namespace wanghua\general_utility_tools_php\errorcode;


class EmailError
{
    //region 邮件错误
    const EMAIL_RECEIVER_IS_EMPTY = [23100, '未设置收信人'];

    const EMAIL_SEND_FAILED = [23200, '邮件发送失败'];
    //endregion
}