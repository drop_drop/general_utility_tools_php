<?php
/*
 * description：
 * author：wh
 * email：
 * createTime：{2020/7/3} {14:14} 
 */

namespace wanghua\general_utility_tools_php\errorcode;


class RequestError
{
    //region 请求错误
    const SUCCESS = [200, 'SUCCESS'];

    const REQUEST_TYPE_ERROR = [50601, '请求类型错误'];//ajax get post

    const SOURCE_NOT_FIND = [50400, '资源找不到'];

    const SYSTEM_ERROR = [50500, '系统错误'];

    const REQUEST_ERROR = [50600, '请求错误'];//一般url资源错误；url错误、敏感参数错误等

    const FAILED = [50602, '请求失败'];
    //endregion 请求错误
}