<?php
/*
 * description：
 * author：wh
 * email：
 * createTime：{2021/08/25} {15:48} 
 */

namespace wanghua\general_utility_tools_php\framework\base;


use wanghua\general_utility_tools_php\framework\BaseController;
use think\Request;

/**
 * common controller have no permission
 *
 * 外部公用架构
 *
 * 无权限校验的公用架构
 *
 * Class PublicController
 * @package app\index\controller
 */
class BasePublicController extends BaseController
{


}