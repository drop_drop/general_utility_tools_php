<?php
/*
 * description：
 * author：wh
 * email：
 * createTime：{2021/4/19} {11:14} 
 */

namespace wanghua\general_utility_tools_php\html;


/**
 * PHP操作html元素【不包含修改】
 * Class Html
 * @package wanghua\general_utility_tools_php\html
 */
class Html
{
    /**
     * desc：提取a标签
     * 返回标签和标签包含的内容
     * author：wh
     * @param $html
     * @return null
     */
    function getAElement(string $html){
        $a_array = null;
        $reg="/<a .*?>.*?<\/a>/";
        preg_match_all($reg,$html,$a_array);
        return $a_array;
    }

    /**
     * desc：提取div标签
     * author：wh
     * @param string $html
     * @return null
     */
    function getDiv(string $html){
        $a_array = null;
        $reg="/<div .*?>.*?<\/div>/";
        preg_match_all($reg,$html,$a_array);
        return $a_array;
    }
    /**
     * desc：提取img标签
     * author：wh
     * @param string $html
     * @return null
     */
    function getImg(string $html){
        $a_array = null;
        $reg="/<img .*?>.*?<\/img>/";
        preg_match_all($reg,$html,$a_array);
        return $a_array;
    }
    /**
     * desc：提取span标签
     * author：wh
     * @param string $html
     * @return null
     */
    function getSpan(string $html){
        $a_array = null;
        $reg="/<span .*?>.*?<\/span>/";
        preg_match_all($reg,$html,$a_array);
        return $a_array;
    }
    /**
     * desc：提取ul标签
     * author：wh
     * @param string $html
     * @return null
     */
    function getUl(string $html){
        $a_array = null;
        $reg="/<ul .*?>.*?<\/ul>/";
        preg_match_all($reg,$html,$a_array);
        return $a_array;
    }
    /**
     * desc：提取li标签
     * author：wh
     * @param string $html
     * @return null
     */
    function getLi(string $html){
        $a_array = null;
        $reg="/<li .*?>.*?<\/li>/";
        preg_match_all($reg,$html,$a_array);
        return $a_array;
    }
    /**
     * desc：提取table标签
     * author：wh
     * @param string $html
     * @return null
     */
    function getTable(string $html){
        $a_array = null;
        $reg="/<table .*?>.*?<\/table>/";
        preg_match_all($reg,$html,$a_array);
        return $a_array;
    }
    /**
     * desc：提取table tr标签
     * author：wh
     * @param string $html
     * @return null
     */
    function getTableTr(string $html){
        $a_array = null;
        $reg="/<tr .*?>.*?<\/tr>/";
        preg_match_all($reg,$html,$a_array);
        return $a_array;
    }
    /**
     * desc：提取table td标签
     * author：wh
     * @param string $html
     * @return null
     */
    function getTableTd(string $html){
        $a_array = null;
        $reg="/<td .*?>.*?<\/td>/";
        preg_match_all($reg,$html,$a_array);
        return $a_array;
    }


    /**
     * desc：提取a标签属性
     * author：wh
     * @param $html
     * @return array
     */
    function getAAttribute($html){
        $aarray = null;
        $reg1="/<a .*?>.*?<\/a>/";
        preg_match_all($reg1,$html,$aarray);

        $hrefarray=null;//这个存放的是匹配出来的href的链接地址
        $acontent=null;//存放匹配出来的a标签的内容
        $reg2="/href=\"([^\"]+)/";
        $a_arr = [];
        for($i=0;$i<count($aarray[0]);$i++){
            preg_match_all($reg2,$aarray[0][$i],$hrefarray);
            //这里输出的就是遍历出来的所有a标签的链接
            $a_arr['url'] = $hrefarray[1][0];

            //拿出《a》标签的内容
            $reg3="/>(.*)<\/a>/";
            preg_match_all($reg3,$aarray[0][$i],$acontent);
            //这里输出的就是a标签的文字了
            $a_arr['text'] = $acontent[1][0];
        }
        return $a_arr;
    }
    /**
     * desc：提取img标签src(提取图片链接)
     * author：wh
     * @param $html
     * @return array
     */
    function getImgAttribute($html){
        $aarray = null;
        $reg1="/<img .*?>.*?<\/img>/";
        preg_match_all($reg1,$html,$aarray);

        $hrefarray=null;//这个存放的是匹配出来的href的链接地址
        $acontent=null;//存放匹配出来的a标签的内容
        $reg2="/src=\"([^\"]+)/";
        $a_arr = [];
        for($i=0;$i<count($aarray[0]);$i++){
            preg_match_all($reg2,$aarray[0][$i],$hrefarray);
            //这里输出的就是遍历出来的所有a标签的链接
            $a_arr['url'] = $hrefarray[1][0];

            //拿出《a》标签的内容
            $reg3="/>(.*)<\/img>/";
            preg_match_all($reg3,$aarray[0][$i],$acontent);
            //这里输出的就是a标签的文字了
            $a_arr['text'] = $acontent[1][0];
        }
        return $a_arr;
    }

    /**
     * desc：删除字符串
     * author：wh
     */
    function clearHtml($html, $str){
        return str_replace($str,'',$html);
    }

    /**
     * desc：正则提取html元素的内容
     *
     * 用法eg：
     * $temp = '
    <div class="num">1</div>
    <div class="num">2</div>
    <div class="num">3</div>
    <div class="num">4</div>
    <div class="num1">3</div>
    <div class="num2">4</div>
    <div class="num">5</div>';
    $result = $this->get_tag_data($html,"div","class","num");
     *
     * author：wh
     * @param $html
     * @param $tag
     * @param $class
     * @param $value
     * @return mixed
     */
    function getTagData($html,$tag,$class,$value){
        //$value 为空，则获取class=$class的所有内容
        $regex = $value ? "/<$tag.*?$class=\"$value\".*?>(.*?)<\/$tag>/is" :  "/<$tag.*?$class=\".*?$value.*?\".*?>(.*?)<\/$tag>/is";
        preg_match_all($regex,$html,$matches,PREG_PATTERN_ORDER);
        //返回数组 ,指定标签内容
        return $matches[1];
    }
}