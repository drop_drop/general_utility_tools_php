<?php
/*
 * description：
 * author：wh
 * email：
 * createTime：{2022/11/8} {15:05} 
 */

namespace wanghua\general_utility_tools_php\html;


use wanghua\general_utility_tools_php\tool\Tools;

class Htmlcontent
{

    /**
     * [curlHtml 获取页面信息]
     * 使用curl获取html页面内容
     * @param  [type] $url [网址]
     * @return [type]      [description]
     */

    function curlHtml($url)
    {
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL            => "{$url}",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING       => "",
            CURLOPT_MAXREDIRS      => 10,
            CURLOPT_TIMEOUT        => 30,
            CURLOPT_HTTP_VERSION   => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST  => "GET",
            CURLOPT_HTTPHEADER     => array(
                "Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8",
                "Accept-Encoding: gzip, deflate, br",
                "Accept-Language: zh-CN,zh;q=0.9",
                "Cache-Control: no-cache",
                "Connection: keep-alive",
                "Pragma: no-cache",
                "Upgrade-Insecure-Requests: 1",
                "User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.77 Safari/537.36",
                "cache-control: no-cache"
            ),
        ));

        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
        $response = curl_exec($curl);
        $err = curl_error($curl);
        curl_close($curl);
        if ($err) return false;
        else return $response;
    }

    /**
     * [get_tag_data 使用正则获取html内容中指定的内容]
     * @param  [type] $html  [爬取的页面内容]
     * @param  [type] $tag   [要查找的标签]
     * @param  [type] $attr  [要查找的属性名]
     * @param  [type] $value [属性名对应的值]
     * @return [type]        [description]
     */
    function get_tag_data($html, $tag, $attr, $value)
    {

        // var_dump($tag, $attr, $value);
        $regex = "/<$tag.*?$attr=\".*?$value.*?\".*?>(.*?)<\/$tag>/is";
        // var_dump($regex);
        preg_match_all($regex, $html, $matches, PREG_PATTERN_ORDER);
        //var_dump($matches);die;
        $data = isset($matches[1]) ? $matches[1] : '';
        // var_dump($data);die;
        return $data;
    }
    /**
     * [get_html_data 使用xpath对获取到的html内容进行处理]
     * @param  [type]  $html [爬取的页面内容]
     * @param  [type]  $path [Xpath语句]
     * @param  integer $tag  [类型 0内容 1标签内容 自定义标签]
     * @param  boolean $type [单个 还是多个（默认单个时输出单个）]
     * @return [type]        [description]
     */

    function get_html_data($html, $path, $tag = 1, $type = true)
    {
        $dom = new \DOMDocument();
        @$dom->loadHTML("<?xml encoding='UTF-8'>" . $html); // 从一个字符串加载HTML并设置UTF8编码
        $dom->normalize(); // 使该HTML规范化
        $xpath = new \DOMXPath($dom); //用DOMXpath加载DOM，用于查询
        $contents = $xpath->query($path); // 获取所有内容
        $data = [];
        foreach ($contents as $value) {
            if ($tag == 1) {
                $data[] = $value->nodeValue; // 获取不带标签内容
            } elseif ($tag == 2) {
                $data[] = $dom->saveHtml($value);  // 获取带标签内容
            } else {
                $data[] = $value->attributes->getNamedItem($tag)->nodeValue; // 获取attr内容
            }
        }
        if (count($data) == 1) {
            $data = $data[0];
        }
        return $data;
    }
    //调用
    public function get_content($url,$tag,$attr='',$value='')
    {
        //$url = input('url') ?? '';
        //$tag = g('tag') ?? '';
        //$attr = g('attr') ?? '';
        //$value = g('value') ?? '';
        if (empty($url) || empty($tag)) {
            return Tools::set_res(4, '缺少关键参数!');
        }

        $html = $this->curlHtml($url);
        //使用正则获取
        $data = $this->get_tag_data($html, $tag, $attr, $value);
        //Xpath方法，暂未搞明白
        // $data = $this->get_html_data($html, $path, $tag = 1, $type = true);
        return Tools::set_res(1, 'ok!', $data);
    }
}