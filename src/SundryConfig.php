<?php
/*
 * description：
 * author：wh
 * email：
 * createTime：{2023/12/31} {21:56} 
 */

namespace wanghua\general_utility_tools_php;


use app\common\model\TabConf;
use think\Db;

/**
 * 系统杂项配置
 *
 * 读取fa_sundry_config表中的配置
 *
 * Class SundryConfig
 * @package wanghua\general_utility_tools_php
 */
class SundryConfig
{
    private static $tablename = 'fa_zc_sundry_config';

    /**
     * desc：获取配置的值，设置配置的值
     *
     * 默认缓存值
     *
     * author：wh
     * @param string $key
     * @param string $val
     * @return float|mixed|string
     */
    static function val(string $key,string $val=''){
        $tabname = self::$tablename;
        if($val){
            $obj = Db::table($tabname);
            $obj->where('key',$key);
            $obj->data(['val'=>$val]);
            return $obj->update();
        }
        $obj = Db::table($tabname);
        return $obj->where(['key'=>$key])
            ->cache($tabname.'_'.$key,0,$tabname)
            ->value('val');
    }
}