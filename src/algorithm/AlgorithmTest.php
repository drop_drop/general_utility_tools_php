<?php
/*
 * description：
 * author：wh
 * email：
 * createTime：{2024/7/27} {17:36} 
 */

namespace wanghua\general_utility_tools_php\algorithm;


/**
 * 算法测试
 * Class AlgorithmTest
 * @package wanghua\general_utility_tools_php\algorithm
 */
class AlgorithmTest
{


    /**
     * desc：测试权重随机算法概率
     * author：wh
     * @param array $weights 权重数组 eg：[1, 9, 30, 70]
     */
    static function testWeightRandom(array $weights){
        echo json_encode($weights)."\n";

        $trials = 1000; // 试验次数
        $selectionCounts = array_fill(0, count($weights), 0); // 用于存储每个索引的选择次数

        for ($i = 0; $i < $trials; $i++) {

            $index = Algorithm::weightRandom($weights);
            $selectionCounts[$index]++; // 记录选择次数
        }

        // 计算每个索引的选择概率
        $probabilities = [];
        foreach ($selectionCounts as $index => $count) {
            $probabilities[$index] = ($count / $trials) * 100; // 转换为百分比
        }

        // 输出结果
        echo "After {$trials} trials, the probabilities of selecting each index are:\n";
        foreach ($probabilities as $index => $probability) {
            echo "Index {$index}: {$probability}%\n";
        }
    }


}