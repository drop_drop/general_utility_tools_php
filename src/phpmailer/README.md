### 使用PHP发送邮件

#### 依赖composer包：wanghua/general-utility-tools-php
#### 环境要求：PHP7+, ThinkPHP5+

示例：

步骤1：填写配置=>config
    
    说明：根据需要读取数据库或文件配置
  
步骤2，调用：
    
```
//原生方式（复制可用）
Logger::send('test title','test body');
```
    
```
//调用封装类（复制可用）
Logger::open_debug();//打开调试模式
Logger::log(['error'=>'系统错误'],'test_log');//调用日志写入  
```

#### 封装类：
```

/**
（复制可用）
 * Class Logger
 * @package app\common\tools
 */
class Logger
{

    /**
     * desc：开启调试模式
     * author：wh
     */
    static function open_debug(){
        LoggerObj::open_debug();
    }

    /**
     * desc：关闭调试模式
     * author：wh
     */
    static function close_debug(){
        LoggerObj::close_debug();
    }
    /**
     * desc：系统未来通用统一日志记录
     * author：wh
     * @param array $data
     * @param string $file_log_name
     */
    static function log(array $data=[], string $file_log_name){
        //服务器配置 start
        $send_server_nickname = SundryConfigModel::getConfigVal('send_server_username');
        $send_server_username = SundryConfigModel::getConfigVal('send_server_username');
        $send_server_pwd = SundryConfigModel::getConfigVal('send_server_pwd');
        $send_server_host = SundryConfigModel::getConfigVal('send_server_host');
        //服务器配置 end
        
        //收件人信息 start
        $receiver = SundryConfigModel::getConfigVal('admin_error_log_email');
        $receiver_nickname = '日志管理员';//默认收件人昵称
        //收件人信息 end
        
        //写入日志
        LoggerObj::set_email_config($send_server_nickname,$send_server_username,$send_server_pwd,$send_server_host);
        LoggerObj::set_receiver_config($receiver,$receiver_nickname);
        LoggerObj::write_text($data,$file_log_name);
    }
}
```

#### 原生方式：
```
class Logger{
    //（复制可用）
    static function send($title, $body){
        $host = 'smtp.qq.com';//qq邮件发送服务器
        $nickname = 'test风控';//你的邮件发送服务器昵称
        $username = '1003076666@qq.com';//你的邮件发送服务器账号
        $userpass = 'pdosdyjuurowbcfc';//你的邮件发送服务器密码
        //邮件通知
        $mail = new Mail($nickname, $host, $username, $userpass);
        $mail->debug = false;//true 测试模式
        $agent = Db::table('fa_agent')->where(['id'=>1])->find();//查询数据
    
        return $mail->send($agent['email'], $agent['nickname'], $title,$body);
    }
}
```