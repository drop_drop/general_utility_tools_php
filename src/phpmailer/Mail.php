<?php

namespace wanghua\general_utility_tools_php\phpmailer;


/**
 * 邮件模型
 */
class Mail
{
    public $debug = 2;//调试模式

    protected $nickname = '';

    protected $host = '';

    protected $username = '';

    protected $pwd = '';

    protected $port = 465;//qq邮箱服务器端口 根据邮箱服务器定

    protected $protocol = 'ssl';//协议 根据邮箱服务器定

    protected $sender_email = '';//发送方邮箱

    public function __construct(string $nickname, string $host, string $username, string $pwd,$sender_email='',$port=465,$protocol='ssl')
    {
        $this->nickname = $nickname;//发送方昵称
        $this->host = $host;//发送方邮件服务器地址
        $this->username = $username;//发送方邮件服务器账号
        $this->pwd = $pwd;//发送方邮件服务器密码
        $this->sender_email = $sender_email;//发送方邮箱
        $this->port = $port;//发送方邮件服务器端口 有465, 25 2465等
        $this->protocol = $protocol;//发送方邮件服务器要求的协议 有ssl、tls
    }

    /**
     * @deprecated [老项目-发送国内邮箱]
     *
     * 注意：发送人和发送人邮箱一致才能用此方法，否则发送失败
     * @Author
     * @Date   2019-11-22
     * @param  [array|string]        $receiver      [收件人邮箱]
     * @param  [array|string]     	 $receiver_nick [收件人名称]
     * @param  [string]     		 $subject       [邮件主题]
     * @param  [string]     		 $body          [邮件正文]
     * @return [type]       		              [description]
     */
    function send($receiver,$receiver_nick,$subject,$body){
        if(empty($receiver)){
            return ['code'=>500, 'msg'=>'收件人邮箱为空'];
        }

        $email_nickname = $this->nickname;//平台邮箱名称

        $mail = new PHPMailer();
        $mail->isSMTP();
        if($this->debug){
            $mail->SMTPDebug = $this->debug;    // 调试模式
        }
        $mail->Host = $this->host;				// 邮件服务器地址
        $mail->SMTPAuth = true;					// 允许 SMTP 认证
        $mail->CharSet = "UTF-8";				// 编码格式
        $mail->Username = $this->username;		// SMTP 用户名  即邮箱的用户名
        $mail->Password = $this->pwd;			// SMTP 密码
        $mail->SMTPSecure = $this->protocol;    // TLS或者ssl协议
        $mail->Port = $this->port;                      // 服务器端口 25 或者465 具体要看邮箱服务器支持
        $mail->setFrom($this->username, $email_nickname);
        //dump($mail);die;
        if(is_array($receiver)){
            //多个收件人
            foreach ($receiver as $key => $val){
                $mail->addAddress($val, $receiver_nick[$key]);
            }
        }else{
            //单个收件人
            $mail->addAddress($receiver, $receiver_nick);
        }

        $mail->isHTML(true);        //是否以HTML文档格式发送
        $mail->Subject = $subject;
        $mail->Body = $body; //发送带有html标签文本
        // $mail->AltBody = ''; //如果邮件客户端不支持HTML则显示此内容
        // $mail->addCC(); // 添加抄送
        // $mail->addBCC(); // 添加密送
        // $mail->addAttachment("2e3f19fd-8e3f-405b-9854-41dd19ddebf8.jpg"); // 添加附件

        if(!$mail->send()){
            return ['code'=>500, 'msg'=>$mail->ErrorInfo];
        }
        return ['code'=>200, 'msg'=>'Email has been sent.'];
    }


    /**
     * [通用发送国内邮箱]
     *
     * 注意：发送人和发送人邮箱一致才能用此方法，否则发送失败
     * @Author
     * @Date   2019-11-22
     * @param  [array|string]        $receiver      [收件人邮箱]
     * @param  [array|string]     	 $receiver_nick [收件人名称]
     * @param  [string]     		 $subject       [邮件主题]
     * @param  [string]     		 $body          [邮件正文]
     * @return [type]       		              [description]
     */
    function sendInner($receiver,$receiver_nick,$subject,$body){
        if(empty($receiver)){
            return ['code'=>500, 'msg'=>'收件人邮箱为空'];
        }

        $email_nickname = $this->nickname;//平台邮箱名称

        $mail = new PHPMailer();
        $mail->isSMTP();
        if($this->debug){
            $mail->SMTPDebug = $this->debug;    // 调试模式
        }
        $mail->Host = $this->host;				// 邮件服务器地址
        $mail->SMTPAuth = true;					// 允许 SMTP 认证
        $mail->CharSet = "UTF-8";				// 编码格式
        $mail->Username = $this->username;		// SMTP 用户名  即邮箱的用户名
        $mail->Password = $this->pwd;			// SMTP 密码
        $mail->SMTPSecure = $this->protocol;    // TLS或者ssl协议
        $mail->Port = $this->port;                      // 服务器端口 25 或者465 具体要看邮箱服务器支持
        $mail->setFrom($this->sender_email, $email_nickname);
        if(is_array($receiver)){
            //多个收件人
            foreach ($receiver as $key => $val){
                $mail->addAddress($val, $receiver_nick[$key]);
            }
        }else{
            //单个收件人
            $mail->addAddress($receiver, $receiver_nick);
        }

        $mail->isHTML(true);        //是否以HTML文档格式发送
        $mail->Subject = $subject;
        $mail->Body = $body; //发送带有html标签文本
        // $mail->AltBody = ''; //如果邮件客户端不支持HTML则显示此内容
        // $mail->addCC(); // 添加抄送
        // $mail->addBCC(); // 添加密送
        // $mail->addAttachment("2e3f19fd-8e3f-405b-9854-41dd19ddebf8.jpg"); // 添加附件

        if(!$mail->send()){
            return ['code'=>500, 'msg'=>$mail->ErrorInfo];
        }
        return ['code'=>200, 'msg'=>'Email has been sent.'];
    }

    /**
     * [发送【国外】aws亚马逊邮件]
     *
     * 邮件服务器用亚马逊服务器，接收者邮箱不限制
     *
     * 注意：
     * 发送方要在亚马逊控制台，添加发送者邮箱，才可以对外发邮件
     * 端口用25，25测试通过
     *
     * @Author
     * @Date   2019-11-22
     * @param  [array|string]        $sender        [发件人邮箱]
     * @param  [array|string]        $receiver      [收件人邮箱]
     * @param  [array|string]     	 $receiver_nick [收件人名称]
     * @param  [string]     		 $subject       [邮件主题]
     * @param  [string]     		 $body          [邮件正文]
     * @return [type]       		              [description]
     */
    function sendAWS($receiver,$receiver_nick,$subject,$body){
        if(empty($receiver)){
            return ['code'=>500, 'msg'=>'收件人邮箱为空'];
        }
        if(empty($this->sender_email)){
            return ['code'=>500, 'msg'=>'发件人邮箱为空'];
        }

        $email_nickname = $this->nickname;//平台邮箱名称

        $mail = new PHPMailer();
        $mail->isSMTP();
        if($this->debug){
            $mail->SMTPDebug = $this->debug;    // 调试模式
        }
        //$mail->setFrom($this->sender_email, $email_nickname);

        $mail->Host = $this->host;				// 邮件服务器地址
        $mail->SMTPAuth = true;					// 允许 SMTP 认证
        $mail->CharSet = "UTF-8";				// 编码格式
        $mail->Username = $this->username;		// SMTP 用户名  即邮箱的用户名
        $mail->Password = $this->pwd;			// SMTP 密码
        $mail->SMTPSecure = $this->protocol;    // TLS或者ssl协议
        $mail->Port = $this->port;              // 服务器端口 腾讯465，具体要看邮箱服务器支持 亚马逊25 或 2465
        $mail->setFrom($this->sender_email, $email_nickname);

        if(is_array($receiver)){
            //多个收件人
            foreach ($receiver as $key => $val){
                $mail->addAddress($val, $receiver_nick[$key]);
            }
        }else{
            //单个收件人
            $mail->addAddress($receiver, $receiver_nick);
        }

        $mail->isHTML(true);        //是否以HTML文档格式发送
        $mail->Subject = $subject;
        $mail->Body = $body; //发送带有html标签文本
        // $mail->AltBody = ''; //如果邮件客户端不支持HTML则显示此内容
        // $mail->addCC(); // 添加抄送
        // $mail->addBCC(); // 添加密送
        // $mail->addAttachment("2e3f19fd-8e3f-405b-9854-41dd19ddebf8.jpg"); // 添加附件

        if(!$mail->send()){
            return ['code'=>500, 'msg'=>$mail->ErrorInfo];
        }
        return ['code'=>200, 'msg'=>'Email has been sent.'];
    }

}