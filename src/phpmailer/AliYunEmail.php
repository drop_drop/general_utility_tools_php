<?php

// This file is auto-generated, don't edit it. Thanks.

namespace wanghua\general_utility_tools_php\phpmailer;

use AlibabaCloud\SDK\Dm\V20151123\Dm;
use \Exception;
use AlibabaCloud\Tea\Exception\TeaError;
use AlibabaCloud\Tea\Utils\Utils;

use Darabonba\OpenApi\Models\Config;
use AlibabaCloud\SDK\Dm\V20151123\Models\SingleSendMailRequest;
use AlibabaCloud\Tea\Utils\Utils\RuntimeOptions;
use wanghua\general_utility_tools_php\Mmodel;
use wanghua\general_utility_tools_php\tool\Tools;


/**
 * 依赖：composer require alibabacloud/dm-20151123 1.2.0
 *
 * 邮件服务配置地址：
 * https://dm.console.aliyun.com/?spm=5176.22565350.0.0.44ce778b61qU0v#/directmail/MailAddress/cn-hangzhou
 *
 * Class AliYunEmail
 * @package app\api\controller
 */
class AliYunEmail {

    /**
     * 配置
     * accessKeyId 阿里云控制台获取
     * accessKeySecret 阿里云控制台获取
     *
     * accountName 发信地址，在这里界面添加：https://dm.console.aliyun.com/?spm=5176.22565350.0.0.44ce778b61qU0v#/directmail/MailAddress/cn-hangzhou
     */
    private $config = [];
    public function __construct($config)
    {
        $this->config = $config;
        if(empty($this->config['AccessKeyId']) || empty($this->config['AccessKeySecret'])){
            throw new Exception('请配置accessKeyId和accessKeySecret');
        }
        if(empty($this->config['accountName'])){
            throw new Exception('请配置发信地址accountName');
        }
        if(empty($this->config['fromAlias'])){
            throw new Exception('请配置发信人昵称fromAlias');
        }
    }


    /**
     * desc：发送邮件
     * author：wh
     * @param string $toAddress 收件人
     * @param string $subject 邮件标题
     * @param string $content 邮件内容(支持Html)
     */
    public function sendMail(string $toAddress,string $subject,string $content=''){
        return Mmodel::catchJson(function () use ($toAddress,$subject,$content){
            $client = self::createClient();
            $conf = [
                "accountName" => $this->config['accountName'],//"libin@mail.excn.vip",
                "addressType" => isset($this->config['addressType'])?$this->config['addressType']:1,//默认批量发送
                "replyToAddress" => isset($this->config['replyToAddress'])?$this->config['replyToAddress']:false,//默认不接收回复
                "fromAlias" => $this->config['fromAlias'],//"test发信人昵称"
                "tagName" => "test_tag",
                "toAddress" => $toAddress,//"382379437@qq.com",//多个逗号隔开
                "subject" => $subject,//"test主题",
                "htmlBody" => $content,//"test HtmlBody html 邮件正文",
                "textBody" => "邮件正文邮件正文邮件正文邮件正文test TextBody  txt邮件正文",
            ];
            //dump($conf);die;
            $singleSendMailRequest = new SingleSendMailRequest($conf);
            $runtime = new RuntimeOptions([]);
            // 复制代码运行请自行打印 API 的返回值
            $res = $client->singleSendMailWithOptions($singleSendMailRequest, $runtime);
            //有可能有延迟，最高的有20分钟延迟
            if($res->statusCode == 200){
                return Tools::set_ok();
            }
            return Tools::set_fail('邮件发送失败.');
        });
    }

    /**
     * 使用AK&SK初始化账号Client
     * @return Dm Client
     */
    private function createClient(){
        // 工程代码泄露可能会导致 AccessKey 泄露，并威胁账号下所有资源的安全性。以下代码示例仅供参考。
        // 建议使用更安全的 STS 方式，更多鉴权访问方式请参见：https://help.aliyun.com/document_detail/311677.html。
        $conf = [
            // 必填，请确保代码运行环境设置了环境变量 ALIBABA_CLOUD_ACCESS_KEY_ID。
            "accessKeyId" => $this->config['AccessKeyId'],//config('aliyun_oss_config.AccessKeyId'),
            // 必填，请确保代码运行环境设置了环境变量 ALIBABA_CLOUD_ACCESS_KEY_SECRET。
            "accessKeySecret" => $this->config['AccessKeySecret'],//config('aliyun_oss_config.AccessKeySecret'),
            //关闭证书验证
            "securityToken" => isset($this->config['securityToken'])?$this->config['securityToken']:"false",//默认
            // 访问的 debug 模式
            "debug" => isset($this->config['debug'])?$this->config['debug']:true,//默认为调试模式
            // 默认使用 HTTP 协议
            "protocol" => isset($this->config['protocol'])?$this->config['protocol']:"HTTP"//默认http
        ];
        //dump($conf);die;
        $config = new Config($conf);
        // Endpoint 请参考 https://api.aliyun.com/product/Dm
        $config->endpoint = "dm.aliyuncs.com";//默认
        return new Dm($config);
    }
}