<?php
/*
 * description：
 * author：wh
 * email：
 * createTime：{2024/5/1} {21:28} 
 */

namespace wanghua\general_utility_tools_php;


use think\Db;
use wanghua\general_utility_tools_php\tool\Tools;

/**
 * thinkphp5模型快捷方法库
 * Class Mmodel
 * @package wanghua\general_utility_tools_php
 */
class Mmodel
{
    /**
     * desc：存在则更新否则插入
     * author：wh
     * @param string $table 表名
     * @param array $where 更新条件（只在更新时有效）
     * @param array $data 更新或写入数据
     * @return int 数据主键id
     */
    static function existsUpdateInsert($table,$where,$data){
        if(empty($where)){
            return Db::table($table)->insertGetId($data);
        }
        $res = Db::table($table)
            ->where($where)
            ->find();
        if($res){
            //不修改条件数据
            $data = array_diff_key($data,$where);//从data中删除where数组中的键值对

            Db::table($table)
                ->where($where)
                ->data($data)
                ->update();
            return $res['id'];
        }
        return Db::table($table)->insertGetId($data);
    }


    /**
     * desc：捕获式函数块
     *
     * 无事务处理
     *
     * author：wh
     * @param $fn
     */
    static function catch($fn){
        try{
            //不输出日志，此代码块可能用于内部逻辑
            //Tools::log_to_write_txt(['input'=>input()]);
            $res = $fn();
            //Tools::log_to_write_txt(['output'=>$res]);
            return $res;
        }catch (\Exception $e){
            Tools::error_txt_log($e);
            return Tools::set_fail('操作失败.',$e->getMessage());
        }
    }
    /**
     * desc：捕获式函数块
     *
     * 无事务处理
     *
     * author：wh
     * @param $fn
     */
    static function catchJson($fn){
        try{
            Tools::log_to_write_txt(['input'=>input()]);
            $res = $fn();
            Tools::log_to_write_txt(['output'=>$res]);
            return json($res);
        }catch (\Exception $e){
            Tools::error_txt_log($e);
            return json(Tools::set_fail('操作失败.',$e->getMessage()));
        }
    }
    /**
     * desc：捕获式函数块
     *
     * 自动事务处理
     *
     * author：wh
     * @param $fn
     */
    static function catchTrans($fn){
        Db::startTrans();
        try{
            Tools::log_to_write_txt(['input'=>input()]);
            $res = $fn();
            Tools::log_to_write_txt(['output'=>$res]);
            Db::commit();
            return $res;
        }catch (\Exception $e){
            Db::rollback();
            Tools::error_txt_log($e);
            return Tools::set_fail('操作失败.',$e->getMessage());
        }
    }
    /**
     * desc：捕获式函数块
     *
     * 自动事务处理
     *
     * author：wh
     * @param $fn
     */
    static function catchTransJson($fn){
        Db::startTrans();
        try{
            Tools::log_to_write_txt(['input'=>input()]);
            $res = $fn();
            Tools::log_to_write_txt(['output'=>$res]);
            Db::commit();
            return json($res);
        }catch (\Exception $e){
            Db::rollback();
            Tools::error_txt_log($e);
            return json(Tools::set_fail('操作失败.',$e->getMessage()));
        }
    }
}