<?php
/*
 * description：
 * author：wh
 * email：
 * createTime：{2020/11/14} {10:04} 
 */

namespace wanghua\general_utility_tools_php\exception\api;


use wanghua\general_utility_tools_php\exception\BaseException;

class ApiException extends BaseException
{
    const API_RESPONSE_FORMAT_ERROR='The api response result is incorrectly formatted: code is not exist.';


    const EMPTY_PARAM_ERROR     ='param is empty.';
    const EMPTY_URL_ERROR       ='url of param is empty.';
}