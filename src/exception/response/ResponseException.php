<?php
/*
 * description：
 * author：wh
 * email：
 * createTime：{2020/11/14} {10:48} 
 */

namespace wanghua\general_utility_tools_php\exception\response;



use wanghua\general_utility_tools_php\exception\BaseException;

class ResponseException extends BaseException
{
    const RESPONSE_FORMAT_ERROR='The response result is incorrectly formatted.';

}