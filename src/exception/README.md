# 系统业务级异常类
1. api异常
2. 请求异常
3. api异常
4. 响应异常
5. 待扩展......



##使用说明
例1：
    
    if(empty($params)){
        //参数错误，抛出空参数异常
        throw new RequestException(RequestException::EMPTY_PARAM_ERROR);
    }
    
    


例2：

    //假设返回结果格式为：['code'=>200, 'msg'=>'操作成功', 'data'=>[]]
    if(is_null($result['code'])){
       //无code字段，抛出响应格式错误
       throw new ResponseException(ResponseException::RESPONSE_FORMAT_ERROR);
    }