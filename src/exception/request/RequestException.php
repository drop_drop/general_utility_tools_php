<?php
/*
 * description：
 * author：wh
 * email：
 * createTime：{2020/11/14} {10:13} 
 */

namespace wanghua\general_utility_tools_php\exception\request;


use wanghua\general_utility_tools_php\exception\BaseException;

class RequestException extends BaseException
{
    const EMPTY_PARAM_ERROR     ='param is empty.';
    const EMPTY_URL_ERROR       ='url of param is empty.';
}