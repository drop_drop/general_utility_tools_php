<?php
/*
 * description：
 * author：wh
 * email：
 * createTime：{2024/1/17} {13:07} 
 */

namespace wanghua\general_utility_tools_php\image;


use wanghua\general_utility_tools_php\tool\Tools;

class Image
{

    /**
     * desc：将二进制字符串转为PNG图片
     *
     * 应用场景：当你的图片来源是buffer二进制时
     *
     * 实战场景：微信小程序获取小程序码，调用获取微信小程序码接口时，微信接口返回的是图片Buffer二进制，
     * 这时候需要前端转换为图片，有时候与你和做的前端技术需要你转换为图片，再把图片路径传给他直接使用
     *
     * author：wh
     * @param $binary 图片的Buffer二进制数据
     * @param $img_save_path 系统保存路径（传public网站根目录下面的路径）
     * @param $filename 要保存的文件名称，必须带后缀名
     */
    static function binaryToImage($binary,$img_save_path,$filename){
        // 获取二进制数据（这里假设从文件或其他地方获得）
        //$binaryData = file_get_contents($binary); // 替换成真正的路径

        // 创建图像资源
        $imageResource = imagecreatefromstring($binary);

        $img_save_path = Tools::get_root_path().'public/'.$img_save_path;
        if(!file_exists($img_save_path)){
            mkdir($img_save_path,0777,true);
        }
        // 保存图像到指定位置
        imagepng($imageResource, $img_save_path.$filename); // 替换成想要保存的路径及名称

        // 释放内存并关闭图像资源
        imagedestroy($imageResource);
    }


    /**
     * desc：从富文本（任意文本）中提取图片链接
     *
     * 使用场景
     * 在需要的任意场景调用
     *
     * author：wh
     */
    static function extractImageUrls(string $html){
//        $html = <<<HTML
//<p>这是一张图片：<a href="http://example.com/images/photo1.jpg" target="_blank">点击查看</a></p>
//<p>还有一张：<img src="http://example.com/images/photo2.jpg" alt="Another Photo"></p>
//<p>图片链接也可以是：<img src="https://example.com/images/photo3.png"></p>
//HTML;
        // 使用正则表达式匹配图片链接
        $pattern = '/(https?:\/\/[^\s]+\.(?:jpg|jpeg|png|gif))/i';
        $images = [];
        if (preg_match_all($pattern, $html, $matches)) {
            //echo "找到的图片链接如下：\n";
            foreach ($matches[0] as $match) {
                $images[] = $match;
            }
        } else {
            return $images;
        }
        return $images;
    }
}