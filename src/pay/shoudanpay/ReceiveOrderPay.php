<?php
/*
 * description：
 * author：wh
 * email：
 * createTime：{2021/2/23} {10:46} 
 */

namespace wanghua\general_utility_tools_php\pay\shoudanpay;

use wanghua\general_utility_tools_php\tool\Tools;

/**
 * 收单宝提现
 * Class ReceiveOrderPay
 * @package librarie\pay\shoudanpay
 */
class ReceiveOrderPay extends BasePay
{

    /**
     * desc：设置请求数据
     * author：wh
     */
    function setData(){
        $this->post_params['shop_no'] = $this->shop_no;
        $this->post_params['accountNo'] = $this->accountNo;
        $this->post_params['run_type'] = $this->run_type;

        $this->post_params['nonce_str'] = $this->nonce_str;
        $this->post_params['payType'] = $this->payType;
        $this->post_params['realUserName'] = $this->realUserName;
        $this->post_params['bankNo'] = $this->bankNo;
        $this->post_params['requestNo'] = $this->requestNo;
        $this->post_params['amount'] = $this->amount;
        $this->post_params['sign'] = $this->sign;

        if($this->bankCode) $this->post_params['bankCode'] = $this->bankCode;
    }
    /**
     * desc：调起支付
     * author：wh
     */
    function pay(){
        set_time_limit($this->request_time_out);//脚本超时时间


        return Tools::curl_post($this->url, json_encode($this->post_params));
    }


}