<?php
/*
 * description：
 * author：wh
 * email：
 * createTime：{2021/2/23} {10:48} 
 */

namespace wanghua\general_utility_tools_php\pay\shoudanpay;


use wanghua\general_utility_tools_php\tool\Tools;

class BasePay
{
    public $post_params = [];
    public $request_time_out = 5;//请求支付网关脚本超时时间

    public $url = 'https://www.darlingpage.com/settleapi/paytobank/transfer.do';
    public $appKey = '';
    public $shop_no = '';//店铺编号
    public $accountNo = '';//付款账户编号
    public $run_type = '';//运行环境 sandbox 沙箱环境（开发、测试时用）, product 正式环境 （上线后用）

    public $nonce_str = '';//随机数
    public $payType = '';//wechat_bank 表示微信付款 到银行卡， wechat 表示付 款到微信钱包， wechat_readpack 表示付款到微信红包， alipay 表示支付宝付款到支付宝账户
    public $realUserName = '';//收款人姓名
    public $bankNo = '';//收款银行卡号，微信 openid, 或 支 付宝账号
    public $requestNo = '';//转账申请编号
    public $amount = 0;//付款金额,单位：元
    public $sign = '';
    public $bankCode = '';//微信付款到银行卡时必填，收款银行代码


    /**
     * desc：设置随机数
     * author：wh
     */
    function setNonceStr(){
        $this->nonce_str = Tools::rand_str(16);
    }

    /**
     * desc：设置支付类型
     * author：wh
     * @param $payType
     * @throws \Exception
     */
    function setPayType($payType){
        if(!in_array($payType,[
            'wechat_bank',
            'wechat',
            'wechat_readpack',
            'alipay',
        ])) throw new \Exception('支付类型错误');
        //wechat_bank 表示微信付款 到银行卡， wechat 表示付 款到微信钱包， wechat_readpack 表示付款到微信红包， alipay 表示支付宝付款到支付宝账户
        $this->payType = $payType;
    }


    /**
     * desc：设置订单编号
     * author：wh
     */
    function setRequestNo(){
        $this->requestNo = 'sdb'.date('YmdHis').Tools::rand_str(6);
    }

    /**
     * desc：设置签名
     * author：wh
     */
    function setSign(){
        $this->sign = md5($this->appKey.$this->run_type.$this->shop_no.$this->nonce_str);
    }


}