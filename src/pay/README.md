#### 支付类库
##### 收单宝
##### 通联 - 收银宝
##### ......



##### 1、收单宝
示例：
```
    $appKey = '123134sdf6sd4s6';
    $shop_no = 's46dfs64f';
    $accountNo = "sd1f2s4g3s4";//付款账户编号
    $run_type = 'sandbox';//运行环境 sandbox 沙箱环境（开发、测试时用）, product 正式环境 （上线后用）

    $pay = new ReceiveOrderPay();
    $pay->appKey = $appKey;

    $pay->shop_no = $shop_no;
    $pay->accountNo = $accountNo;
    $pay->run_type = $run_type;

    $pay->setNonceStr();
    $pay->setPayType('alipay');//wechat_bank 表示微信付款 到银行卡， wechat 表示付款到微信钱包， wechat_readpack 表示付款到微信红包， alipay 表示支付宝付款到支付宝账户
    
    $pay->realUserName = '张三';
    $pay->bankNo = '18290416099';
    $pay->setRequestNo();//不使用默认编号可以自写代替
    $pay->amount = 0.01;//元

    $pay->setSign();//设置签名
    $pay->setData();
    
    //记录日志到runtime/log/shoudan_pay_log中
    //Tools::log_to_write_txt(['入参：',$pay->post_params], 'shoudan_pay_log');
    $res = $pay->pay();
    //Tools::log_to_write_txt(['出参：',$res], 'shoudan_pay_log');


    dump($res);
```